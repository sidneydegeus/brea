package utility;

import controller.bedrijfsleven.BedrijfslevenImporterenController;
import dao.RegelcodeDAO;
import model.Regelcode;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Edited by Dennis
 */
public class RegelcodeImporteren {

    /**
     * The method does import all the regelcodes from a .xlsx spreadsheet and pushes the values to the database.
     * When the method is done the method opens a alertbox to inform the user. The task is appended to show the progress bar as a separete thread
     */
    public synchronized void importRegelcode(XSSFWorkbook workbook, BedrijfslevenImporterenController task) {
        Cell cell;
        String regelcodeCode = "";
        String activiteit = "";
        System.out.print("Start regelcode importeren..\n");

        ArrayList<Regelcode> regelcodes = new ArrayList<>();
        XSSFSheet sheet = workbook.getSheet("RegelCodes");
        Iterator<Row> rowIterator = sheet.iterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                cell = cellIterator.next();
                if (cell.getColumnIndex() == 2) {
                    regelcodeCode = cell.getStringCellValue();
                } else if (cell.getColumnIndex() == 3) {
                    activiteit = cell.getStringCellValue();
                }
            }

            if (regelcodeCode.contains("RegelCode") == false && activiteit.contains("Activiteit2008") == false) {
                Regelcode regelcode = new Regelcode(Integer.valueOf(regelcodeCode), activiteit);
                regelcodes.add(regelcode);
            }
        }

        RegelcodeDAO regelcodeDAO = new RegelcodeDAO();

        // get regelcodes that are already in the db
        ArrayList<Regelcode> regelcodesInDb = regelcodeDAO.getAll();
        ArrayList<Regelcode> regelcodesToRemove = new ArrayList<>();
        //check
        for (int i = 0; i < regelcodesInDb.size(); i++) {
            for (int j = 0; j < regelcodes.size(); j++) {
                if (regelcodesInDb.get(i).getRegelcodeId() == regelcodes.get(j).getRegelcodeId()) {
                    regelcodesToRemove.add(regelcodes.get(j));
                }
            }
        }

        //remove regelcodes already in db
        for (Regelcode regelcode : regelcodesToRemove) {
            regelcodes.remove(regelcode);
        }

        regelcodeDAO.insertMultiple(regelcodes, task);
        System.out.print("regelcode importeren klaar\n");
    }
}
