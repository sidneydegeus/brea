package utility;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Sidney on 6/30/2016.
 */
public class FactorenBerekenenTest {


    @Test
    public void calculateToegevoegdeWaarde() {
        int toegevoegdeWaarde = 300+650+550;
        assertEquals("personeel kosten (300) + vaste activa (650) + resultaat (550) moet 1500 zijn", 1500, toegevoegdeWaarde);
    }

    @Test
    public void calculatePROD() {
        assertEquals("Opbrengsten (1500) delen door banen (300)", 5, 1500/300);
    }

    @Test
    public void calculateBTW() {
        assertEquals("Toegevoegde waarde (3120) delen door banen (156)", 20, 3120/156);
    }

    @Test
    public void prodTotaal() {
        double prodTotaal = 200.0/400.0;
        assertEquals(0.50, prodTotaal, 0.0);
    }

    @Test
    public void twTotaal() {
        double prodTotaal = 700.0/2100.0;
        assertEquals(0.33, prodTotaal, 0.004);
    }

    @Test
    public void prod1_10() {
        double prod1_10 = 0.8 * 2000.0 * 0.50;
        assertEquals(800.0, prod1_10, 0.001);
    }

    @Test
    public void tw1_10() {
        double tw1_10 = 0.8 * 4000.0 * 0.75;
        assertEquals(2400.0, tw1_10, 0.001);
    }

    @Test
    public void prod10_100() {
        double prod10_100 = 5000.0 * 1.22;
        assertEquals(6100.0, prod10_100, 0.001);
    }

    @Test
    public void tw10_100() {
        double tw10_100 = 1000.0 * 1.56;
        assertEquals(1560.0, tw10_100, 0.001);
    }

    @Test
    public void prod100() {
        double prod100 = 2000.0 * 2.56;
        assertEquals(5120.0, prod100, 0.001);
    }

    @Test
    public void tw100() {
        double tw100 = 2500.0 * 3.00;
        assertEquals(7500.0, tw100, 0.001);
    }
}