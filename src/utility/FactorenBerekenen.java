package utility;

import controller.factoren.FactorenBerekenenController;
import model.*;

import java.util.ArrayList;

/**
 * Edited by Sidney, Dennis
 */
public class FactorenBerekenen {
    private ArrayList<Bedrijfsleven> bedrijfslevenList;
    private ArrayList<Hoofdgroep> hoofdgroepList;
    private ArrayList<Correctiefactoren> correctiefactorenList;
    private ArrayList<Factoren> factorenList;

    private FactorenBerekenenController task;

    public FactorenBerekenen(
            ArrayList<Bedrijfsleven> bedrijfslevenList,
            ArrayList<Hoofdgroep> hoofdgroepList,
            ArrayList<Correctiefactoren> correctiefactorenList,
            FactorenBerekenenController task
    ) {
        this.bedrijfslevenList = bedrijfslevenList;
        this.hoofdgroepList = hoofdgroepList;
        this.correctiefactorenList = correctiefactorenList;
        this.factorenList = new ArrayList<>();
        this.task = task;
    }

    public ArrayList<Factoren> berekenen() {
        try {
            for (Hoofdgroep hoofdgroep : hoofdgroepList) { // go through all publicatie niveaus

                for (BedrijfslevenData publicatieNiveauData : hoofdgroep.getBedrijfslevenDataList()) { // go through all publicatieniveau data years

                    double prodPublicatieNiveau = publicatieNiveauData.getOpbrengsten() / publicatieNiveauData.getBanen();
                    double btwPublicatieNiveau = publicatieNiveauData.getToegevoegdeWaarde() / publicatieNiveauData.getBanen();

                    for (Bedrijfsleven bedrijfsleven : bedrijfslevenList) { // go through all sbi

                        if (bedrijfsleven.getHoofdgroep().getPublicatie_niveau() != null) {
                            if (!bedrijfsleven.getBedrijfslevenDataList().isEmpty()
                                    && bedrijfsleven.getHoofdgroep().getPublicatie_niveau().equals(hoofdgroep.getPublicatie_niveau())) {

                                for (BedrijfslevenData sbiData : bedrijfsleven.getBedrijfslevenDataList()) { // go through all sbi data years
                                    if (publicatieNiveauData.getJaar() == sbiData.getJaar()) {
                                        double prodSbi = sbiData.getOpbrengsten() / sbiData.getBanen();
                                        double btwSbi = sbiData.getToegevoegdeWaarde() / sbiData.getBanen();
                                        try {
                                            double prodTotaal = prodSbi / prodPublicatieNiveau;
                                            if (Double.isNaN(prodTotaal) || prodTotaal == Double.POSITIVE_INFINITY || prodTotaal == Double.NEGATIVE_INFINITY) {
                                                prodTotaal = 0.0;
                                            }
                                            double prod0 = 0.001;
                                            double prod1_10 = 0.8 * prodTotaal * bedrijfsleven.getCorrectiefactoren().getBanen1Tot10Productie(); // dient correctie factor te zijn
                                            double prod10_100 = prodTotaal * bedrijfsleven.getCorrectiefactoren().getBanen10Tot100Productie(); // dient correctie factor te zijn
                                            double prod100 = prodTotaal * bedrijfsleven.getCorrectiefactoren().getBanen100Productie(); // dient correctie factor te zijn

                                            double twTotaal = btwSbi / btwPublicatieNiveau;
                                            if (Double.isNaN(twTotaal) || twTotaal == Double.POSITIVE_INFINITY || twTotaal == Double.NEGATIVE_INFINITY) {
                                                twTotaal = 0.0;
                                            }
                                            double tw0 = 0.001;
                                            double tw1_10 = 0.8 * twTotaal * bedrijfsleven.getCorrectiefactoren().getBanen1Tot10Tw(); // correctie factor bedrijfsleven.getCorrectiefactor.getTw1_10?
                                            double tw10_100 = prodTotaal * bedrijfsleven.getCorrectiefactoren().getBanen10Tot100Tw(); // correctie factor
                                            double tw100 = prodTotaal * bedrijfsleven.getCorrectiefactoren().getBanen100Tw(); // correctie factor

                                            Factoren factoren = new Factoren(
                                                    bedrijfsleven.getSbi(),
                                                    sbiData.getJaar(),
                                                    prodTotaal,
                                                    prod0,
                                                    prod1_10,
                                                    prod10_100,
                                                    prod100,
                                                    twTotaal,
                                                    tw0,
                                                    tw1_10,
                                                    tw10_100,
                                                    tw100
                                            );
                                            factorenList.add(factoren);
                                            task.setCurrentRow();
                                            task.updateProgressBar();
                                            /*
                                            System.out.println(bedrijfsleven.getSbiString() + " " + sbiData.getJaar() +
                                                    " prodTot: " + factoren.getProdTotaal() + " twTot: " + factoren.getTwTotaal()
                                                    + " prod1-10: " + factoren.getProd1To10() + " prod10-100: " + factoren.getProd10To100()
                                                    + " prod100: " + factoren.getProd100()
                                            );*/
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return factorenList;
    }
}
