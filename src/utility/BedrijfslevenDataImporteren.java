package utility;

import controller.bedrijfsleven.BedrijfslevenImporterenController;
import dao.BedrijfslevenDAO;
import dao.BedrijfslevenDataDAO;
import dao.HoofdgroepDAO;
import model.Bedrijfsleven;
import model.BedrijfslevenData;
import model.Hoofdgroep;
import model.Regelcode;
import model.factory.BedrijfslevenFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Sidney on 17-6-2016.
 */
public class BedrijfslevenDataImporteren {

    private Cell cell;
    private ArrayList<Integer> jaren = new ArrayList<>();
    private ArrayList<Double> rowCellData = new ArrayList<>();
    private ArrayList<Bedrijfsleven> bedrijfslevenList = new ArrayList<>();
    private ArrayList<Hoofdgroep> hoofdgroepList = new ArrayList<>();
    private ArrayList<BedrijfslevenData> bedrijfslevenDataList = new ArrayList<>();
    private BedrijfslevenImporterenController task;

    /**
     * importeren van bedrijfsleven data
     * @param workbook
     * @param task
     */
    public void importBedrijfslevenData(XSSFWorkbook workbook, BedrijfslevenImporterenController task) {
        this.task = task;
        System.out.print("Start ruwe data importeren..\n");

        //hack, automaticly create hoofdgroep A
        hoofdgroepList.add(new Hoofdgroep("A", ""));

        ArrayList<Regelcode> regelcodes = new ArrayList<>();
        XSSFSheet sheet = workbook.getSheet("Ruwe data Bedrijfsleven");

        Iterator<Row> rowIterator = sheet.iterator();
        int currentRow = 1;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            // check all available years
            if (currentRow == 2) {
                jaarChecker(row);
            }

            if (currentRow > 6) {
                //System.out.println("current row... " + currentRow);
                int currentCellIndex = 1;
                rowCellData.clear();
                String first = "";
                String rest = "";

                while (cellIterator.hasNext()) {
                    cell = cellIterator.next();
                    // columnIndex 0 = sbi & hoofdgroepen
                    if (cell.getColumnIndex() == 0) {
                        String cellValue = cell.getStringCellValue();
                        int i = cellValue.indexOf(" "); // 4
                        first = cellValue.substring(0, i);
                        rest = cellValue.substring(i + 1);

                        if (isNumeric(first)) {
                            String sbi = first;
                            String cbsOmschrijving = rest;

                            try {
                                Bedrijfsleven bedrijfsleven = new BedrijfslevenFactory().createBedrijfsleven(
                                        sbi,
                                        "",
                                        cbsOmschrijving,
                                        new Regelcode(0),
                                        hoofdgroepList.get(hoofdgroepList.size() - 1)
                                );
                                bedrijfslevenList.add(bedrijfsleven);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (isAlphabetic(first)) {
                            if (first.contains("-") || first.contains("+")) {
                                //hoofdgroep verzameling
                            } else if (first.length() == 1) {
                                String publicatieNiveau = first.toUpperCase();
                                String hoofdgroepOmschrijving = rest;
                                Hoofdgroep hoofdgroep = new Hoofdgroep(publicatieNiveau, hoofdgroepOmschrijving);
                                hoofdgroepList.add(hoofdgroep);
                            }
                        }
                    } else { // if cell columns != 0, which means the actual data
                        if (currentCellIndex <= (jaren.size() * 9) && isNumeric(first)) {
                            if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
                                rowCellData.add(cell.getNumericCellValue());
                            } else {
                                rowCellData.add(0.0);
                            }
                        } else {
                            break;
                        }
                        currentCellIndex++;
                    }
                } // cell iterator ends here
                //get all collected data and put it in objects...
                if (!rowCellData.isEmpty()) {
                    for (int b = 0; b < (jaren.size() * 9); b += 9) {
                        BedrijfslevenData bedrijfslevenData = new BedrijfslevenData(
                                bedrijfslevenList.get(bedrijfslevenList.size() - 1).getSbi(),
                                jaren.get(b / 9).intValue(),
                                rowCellData.get(b), // banen
                                rowCellData.get(b + 1), // opbrengsten
                                rowCellData.get(b + 2), // bedrijfskosten
                                rowCellData.get(b + 3), // inkoopwaardeOmzet
                                rowCellData.get(b + 4), // personeelKosten
                                rowCellData.get(b + 5), // overigeKosten
                                rowCellData.get(b + 6), // afschrijvingenVasteActiva
                                rowCellData.get(b + 7), // bedrijfsResultaat
                                rowCellData.get(b + 8)  // resultaatBelastingen
                        );
                        bedrijfslevenDataList.add(bedrijfslevenData);
                    }
                }
            } // if current row > 6 ends here
            currentRow++;
        } // row iterator ends here
        performDAO();
    }


    /**
     * checkt of een string numeric is
     * @param str
     * @return
     */
    private boolean isNumeric(String str) {
        try {
            int d = Integer.parseInt(str);
        } catch(NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * checkt of een string alfabetisch is
     * @param str
     * @return
     */
    private boolean isAlphabetic(String str) {
        boolean result = false;
        if (str.matches("^.*[A-Z].*$")) {
            result = true;
        }
        return result;
    }

    /**
     * leest de aantal jaren in het bestand
     * @param row
     */
    private void jaarChecker(Row row) {
        boolean notEmptyCell = true;
        int cellIndex = 1;
        while (notEmptyCell) {
            try {
                cell = row.getCell(cellIndex);

                if (cell.getCellType() == cell.CELL_TYPE_BLANK) {
                    notEmptyCell = false;
                } else {
                    jaren.add(Integer.parseInt(cell.getStringCellValue()));
                }
                cellIndex += 9;
            } catch(Exception e) {
                //e.printStackTrace();
                notEmptyCell = false;
            }
        }
    }

    /**
     * voeg alle data aan de dao toe.
     */
    private void performDAO() {
        HoofdgroepDAO hoofdgroepDAO = new HoofdgroepDAO();
        BedrijfslevenDAO bedrijfslevenDAO = new BedrijfslevenDAO();
        BedrijfslevenDataDAO bedrijfslevenDataDAO = new BedrijfslevenDataDAO();

        // get regelcodes that are already in the db
        ArrayList<Hoofdgroep> hoofdgroepInDatabaseList = hoofdgroepDAO.getAll();
        ArrayList<Regelcode> regelcodesToRemove = new ArrayList<>();

        for (int i = 0; i < hoofdgroepInDatabaseList.size(); i++) {
            for (int j = 0; j < hoofdgroepList.size(); j++) {
                if (hoofdgroepInDatabaseList.get(i).getPublicatie_niveau().equals(hoofdgroepList.get(j).getPublicatie_niveau())) {
                    hoofdgroepList.remove(j);
                }
            }
        }

        hoofdgroepDAO.insertMultiple(hoofdgroepList, task);

        // duplicate list wordt gebruikt om vervolgens te checken of de ancestors al in de bedrijfslist staan
        ArrayList<Bedrijfsleven> bedrijfslevenDuplicateList = bedrijfslevenList;
        try {
            for (Bedrijfsleven bedrijfsleven : bedrijfslevenList) {
                ArrayList<Bedrijfsleven> ancestors = bedrijfslevenDAO.getAllParentSbi(bedrijfsleven.getSbi());
                for (Bedrijfsleven ancestor : ancestors) {
                    boolean found = false;
                    for (Bedrijfsleven bed : bedrijfslevenDuplicateList) {
                        if (bed.getSbi() == ancestor.getSbi()) {
                            found = true;
                        }
                    }
                    if (!found) {
                        ancestor.getHoofdgroep().setPublicatie_niveau(bedrijfsleven.getHoofdgroep().getPublicatie_niveau());
                        bedrijfslevenList.add(ancestor);
                    }
                }
                bedrijfslevenDuplicateList = bedrijfslevenList;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        bedrijfslevenDAO.updateMultiple(bedrijfslevenList, task);
        bedrijfslevenDataDAO.insertMultiple(bedrijfslevenDataList, task);

        ArrayList<Bedrijfsleven> bedrijfslevenWithNoChildrenList = bedrijfslevenDAO.getSbiWithNoChildren();

        for (int i = 0; i < bedrijfslevenWithNoChildrenList.size(); i++) {
            for (int j = 0; j < bedrijfslevenList.size(); j++) {
                if (bedrijfslevenWithNoChildrenList.get(i).getSbi() == bedrijfslevenList.get(j).getSbi()) {
                    bedrijfslevenList.remove(j);
                }
            }
        }

        bedrijfslevenDataDAO.deleteMultiple(bedrijfslevenList);
    }
}
