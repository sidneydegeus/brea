package utility;

import controller.bedrijfsleven.BedrijfslevenImporterenController;
import dao.BedrijfslevenDAO;
import dao.BedrijfslevenVerzamelingDAO;
import model.Bedrijfsleven;
import model.BedrijfslevenSBIVerzameling;
import model.Hoofdgroep;
import model.Regelcode;
import model.factory.BedrijfslevenFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.ArrayList;
import java.util.Iterator;

import static java.lang.String.valueOf;

/**
 * @author Jordan Munk
 */
public class BedrijfslevenImporteren {


    /**
     * Hier wordt het tabblad Indeling Stat Bedrijfsleven geimporteerd. Elke rij wordt bekeken en er worden verschillende vergelijkingen uitgevoerd.
     * @param task
     * @param workbook
     */
    public synchronized void importBedrijfsleven(XSSFWorkbook workbook, BedrijfslevenImporterenController task) {
        Cell cell;
        String sbi = "";
        String regelcode = "";
        String omschrijving = "";

        ArrayList<Bedrijfsleven> bedrijfslevens = new ArrayList<>();
        ArrayList<BedrijfslevenSBIVerzameling> bedrijfslevensPlus = new ArrayList<>();
        ArrayList<BedrijfslevenSBIVerzameling> bedrijfslevensPlus2 = new ArrayList<>();
        XSSFSheet sheet = workbook.getSheet("Indeling Stat Bedrijfsleven");
        Iterator<Row> rowIterator = sheet.iterator();

        System.out.print("Start bedrijfsleven importeren..");

        int A = 1;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();


            while (cellIterator.hasNext()) {
                cell = cellIterator.next();

                /**
                 * Deze twee ifjes zijn bedoeld om te kijken of het tekstveld een String value heeft of een integer value.
                 * Als het een integer is wordt het gewijzigd naar een String value.
                 */
                if (cell.getColumnIndex() == 0) {
                    if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
                        int i = (int) cell.getNumericCellValue();
                        sbi = valueOf(i);
                    } else {
                        sbi = cell.getStringCellValue();
                    }
                }

                else if (cell.getColumnIndex() == 2) {
                    if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
                        int i = (int) cell.getNumericCellValue();
                        regelcode = valueOf(i);
                    } else {
                        regelcode = cell.getStringCellValue();
                    }
                }

                else if (cell.getColumnIndex() == 3) {
                    if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
                        int i = (int) cell.getNumericCellValue();
                        omschrijving = valueOf(i);
                    } else {
                        omschrijving = cell.getStringCellValue();
                    }
                }

            }

            /**
             * Hieronder worden de vergelijkingen uitgevoerd.
             */
            if(!sbi.contains("sbi") && !regelcode.contains("RegelCode Berijfsleven") && !omschrijving.contains("Activiteit omschrijving")) {
                if (!sbi.isEmpty() && !regelcode.isEmpty() && !omschrijving.isEmpty()) {

                    /**
                     * Er is een rij gevonden waar sbi een spatie voor had. Dit is zo opgelost.
                     */
                    if(regelcode.contains(" 99")){
                        regelcode = "99";
                    }

                    /**
                     * Sommige SBI's hebben een rest value. Vb: 103rest=1032+1039
                     * Er staan nog verschillende ifjes dit komt omdat er soms een aparte sbi staat. Dit kan alleen opgelost worden door meerdere ifjes te gebruiken.
                     * Soms staat er in de rest value bijvoorbeeld: 103rest=1029-1034+1035 of 103rest=1029, 1030+1031.
                     */
                    if (sbi.contains("rest")) {
                        //Stoppen in SBI_verzameling
                        String restVoor = sbi.split("rest")[0];
                        String restNa = sbi.split("rest=")[1];

                        Bedrijfsleven bedrijfslevenVoor = new BedrijfslevenFactory().createBedrijfsleven(
                                restVoor,
                                omschrijving,
                                "",
                                new Regelcode(Integer.valueOf(regelcode)),
                                new Hoofdgroep(0, "", ""));

                        bedrijfslevens.add(bedrijfslevenVoor);

                        BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzameling = new BedrijfslevenSBIVerzameling(Integer.valueOf(restVoor), omschrijving);
                        bedrijfslevensPlus.add(bedrijfslevenSBIVerzameling);

                        if(!restNa.contains("-") && !restNa.contains("+")){

                            Bedrijfsleven bedrijfslevenNa = new BedrijfslevenFactory().createBedrijfsleven(
                                    restNa,
                                    omschrijving,
                                    "",
                                    new Regelcode(Integer.valueOf(regelcode)),
                                    new Hoofdgroep(0, "", ""));

                            bedrijfslevens.add(bedrijfslevenNa);

                            BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzamelingNa = new BedrijfslevenSBIVerzameling(Integer.valueOf(fixSBI(restNa)), omschrijving);
                            bedrijfslevensPlus2.add(bedrijfslevenSBIVerzamelingNa);
                        }

                        if(restNa.contains("+")){
                            if(restNa.contains("-")){
                                String minGetalL = restNa.split("\\-")[0];
                                String minGetalR = restNa.split("\\-")[1];
                                String minGetalR2 = minGetalR.split("\\+")[0];
                                String naPlus = minGetalR.split("\\+")[1];

                                Bedrijfsleven bedrijfsleven = new BedrijfslevenFactory().createBedrijfsleven(
                                        naPlus,
                                        omschrijving,
                                        "",
                                        new Regelcode(Integer.valueOf(regelcode)),
                                        new Hoofdgroep(0, "", ""));


                                bedrijfslevens.add(bedrijfsleven);

                                BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzamelingnaPlus = new BedrijfslevenSBIVerzameling(Integer.valueOf(fixSBI(naPlus)), omschrijving);
                                bedrijfslevensPlus2.add(bedrijfslevenSBIVerzamelingnaPlus);

                                for(int i = Integer.parseInt(minGetalL); i <= Integer.parseInt(minGetalL) + Integer.parseInt(minGetalR2); i++){
                                    bedrijfsleven = new BedrijfslevenFactory().createBedrijfsleven(
                                            Integer.toString(i),
                                            omschrijving,
                                            "",
                                            new Regelcode(Integer.valueOf(regelcode)),
                                            new Hoofdgroep(0, "", ""));

                                    bedrijfslevens.add(bedrijfsleven);

                                    BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzamelingMin = new BedrijfslevenSBIVerzameling(Integer.valueOf(fixSBI(Integer.toString(i))), omschrijving);
                                    bedrijfslevensPlus2.add(bedrijfslevenSBIVerzamelingMin);
                                }

                            } else {
                                for (String retval : restNa.split("\\+")) {
                                    if(retval.equals("351(excl3514)")){
                                        retval = "351";
                                    }
                                    if (retval.equals("19202")) {
                                        retval = "1922";
                                    }
                                    if (retval.equals("78201")) {
                                        retval = "7821";
                                    }
                                    if (retval.equals("78202")) {
                                        retval = "7822";
                                    }

                                    Bedrijfsleven bedrijfsleven = new BedrijfslevenFactory().createBedrijfsleven(
                                            retval,
                                            omschrijving,
                                            "",
                                            new Regelcode(Integer.valueOf(regelcode)),
                                            new Hoofdgroep(0, "", ""));


                                    bedrijfslevens.add(bedrijfsleven);

                                    BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzamelingPlus = new BedrijfslevenSBIVerzameling(Integer.valueOf(fixSBI(retval)), omschrijving);
                                    bedrijfslevensPlus2.add(bedrijfslevenSBIVerzamelingPlus);

                                }
                            }
                        }

                        if(restNa.contains("-") && !restNa.contains(", ") && !restNa.contains(("+"))){
                            String voorMin = restNa.split("-")[0];
                            String naMin = restNa.split("-")[1];

                            if(naMin.equals("133 ")){
                                naMin = "133";
                            }

                            for(int i = Integer.parseInt(voorMin); i <= Integer.parseInt(naMin); i++){
                                Bedrijfsleven bedrijfsleven = new BedrijfslevenFactory().createBedrijfsleven(
                                        Integer.toString(i),
                                        omschrijving,
                                        "",
                                        new Regelcode(Integer.valueOf(regelcode)),
                                        new Hoofdgroep(0, "", ""));

                                bedrijfslevens.add(bedrijfsleven);

                                BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzamelingMin = new BedrijfslevenSBIVerzameling(Integer.valueOf(fixSBI(Integer.toString(i))), omschrijving);
                                bedrijfslevensPlus2.add(bedrijfslevenSBIVerzamelingMin);
                            }
                        }

                        if(restNa.contains(", ")){
                            String kommaGetal = restNa.split("\\,")[0];
                            String nakommaGetal = restNa.split("\\, ")[1];

                            Bedrijfsleven bedrijfsleven = new BedrijfslevenFactory().createBedrijfsleven(
                                    kommaGetal,
                                    omschrijving,
                                    "",
                                    new Regelcode(Integer.valueOf(regelcode)),
                                    new Hoofdgroep(0, "", ""));

                            bedrijfslevens.add(bedrijfsleven);

                            BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzamelingKomma = new BedrijfslevenSBIVerzameling(Integer.valueOf(fixSBI(kommaGetal)), omschrijving);
                            bedrijfslevensPlus2.add(bedrijfslevenSBIVerzamelingKomma);

                            if(restNa.contains("-")){
                                String voorMin = nakommaGetal.split("-")[0];
                                String naMin = nakommaGetal.split("-")[1];

                                for(int i = Integer.parseInt(voorMin); i <= Integer.parseInt(naMin); i++){
                                    bedrijfsleven = new BedrijfslevenFactory().createBedrijfsleven(
                                            Integer.toString(i),
                                            omschrijving,
                                            "",
                                            new Regelcode(Integer.valueOf(regelcode)),
                                            new Hoofdgroep(0, "", ""));

                                    bedrijfslevens.add(bedrijfsleven);

                                    BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzamelingMin = new BedrijfslevenSBIVerzameling(Integer.valueOf(fixSBI(Integer.toString(i))), omschrijving);
                                    bedrijfslevensPlus2.add(bedrijfslevenSBIVerzamelingMin);
                                }
                            }
                        }
                    }

                    /**
                     * Wanneer de SBI een - value heeft. (vb. 109-110). Moet 109 en 110 toegevoegd worden.
                     */
                    else if (sbi.contains("-")) {
                        String SBIMin_L = sbi.split("\\-")[0];
                        String SBIMin_R = sbi.split("\\-")[1];

                        Bedrijfsleven bedrijfslevenL = new BedrijfslevenFactory().createBedrijfsleven(
                                SBIMin_L,
                                omschrijving,
                                "",
                                new Regelcode(Integer.valueOf(regelcode)),
                                new Hoofdgroep(0, "", ""));

                        Bedrijfsleven bedrijfslevenR = new BedrijfslevenFactory().createBedrijfsleven(
                                SBIMin_R,
                                omschrijving,
                                "",
                                new Regelcode(Integer.valueOf(regelcode)),
                                new Hoofdgroep(0, "", ""));

                        bedrijfslevens.add(bedrijfslevenL);
                        bedrijfslevens.add(bedrijfslevenR);

                        BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzameling = new BedrijfslevenSBIVerzameling(Integer.valueOf(SBIMin_L), omschrijving);
                        bedrijfslevensPlus.add(bedrijfslevenSBIVerzameling);

                        BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzamelingL = new BedrijfslevenSBIVerzameling(Integer.valueOf(fixSBI(SBIMin_L)), omschrijving);
                        BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzamelingR = new BedrijfslevenSBIVerzameling(Integer.valueOf(fixSBI(SBIMin_R)), omschrijving);
                        bedrijfslevensPlus2.add(bedrijfslevenSBIVerzamelingL);
                        bedrijfslevensPlus2.add(bedrijfslevenSBIVerzamelingR);
                    }

                    /**
                     * Wanneer de SBI een + value heeft. (vb. 109-110). Moet 109 en 110 toegevoegd worden.
                     */
                    else if (sbi.contains("+")) {
                        String SBI_L = sbi.split("\\+")[0];
                        String SBI_R = sbi.split("\\+")[1];

                        if(SBI_L.equals("56102") && SBI_R.equals("56103")){
                            SBI_L = "5612";
                            SBI_R = "5613";
                        }

                                    Bedrijfsleven bedrijfslevenL = new BedrijfslevenFactory().createBedrijfsleven(
                                            SBI_L,
                                            omschrijving,
                                            "",
                                            new Regelcode(Integer.valueOf(regelcode)),
                                            new Hoofdgroep(0, "", ""));

                                    Bedrijfsleven bedrijfslevenR = new BedrijfslevenFactory().createBedrijfsleven(
                                            SBI_R,
                                            omschrijving,
                                            "",
                                            new Regelcode(Integer.valueOf(regelcode)),
                                            new Hoofdgroep(0, "", ""));

                                    bedrijfslevens.add(bedrijfslevenL);
                                    bedrijfslevens.add(bedrijfslevenR);

                         BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzameling = new BedrijfslevenSBIVerzameling(Integer.valueOf(SBI_L), omschrijving);
                         bedrijfslevensPlus.add(bedrijfslevenSBIVerzameling);

                         BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzamelingL = new BedrijfslevenSBIVerzameling(Integer.valueOf(fixSBI(SBI_L)), omschrijving);
                         BedrijfslevenSBIVerzameling bedrijfslevenSBIVerzamelingR = new BedrijfslevenSBIVerzameling(Integer.valueOf(fixSBI(SBI_R)), omschrijving);
                         bedrijfslevensPlus2.add(bedrijfslevenSBIVerzamelingL);
                         bedrijfslevensPlus2.add(bedrijfslevenSBIVerzamelingR);
                    }

                    /**
                     * Als de SBI normaal is (alleen een getal).
                     */
                    else {
                        Bedrijfsleven bedrijfsleven = new BedrijfslevenFactory().createBedrijfsleven(
                                sbi,
                                omschrijving,
                                "",
                                new Regelcode(Integer.valueOf(regelcode)),
                                new Hoofdgroep(0, "", ""));

                        bedrijfslevens.add(bedrijfsleven);

                    }

                    A++;
                }
            }
        }

        BedrijfslevenDAO bedrijfslevenDAO = new BedrijfslevenDAO();
        BedrijfslevenVerzamelingDAO bedrijfslevenSBIVerzamelingDAO = new BedrijfslevenVerzamelingDAO();

        // get bedrijfsleven that are already in the db
        ArrayList<Bedrijfsleven> bedrijfslevenInDb = bedrijfslevenDAO.getAll();
        ArrayList<Bedrijfsleven> bedrijfslevenToRemove = new ArrayList<>();

        bedrijfslevenDAO.insertMultiple(bedrijfslevens, task);

        bedrijfslevenSBIVerzamelingDAO.insertMultiple(bedrijfslevensPlus);
        bedrijfslevenSBIVerzamelingDAO.insertMultiple_sbi(bedrijfslevensPlus2);

        System.out.println("Bedrijfsleven Importeren is klaar!");
    }

    /**
     * SBI fixen
     * @param sbi
     */
    public String fixSBI(String sbi) {
        String stringSbi = "";
        char[] splitSbi = sbi.toCharArray();
        if (splitSbi.length == 1) {
            stringSbi = "0" + sbi;
        } else {
            stringSbi = sbi;
        }
        int newSbi = Integer.parseInt("1" + stringSbi);

        return valueOf(newSbi);
    }

}
