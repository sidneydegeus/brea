
import javafx.fxml.FXMLLoader;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private Stage window;
    private double width = 1366;
    private double height = 700;


    @Override
    public void start(Stage primaryStage) throws Exception {
        this.window = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("view/Base.fxml"));
        window.setTitle("Basisgegevens Regionaal Economische Analyses");

        window.setScene(new Scene(root, width, height));
        window.centerOnScreen();
        window.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}