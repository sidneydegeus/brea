package model;

import java.util.ArrayList;

/**
 * @author Jordan Munk
 */
public class Hoofdgroep {

    private int hoofdgroepID;
    private String hoofdgroepOmschrijving;
    private String publicatie_niveau;
    private ArrayList<BedrijfslevenData> bedrijfslevenDataList = new ArrayList<>();

    /* hack for now. will have to change later*/
    public Hoofdgroep(int hoofdgroepID) {
        this.hoofdgroepID = hoofdgroepID;
    }

    /**
     * Constructor voor hoofdgroep. Heeft geen hoofdgroepID.
     * @param publicatie_niveau
     * @param hoofdgroepOmschrijving
     */
    public Hoofdgroep(String publicatie_niveau, String hoofdgroepOmschrijving) {
        this.publicatie_niveau = publicatie_niveau;
        this.hoofdgroepOmschrijving = hoofdgroepOmschrijving;
    }

    /**
     * Constructor voor hoofdgroep. Deze constructor bevat wel hoofdgroepID.
     * @param publicatie_niveau
     * @param hoofdgroepOmschrijving
     * @param hoofdgroepID
     */
    public Hoofdgroep(int hoofdgroepID, String publicatie_niveau, String hoofdgroepOmschrijving) {
        this.hoofdgroepID = hoofdgroepID;
        this.publicatie_niveau = publicatie_niveau;
        this.hoofdgroepOmschrijving = hoofdgroepOmschrijving;
    }

    public int getHoofdgroepID() {
        return hoofdgroepID;
    }

    public void setHoofdgroepID(int hoofdgroepID) {
        this.hoofdgroepID = hoofdgroepID;
    }

    public String getHoofdgroepOmschrijving() {
        return hoofdgroepOmschrijving;
    }

    public void setHoofdgroepOmschrijving(String hoofdgroepOmschrijving) {
        this.hoofdgroepOmschrijving = hoofdgroepOmschrijving;
    }

    public String getPublicatie_niveau() {
        return publicatie_niveau;
    }

    public void setPublicatie_niveau(String publicatie_niveau) {
        this.publicatie_niveau = publicatie_niveau;
    }

    public ArrayList<BedrijfslevenData> getBedrijfslevenDataList() {
        return bedrijfslevenDataList;
    }

    public void setBedrijfslevenDataList(ArrayList<BedrijfslevenData> bedrijfslevenDataList) {
        this.bedrijfslevenDataList = bedrijfslevenDataList;
    }
}
