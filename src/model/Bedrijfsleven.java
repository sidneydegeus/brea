package model;

import java.util.ArrayList;

/**
 * Created by Sidney on 16-5-2016.
 */
public abstract class Bedrijfsleven implements Subject {

    protected int sbi;
    protected int parentSbi;
    protected String omschrijving;
    protected String cbsOmschrijving; // was eerst bedrijfstak
    protected Regelcode regelcode;
    protected ArrayList<BedrijfslevenData> bedrijfslevenDataList = new ArrayList<>();
    protected Hoofdgroep hoofdgroep;
    protected String sbiString;

    protected Correctiefactoren correctiefactoren;

    /**
     * Dient alleen gebruikt te worden bij het inladen van spreadsheets.
     * @param sbi
     * @param omschrijving
     * @param regelcode
     */
    public Bedrijfsleven(int sbi, String omschrijving, Regelcode regelcode) {
        this.sbi = sbi;
        this.omschrijving = omschrijving;
        this.regelcode = regelcode;
    }

    /**
     * Bedrijfsklasse constructor.
     * @param cbsOmschrijving
     * @param hoofdgroep
     * @param sbi
     * @param omschrijving
     */
    public Bedrijfsleven(int sbi, String omschrijving, String cbsOmschrijving, Regelcode regelcode, Hoofdgroep hoofdgroep, String sbiString) {
        this.sbi = sbi;
        this.omschrijving = omschrijving;
        this.cbsOmschrijving = cbsOmschrijving;
        this.regelcode = regelcode;
        this.hoofdgroep = hoofdgroep;
        this.sbiString = sbiString;
    }

    /**
     * constructor voor alles behalve bedrijfsklasse.
     * @param parentSbi
     * @param cbsOmschrijving
     * @param hoofdgroep
     * @param sbi
     * @param omschrijving
     */
    public Bedrijfsleven(int sbi, int parentSbi, String omschrijving, String cbsOmschrijving, Regelcode regelcode, Hoofdgroep hoofdgroep, String sbiString) {
        this.sbi = sbi;
        this.parentSbi = parentSbi;
        this.omschrijving = omschrijving;
        this.cbsOmschrijving = cbsOmschrijving;
        this.regelcode = regelcode;
        this.hoofdgroep = hoofdgroep;
        this.sbiString = sbiString;
    }

    public void registerObserver() {

    }

    public void removeObserver() {

    }

    public void notifyObserver() {

    }

    public int getSbi() {
        return sbi;
    }

    public void setSbi(int sbi) {
        this.sbi = sbi;
    }

    public int getParentSbi() {
        return parentSbi;
    }

    public void setParentSbi(int parentSbi) {
        this.parentSbi = parentSbi;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public String getCbsOmschrijving() {
        return cbsOmschrijving;
    }

    public void setCbsOmschrijving(String cbsOmschrijving) {
        this.cbsOmschrijving = cbsOmschrijving;
    }

    public Regelcode getRegelcode() {
        return regelcode;
    }

    public void setRegelcode(Regelcode regelcode) {
        this.regelcode = regelcode;
    }

    public void setRegelcodeId (int regelcodeId) {
        this.regelcode.setRegelcodeId(regelcodeId);
    }

    public ArrayList<BedrijfslevenData> getBedrijfslevenDataList() {
        return bedrijfslevenDataList;
    }

    public void setBedrijfslevenDataList(ArrayList<BedrijfslevenData> bedrijfslevenDataList) {
        this.bedrijfslevenDataList = bedrijfslevenDataList;
    }

    public void registerBedrijfslevenData(BedrijfslevenData bedrijfslevenData) {
        this.getBedrijfslevenDataList().add(bedrijfslevenData);
    }

    public void removeBedrijfslevenData(BedrijfslevenData bedrijfslevenData) {
        this.getBedrijfslevenDataList().remove(bedrijfslevenData);
    }

    public Hoofdgroep getHoofdgroep() {
        return hoofdgroep;
    }

    public void setHoofdgroep(Hoofdgroep hoofdgroep) {
        this.hoofdgroep = hoofdgroep;
    }

    public String getHoofdgroepOmschrijving() {
        return getHoofdgroep().getHoofdgroepOmschrijving();
    }

    public String getSbiString() {
        return sbiString;
    }

    public void setSbiString(String sbiString) {
        this.sbiString = sbiString;
    }

    public Correctiefactoren getCorrectiefactoren() {
        return correctiefactoren;
    }

    public void setCorrectiefactoren(Correctiefactoren correctiefactoren) {
        this.correctiefactoren = correctiefactoren;
    }

}
