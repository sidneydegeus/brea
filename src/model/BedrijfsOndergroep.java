package model;

/**
 * Created by Sidney on 16-5-2016.
 */
public class BedrijfsOndergroep extends BedrijfsSubgroep {

    /**
     * Zie Bedrijfsleven constructors.
     * @param sbi
     * @param omschrijving
     * @param regelcode
     */
    public BedrijfsOndergroep(int sbi, String omschrijving, Regelcode regelcode) {
        super(sbi, omschrijving, regelcode);
    }

    /**
     * Zie Bedrijfsleven constructors.
     * @param sbi
     * @param parentSbi
     * @param omschrijving
     * @param cbsOmschrijving
     * @param regelcode
     * @param hoofdgroep
     */
    public BedrijfsOndergroep(int sbi, int parentSbi, String omschrijving, String cbsOmschrijving, Regelcode regelcode, Hoofdgroep hoofdgroep, String sbiString) {
        super(sbi, parentSbi, omschrijving, cbsOmschrijving, regelcode, hoofdgroep, sbiString);
    }


}
