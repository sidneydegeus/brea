package model;

/**
 * Created by dennis on 16-5-2016.
 */
public class Factoren implements Subject{
    private int SBI;            //1
    private int jaar;           //2
    private double prodTotaal;  //3
    private double prod0;       //4
    private double prod1To10;   //5
    private double prod0To10;   //6
    private double prod0To19;   //7
    private double prod10To100; //8
    private double prod20To100; //9
    private double prod0To100;  //10
    private double prod100;     //11
    private double twTotaal;    //12
    private double tw0;         //13
    private double tw1To10;     //14
    private double tw0To10;     //15
    private double tw10To100;   //16
    private double tw0To19;     //17
    private double tw20To100;   //18
    private double tw0To100;    //19
    private double tw100;       //20

    private String sbiString;
    private String publicatieNiveau;

    /**
     * Constructor voor alle waarden.
     * @param sbi
     * @param jaar
     * @param prodTotaal
     * @param prod0
     * @param prod1To10
     * @param prod0To10
     * @param prod10To100
     * @param prod0To19
     * @param prod20To100
     * @param prod100
     * @param twTotaal
     * @param tw0
     * @param tw1To10
     * @param tw0To10
     * @param tw10To100
     * @param tw0To19
     * @param tw0To100
     * @param tw100
     * @param sbiString
     */
    public Factoren(int sbi, int jaar, double prodTotaal, double prod0, double prod1To10, double prod0To10, double prod10To100, double prod0To19, double prod20To100, double prod0To100, double prod100, double twTotaal, double tw0,
                    double tw1To10, double tw0To10, double tw10To100, double tw0To19, double tw20To100, double tw0To100, double tw100, String sbiString, String publicatieNiveau) {
        this.SBI = sbi;
        this.jaar = jaar;
        this.prodTotaal = prodTotaal;
        this.prod0 = prod0;
        this.prod1To10 = prod1To10;
        this.prod0To10 = prod0To10;
        this.prod0To19 = prod0To19;
        this.prod10To100 = prod10To100;
        this.prod20To100 = prod20To100;
        this.prod0To100 = prod0To100;
        this.prod100 = prod100;
        this.twTotaal = twTotaal;
        this.tw0 = tw0;
        this.tw1To10 = tw1To10;
        this.tw0To10 = tw0To10;
        this.tw10To100 = tw10To100;
        this.tw0To19 = tw0To19;
        this.tw20To100 = tw20To100;
        this.tw0To100 = tw0To100;
        this.tw100 = tw100;
        this.sbiString = sbiString;
        this.publicatieNiveau = publicatieNiveau;
    }

    /**
     * Constructor voor de niet alle ingevoerde gegevens.
     * @param sbi
     * @param jaar
     * @param prodTotaal
     * @param prod0
     * @param prod1To10
     * @param prod10To100
     * @param prod100
     * @param twTotaal
     * @param tw0
     * @param tw1To10
     * @param tw10To100
     * @param tw100
     */
    public Factoren(int sbi, int jaar, double prodTotaal, double prod0, double prod1To10, double prod10To100, double prod100, double twTotaal, double tw0,
                    double tw1To10, double tw10To100, double tw100) {
        this.SBI = sbi;
        this.jaar = jaar;
        this.prodTotaal = prodTotaal;
        this.prod0 = prod0;
        this.prod1To10 = prod1To10;
        this.prod10To100 = prod10To100;
        this.prod100 = prod100;
        this.twTotaal = twTotaal;
        this.tw0 = tw0;
        this.tw1To10 = tw1To10;
        this.tw10To100 = tw10To100;
        this.tw100 = tw100;
    }

    @Override
    public void registerObserver() {

    }

    @Override
    public void removeObserver() {

    }

    @Override
    public void notifyObserver() {

    }

    public int getSBI() {
        return SBI;
    }

    public void setSBI(int SBI) {
        this.SBI = SBI;
    }

    public int getJaar() {
        return jaar;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    public double getProdTotaal() {
        return prodTotaal;
    }

    public void setProdTotaal(double prodTotaal) {
        this.prodTotaal = prodTotaal;
    }

    public double getProd0() {
        return prod0;
    }

    public void setProd0(double prod0) {
        this.prod0 = prod0;
    }

    public double getProd1To10() {
        return prod1To10;
    }

    public void setProd1To10(double prod1To100) {
        this.prod1To10 = prod1To100;
    }

    public double getProd0To10() {
        return prod0To10;
    }

    public void setProd0To10(double prod0To10) {
        this.prod0To10 = prod0To10;
    }

    public double getProd0To19() {
        return prod0To19;
    }

    public void setProd0To19(double prod0To19) {
        this.prod0To19 = prod0To19;
    }

    public double getProd20To100() {
        return prod20To100;
    }

    public void setProd20To100(double prod20To100) {
        this.prod20To100 = prod20To100;
    }

    public double getProd100() {
        return prod100;
    }

    public void setProd100(double prod100) {
        this.prod100 = prod100;
    }

    public double getTwTotaal() {
        return twTotaal;
    }

    public void setTwTotaal(double twTotaal) {
        this.twTotaal = twTotaal;
    }

    public double getTw0() {
        return tw0;
    }

    public void setTw0(double tw0) {
        this.tw0 = tw0;
    }

    public double getTw1To10() {
        return tw1To10;
    }

    public void setTw1To10(double tw1To10) {
        this.tw1To10 = tw1To10;
    }

    public double getTw0To10() {
        return tw0To10;
    }

    public void setTw0To10(double tw0To10) {
        this.tw0To10 = tw0To10;
    }

    public double getTw10To100() {
        return tw10To100;
    }

    public void setTw10To100(double tw10To100) {
        this.tw10To100 = tw10To100;
    }

    public double getTw0To19() {
        return tw0To19;
    }

    public void setTw0To19(double tw0To19) {
        this.tw0To19 = tw0To19;
    }

    public double getTw0To100() {
        return tw0To100;
    }

    public void setTw0To100(double tw0To100) {
        this.tw0To100 = tw0To100;
    }

    public double getTw100() {
        return tw100;
    }

    public void setTw100(double tw100) {
        this.tw100 = tw100;
    }


    public double getProd10To100() {
        return prod10To100;
    }

    public void setProd10To100(double prod10To100) {
        this.prod10To100 = prod10To100;
    }

    public double getProd0To100() {
        return prod0To100;
    }

    public void setProd0To100(double prod0To100) {
        this.prod0To100 = prod0To100;
    }

    public double getTw20To100() {
        return tw20To100;
    }

    public void setTw20To100(double tw20To100) {
        this.tw20To100 = tw20To100;
    }

    public String getSbiString() {
        return sbiString;
    }

    public String getPublicatieNiveau() {
        return publicatieNiveau;
    }
}
