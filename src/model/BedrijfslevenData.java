package model;

/**
 * Created by Sidney on 16-5-2016.
 */
public class BedrijfslevenData implements Subject {

    private int sbi;
    private int jaar;
    private double banen;
    private double opbrengsten;
    private double bedrijfskosten;
    private double inkoopwaardeOmzet;
    private double personeelKosten;
    private double overigeKosten;
    private double afschrijvingenVasteActiva;
    private double bedrijfsResultaat;
    private double resultaatBelastingen;

    private double toegevoegdeWaarde;

    public BedrijfslevenData(int sbi, int jaar, double banen, double opbrengsten, double bedrijfskosten,
                             double inkoopwaardeOmzet, double personeelKosten, double overigeKosten,
                             double afschrijvingenVasteActiva, double bedrijfsResultaat, double resultaatBelastingen) {
        this.sbi = sbi;
        this.jaar = jaar;
        this.banen = banen;
        this.opbrengsten = opbrengsten;
        this.bedrijfskosten = bedrijfskosten;
        this.inkoopwaardeOmzet = inkoopwaardeOmzet;
        this.personeelKosten = personeelKosten;
        this.overigeKosten = overigeKosten;
        this.afschrijvingenVasteActiva = afschrijvingenVasteActiva;
        this.bedrijfsResultaat = bedrijfsResultaat;
        this.resultaatBelastingen = resultaatBelastingen;
        this.toegevoegdeWaarde = personeelKosten + afschrijvingenVasteActiva + bedrijfsResultaat;
    }

    @Override
    public void registerObserver() {

    }

    @Override
    public void removeObserver() {

    }

    @Override
    public void notifyObserver() {

    }

    public int getSbi() {
        return sbi;
    }

    public void setSbi(int sbi) {
        this.sbi = sbi;
    }

    public int getJaar() {
        return jaar;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    public double getBanen() {
        return banen;
    }

    public void setBanen(double banen) {
        this.banen = banen;
    }

    public double getOpbrengsten() {
        return opbrengsten;
    }

    public void setOpbrengsten(double opbrengsten) {
        this.opbrengsten = opbrengsten;
    }

    public double getBedrijfskosten() {
        return bedrijfskosten;
    }

    public void setBedrijfskosten(double bedrijfskosten) {
        this.bedrijfskosten = bedrijfskosten;
    }

    public double getInkoopwaardeOmzet() {
        return inkoopwaardeOmzet;
    }

    public void setInkoopwaardeOmzet(double inkoopwaardeOmzet) {
        this.inkoopwaardeOmzet = inkoopwaardeOmzet;
    }

    public double getPersoneelKosten() {
        return personeelKosten;
    }

    public void setPersoneelKosten(double personeelKosten) {
        this.personeelKosten = personeelKosten;
    }

    public double getOverigeKosten() {
        return overigeKosten;
    }

    public void setOverigeKosten(double overigeKosten) {
        this.overigeKosten = overigeKosten;
    }

    public double getAfschrijvingenVasteActiva() {
        return afschrijvingenVasteActiva;
    }

    public void setAfschrijvingenVasteActiva(double afschrijvingenVasteActiva) {
        this.afschrijvingenVasteActiva = afschrijvingenVasteActiva;
    }

    public double getBedrijfsResultaat() {
        return bedrijfsResultaat;
    }

    public void setBedrijfsResultaat(double bedrijfsresultaat) {
        this.bedrijfsResultaat = bedrijfsresultaat;
    }

    public double getResultaatBelastingen() {
        return resultaatBelastingen;
    }

    public void setResultaatBelastingen(double resultaatBelastingen) {
        this.resultaatBelastingen = resultaatBelastingen;
    }


    public double getToegevoegdeWaarde() {
        return toegevoegdeWaarde;
    }

    public void setToegevoegdeWaarde(double toegevoegdeWaarde) {
        this.toegevoegdeWaarde = toegevoegdeWaarde;
    }
}
