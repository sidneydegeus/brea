package model.factory;

import model.*;

import java.util.ArrayList;

/**
 * Edited by: Sidney
 */
public class BedrijfslevenFactory {

    /**
     * createBedrijfsleven dat een string als sbi opvangt. Bedoeld voor nieuwe gegevens die aan de database zullen worden toegevoegd
     * @param sbi
     * @param omschrijving
     * @param cbsOmschrijving
     * @param regelcode
     * @param hoofdgroep
     * @return
     */
    public Bedrijfsleven createBedrijfsleven(String sbi, String omschrijving, String cbsOmschrijving, Regelcode regelcode, Hoofdgroep hoofdgroep) {
        String stringSbi = "";
        char[] splitSbi = sbi.toCharArray();
        if (splitSbi.length == 1) {
            stringSbi = "0" + sbi;
        } else {
            stringSbi = sbi;
        }
        int newSbi = Integer.parseInt("1" + stringSbi);

        return create(newSbi, omschrijving, cbsOmschrijving, regelcode, hoofdgroep, stringSbi);
    }

    /**
     * createBedrijfsleven dat een int als sbi opvangt. Bedoeld voor het ophalen van data uit de database.
     * @param sbi
     * @param omschrijving
     * @param cbsOmschrijving
     * @param regelcode
     * @param hoofdgroep
     * @return
     */
    public Bedrijfsleven createBedrijfsleven(int sbi, String omschrijving, String cbsOmschrijving, Regelcode regelcode, Hoofdgroep hoofdgroep, String sbiString) {
        return create(sbi, omschrijving, cbsOmschrijving, regelcode, hoofdgroep, sbiString);
    }

    /**
     * create dient voor reusability, zodat de 2 createBedrijfsleven gebruik maken van de zelfde methode.
     * @param sbi
     * @param omschrijving
     * @param cbsOmschrijving
     * @param regelcode
     * @param hoofdgroep
     * @return
     */
    private Bedrijfsleven create(int sbi, String omschrijving, String cbsOmschrijving, Regelcode regelcode, Hoofdgroep hoofdgroep, String sbiString) {
        Bedrijfsleven bedrijfsleven = null;

        int parentSbi = parentSbiCalculator(sbi);

        // divide and conquer
        if (sbi < 10000) {
            if (sbi < 1000) {
                bedrijfsleven = new BedrijfsKlasse(sbi, omschrijving, cbsOmschrijving, regelcode, hoofdgroep, sbiString); //2 digits
            } else {
                bedrijfsleven = new BedrijfsGroep(sbi, parentSbi, omschrijving, cbsOmschrijving, regelcode, hoofdgroep, sbiString); // 3 digits
            }
        } else if (sbi >= 10000) {
            if (sbi < 100000) {
                bedrijfsleven = new BedrijfsSubgroep(sbi, parentSbi, omschrijving, cbsOmschrijving, regelcode, hoofdgroep, sbiString); // 4 digits
            } else {
                bedrijfsleven = new BedrijfsOndergroep(sbi, parentSbi, omschrijving, cbsOmschrijving, regelcode, hoofdgroep, sbiString); // 5 digits
            }
        }
        return bedrijfsleven;
    }

    public int parentSbiCalculator(int sbi) {
        String tmpSbi = Integer.toString(sbi);
        String tmpParentSbi = "";
        char[] splitSbi = tmpSbi.toCharArray();
        if (splitSbi.length > 3) {
            for (int i = 0; i < (splitSbi.length - 1); i++) {
                if (i > 4) {
                    break;
                }
                tmpParentSbi += splitSbi[i];
            }
        }

        int parentSbi;
        if (tmpParentSbi.equals("")) {
            parentSbi = 0;
        } else {
            parentSbi = Integer.parseInt(tmpParentSbi);
        }
        return parentSbi;
    }

}
