package model;

import java.util.ArrayList;

/**
 * Created by Sidney on 16-5-2016.
 */
public class BedrijfsKlasse extends Bedrijfsleven {

    private ArrayList<BedrijfsGroep> bedrijfsGroepList;

    /**
     * Zie Bedrijfsleven constructors.
     * @param sbi
     * @param omschrijving
     * @param regelcode
     */
    public BedrijfsKlasse(int sbi, String omschrijving, Regelcode regelcode) {
        super(sbi, omschrijving, regelcode);
    }

    /**
     * Zie Bedrijfsleven constructors.
     * @param sbi
     * @param omschrijving
     * @param cbsOmschrijving
     * @param regelcode
     * @param hoofdgroep
     */
    public BedrijfsKlasse(int sbi, String omschrijving, String cbsOmschrijving, Regelcode regelcode, Hoofdgroep hoofdgroep, String sbiString) {
        super(sbi, omschrijving, cbsOmschrijving, regelcode, hoofdgroep, sbiString);
    }

    /**
     * Zie Bedrijfsleven constructors.
     * @param sbi
     * @param omschrijving
     * @param cbsOmschrijving
     * @param regelcode
     * @param hoofdgroep
     */
    public BedrijfsKlasse(int sbi, int parentSbi, String omschrijving, String cbsOmschrijving, Regelcode regelcode, Hoofdgroep hoofdgroep, String sbiString) {
        super(sbi, parentSbi, omschrijving, cbsOmschrijving, regelcode, hoofdgroep, sbiString);
    }
}
