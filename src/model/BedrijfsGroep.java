package model;

import java.util.ArrayList;

/**
 * Created by Sidney on 16-5-2016.
 */
public class BedrijfsGroep extends BedrijfsKlasse {

    private ArrayList<BedrijfsSubgroep> bedrijfsSubgroepList;

    /**
     * Zie Bedrijfsleven constructors.
     * @param sbi
     * @param omschrijving
     * @param regelcode
     */
    public BedrijfsGroep(int sbi, String omschrijving, Regelcode regelcode) {
        super(sbi, omschrijving, regelcode);
    }

    /**
     * Zie Bedrijfsleven constructors.
     * @param sbi
     * @param parentSbi
     * @param omschrijving
     * @param cbsOmschrijving
     * @param regelcode
     * @param hoofdgroep
     */
    public BedrijfsGroep(int sbi, int parentSbi, String omschrijving, String cbsOmschrijving, Regelcode regelcode, Hoofdgroep hoofdgroep, String sbiString) {
        super(sbi, parentSbi, omschrijving, cbsOmschrijving, regelcode, hoofdgroep, sbiString);
    }
}
