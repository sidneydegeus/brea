package model;

/**
 * Created by dennis on 16-6-2016.
 */
public class Correctiefactoren {
    private int sbi;
    private String sbiString;
    private double banen0Productie;
    private double banen1Tot10Productie;
    private double banen10Tot100Productie;
    private double banen100Productie;
    private double banen0Tw;
    private double banen1Tot10Tw;
    private double banen10Tot100Tw;
    private double banen100Tw;

    /**
     * Dient ALÉÉN gebruikt te worden bij het importeren van de spreadsheets.
     * @param sbi
     */
    public Correctiefactoren(int sbi) {
        this.sbi = sbi;
    }

    /**
     * Standaard constructor voor regelcode. Dient in alle gevallen gebruikt te worden buiten importeren om.
     * @param sbi
     * @param banen0Productie
     * @param banen1Tot10Productie
     * @param banen10Tot100Productie
     * @param banen100Productie*
     * @param banen0Tw*
     * @param banen1Tot10Tw*
     * @param banen10Tot100Tw*
     * @param banen100Tw*
     */
    public Correctiefactoren(int sbi, String sbiString, double banen0Productie, double banen1Tot10Productie, double banen10Tot100Productie, double banen100Productie, double banen0Tw,
                             double banen1Tot10Tw, double banen10Tot100Tw, double banen100Tw) {

        this.sbi = sbi;
        this.sbiString = sbiString;
        this.banen0Productie = banen0Productie;
        this.banen1Tot10Productie = banen1Tot10Productie;
        this.banen10Tot100Productie = banen10Tot100Productie;
        this.banen100Productie = banen100Productie;
        this.banen0Tw = banen0Tw;
        this.banen1Tot10Tw = banen1Tot10Tw;
        this.banen10Tot100Tw = banen10Tot100Tw;
        this.banen100Tw = banen100Tw;

    }

    public void setSbiString(String sbiString){
        this.sbiString = sbiString;
    }

    public String getSbiString() {
        return this.sbiString;
    }

    public int getSbi() {
        return sbi;
    }

    public void setSbi(int sbi) {
        this.sbi = sbi;
    }

    public double getBanen0Productie() {
        return banen0Productie;
    }

    public void setBanen0Productie(double banen0Productie) {
        this.banen0Productie = banen0Productie;
    }

    public double getBanen1Tot10Productie() {
        return banen1Tot10Productie;
    }

    public void setBanen1Tot10Productie(double banen1Tot10Productie) {
        this.banen1Tot10Productie = banen1Tot10Productie;
    }

    public double getBanen10Tot100Productie() {
        return banen10Tot100Productie;
    }

    public void setBanen10Tot100Productie(double banen10Tot100Productie) {
        this.banen10Tot100Productie = banen10Tot100Productie;
    }

    public double getBanen100Productie() {
        return banen100Productie;
    }

    public void setBanen100Productie(double banen100Productie) {
        this.banen100Productie = banen100Productie;
    }

    public double getBanen0Tw() {
        return banen0Tw;
    }

    public void setBanen0Tw(double banen0Tw) {
        this.banen0Tw = banen0Tw;
    }

    public double getBanen1Tot10Tw() {
        return banen1Tot10Tw;
    }

    public void setBanen1Tot10Tw(double banen1Tot10Tw) {
        this.banen1Tot10Tw = banen1Tot10Tw;
    }

    public double getBanen10Tot100Tw() {
        return banen10Tot100Tw;
    }

    public void setBanen10Tot100Tw(double banen10Tot100Tw) {
        this.banen10Tot100Tw = banen10Tot100Tw;
    }

    public double getBanen100Tw() {
        return banen100Tw;
    }

    public void setBanen100Tw(double banen100Tw) {
        this.banen100Tw = banen100Tw;
    }


}
