package model;

/**
 * Created by Sidney on 16-5-2016.
 */
public class Regelcode implements Subject {

    private int regelcodeId;
    private String activiteit;

    /**
     * Dient ALÉÉN gebruikt te worden bij het importeren van de spreadsheets.
     * @param regelcodeId
     */
    public Regelcode(int regelcodeId) {
        this.regelcodeId = regelcodeId;
    }

    /**
     * Standaard constructor voor regelcode. Dient in alle gevallen gebruikt te worden buiten importeren om.
     * @param regelcodeId
     * @param activiteit
     *
     */
    public Regelcode(int regelcodeId, String activiteit) {
        this.regelcodeId = regelcodeId;
        this.activiteit = activiteit;
    }

    @Override
    public void registerObserver() {

    }

    @Override
    public void removeObserver() {

    }

    @Override
    public void notifyObserver() {

    }

    public int getRegelcodeId() {
        return regelcodeId;
    }

    public void setRegelcodeId(int regelcodeId) {
        this.regelcodeId = regelcodeId;
    }

    public String getActiviteit() {
        return activiteit;
    }

    public void setActiviteit(String activiteit) {
        this.activiteit = activiteit;
    }
}
