package model;

/**
 * Created by Sidney on 16-5-2016.
 */
interface Subject {

    void registerObserver();

    void removeObserver();

    void notifyObserver();
}
