package model;

/**
 * @author Jordan Munk
 */
public class BedrijfslevenSBIVerzameling {

    protected String omschrijving;
    protected int sbi;

    /**
     * De constructor voor SBI verzameling. Wordt gebruikt bij het importeren van bedrijfsleven.
     * @param sbi
     * @param omschrijving
     */
    public BedrijfslevenSBIVerzameling(int sbi, String omschrijving){
        this.sbi = sbi;
        this.omschrijving = omschrijving;
    }

    public int getSbi(){
        return sbi;
    }

    public String getOmschrijving(){
        return omschrijving;
    }

    public void setSBI(int sbi){
        this.sbi = sbi;
    }

    public void setOmschrijving(String omschrijving){
        this.omschrijving = omschrijving;
    }
}
