package model;

/**
 * Created by Sidney on 16-5-2016.
 */
public class HoofdgroepVerzameling {

    private int hoofdgroepVerzamelingID;
    private String hoofdgroepVerzamelingOmschrijving;

    public HoofdgroepVerzameling() {

    }

    public int getHoofdgroepVerzamelingID() {
        return hoofdgroepVerzamelingID;
    }

    public void setHoofdgroepVerzamelingID(int hoofdgroepVerzamelingID) {
        this.hoofdgroepVerzamelingID = hoofdgroepVerzamelingID;
    }

    public String getHoofdgroepVerzamelingOmschrijving() {
        return hoofdgroepVerzamelingOmschrijving;
    }

    public void setHoofdgroepVerzamelingOmschrijving(String hoofdgroepVerzamelingOmschrijving) {
        this.hoofdgroepVerzamelingOmschrijving = hoofdgroepVerzamelingOmschrijving;
    }
}
