package dao;

import com.sun.prism.impl.Disposer;
import controller.bedrijfsleven.BedrijfslevenImporterenController;
import model.Hoofdgroep;
import model.Regelcode;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by dennis on 16-5-2016.
 * Edited by Jordan on 2-6-2016.
 */
public class RegelcodeDAO {
    private DAO daoInstance;
    private final String SELECT = "SELECT regelcode_id, activiteit ";
    private final String FROM = "FROM public.regelcode;";
    private final String FROM_ORDER_BY = "FROM public.regelcode ORDER BY regelcode_id;";
    private final String FROM_WHERE = "FROM public.regelcode WHERE ";
    private final String INSERT = "INSERT INTO public.regelcode (regelcode_id, activiteit) VALUES(?, ?)";
    private final String UPDATE = "UPDATE public.regelcode SET regelcode_id = ?, activiteit = ?";
    private final String DELETE = "DELETE FROM public.regelcode";

    public RegelcodeDAO() {
        daoInstance = DAO.getInstance();
    }

    /**
     * Get all regel codes from database, orderer by regelcode_id
     * @return Regelcodes
     */
    public ArrayList<Regelcode> getAll() {
        String query = SELECT + FROM_ORDER_BY;
        try {
            Connection connection = daoInstance.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            return buildResult(resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get regelcode by id
     * @param regelcodeId
     * @return Regelcode
     */
    public ArrayList<Regelcode> getByRegelcodeId(int regelcodeId) {
        ArrayList<Regelcode> regelcodeList = null;
        String query = SELECT + FROM_WHERE + "regelcode_id = ?";
        Connection connection = null;

        PreparedStatement preparedStatement = null;
        try {
            connection = daoInstance.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, regelcodeId);
            ResultSet resultSet = preparedStatement.executeQuery();
            regelcodeList = buildResult(resultSet);
        } catch(Exception e) {
            e.printStackTrace();
        }finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return regelcodeList;
    }

    /**
     * Update a regelcode
     * @param regelcode
     */
    public void update(Regelcode regelcode) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = daoInstance.getConnection();
            // Statements allow to issue SQL queries to the database
            preparedStatement = connection.prepareStatement(UPDATE + " WHERE regelcode_id = " + regelcode.getRegelcodeId());
            preparedStatement.setInt(1, regelcode.getRegelcodeId());
            preparedStatement.setString(2, regelcode.getActiviteit());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Insert a regelcode
     * @param regelcode
     */
    public void insert(Regelcode regelcode) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {

            connection = daoInstance.getConnection();
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setInt(1, regelcode.getRegelcodeId());
            preparedStatement.setString(2, regelcode.getActiviteit());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Insert multiple regelcodes
     * @param regelcodes
     */
    public void insertMultiple(ArrayList<Regelcode> regelcodes, BedrijfslevenImporterenController task) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
//            TODO, check if there are regelcodes in the database that already exists in this list, and don't add them to the preparedStatement/batch.

            connection = daoInstance.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(INSERT);

            for (int i = 0; i < regelcodes.size(); i++) {
                preparedStatement.setInt(1, regelcodes.get(i).getRegelcodeId());
                preparedStatement.setString(2, regelcodes.get(i).getActiviteit());
                preparedStatement.addBatch();
                task.setCurrentRow();
                task.updateProgressBar();
            }
            preparedStatement.executeBatch();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            task.setCurrentRow();
            task.updateProgressBar();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Delete a Regelcode
     * @param regelcode
     */
    public void delete(Regelcode regelcode) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = daoInstance.getConnection();
            // Statements allow to issue SQL queries to the database
            preparedStatement = connection.prepareStatement(DELETE + " WHERE regelcode_id = " + regelcode.getRegelcodeId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Build the result
     * @param resultSet
     * @return
     */
    private ArrayList<Regelcode> buildResult(ResultSet resultSet) {
        ArrayList<Regelcode> result = new ArrayList<>();
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    Regelcode regelcode;
                    int regelcodeId = resultSet.getInt(1);
                    String activiteit = resultSet.getString(2);

                    regelcode = new Regelcode(regelcodeId, activiteit);
                    // divide and conquer
                    result.add(regelcode);
                }
            } catch (Exception e) {
                e.printStackTrace();
                //returning null zodat het programma blijft werken
                return null;
            }
        }
        return result;
    }
}
