package dao;

import controller.bedrijfsleven.BedrijfslevenImporterenController;
import model.Hoofdgroep;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Jordan Munk
 */
public class HoofdgroepDAO {

    private DAO daoInstance;
    private final String SELECT = "SELECT publicatie_niveau, omschrijving ";
    private final String FROM = "FROM public.hoofdgroep;";
    private final String FROM_WHERE = "FROM public.hoofdgroep WHERE ";
    private final String INSERT = "INSERT INTO public.hoofdgroep (publicatie_niveau, omschrijving) VALUES(?, ?)";
    private final String UPDATE = "UPDATE public.hoofdgroep SET omschrijving = ?";
    private final String DELETE = "DELETE FROM public.hoofdgroep";

    public HoofdgroepDAO() {
        daoInstance = DAO.getInstance();
    }

    /**
     * Haalt alle hoofdgroepen op. Gebruikt de query SELECT en FROM.
     * @return Hoofdgroepen
     */
    public ArrayList<Hoofdgroep> getAll() {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String query = SELECT + FROM;
        try {
            connection = daoInstance.getConnection();
            preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            return buildResult(resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    /**
     * Een hoofdgroep toevoegen in de database.
     * @param hoofdgroep
     */
    public void insert(Hoofdgroep hoofdgroep) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {

            connection = daoInstance.getConnection();
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, hoofdgroep.getPublicatie_niveau());
            preparedStatement.setString(2, hoofdgroep.getHoofdgroepOmschrijving());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Meerdere hoofdgroepen toevoegen in de database. Hier wordt een arraylist door geloopt.
     * @param hoofdgroepList
     * @param task
     */
    public void insertMultiple(ArrayList<Hoofdgroep> hoofdgroepList, BedrijfslevenImporterenController task) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = daoInstance.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(INSERT);

            for (Hoofdgroep hoofdgroep : hoofdgroepList) {
                preparedStatement.setString(1, hoofdgroep.getPublicatie_niveau());
                preparedStatement.setString(2, hoofdgroep.getHoofdgroepOmschrijving());
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
            connection.commit();
            task.setCurrentRow();
            task.updateProgressBar();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Update een hoofdgroep
     * @param hoofdgroep
     */
    public void update(Hoofdgroep hoofdgroep) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = daoInstance.getConnection();
            // Statements allow to issue SQL queries to the database
            preparedStatement = connection.prepareStatement(UPDATE + " WHERE publicatie_niveau = ?");
            preparedStatement.setString(1, hoofdgroep.getHoofdgroepOmschrijving());
            preparedStatement.setString(2, hoofdgroep.getPublicatie_niveau());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Een hoofdgroep verwijderen
     * @param hoofdgroep
     */
    public void delete(Hoofdgroep hoofdgroep) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = daoInstance.getConnection();
            // Statements allow to issue SQL queries to the database
            preparedStatement = connection.prepareStatement(DELETE + " WHERE publicatie_niveau = ?");
            preparedStatement.setString(1, hoofdgroep.getPublicatie_niveau());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Een hoofdgroep wordt hier gemaakt.
     * @param resultSet
     * @return
     */
    public ArrayList<Hoofdgroep> buildResult(ResultSet resultSet) {
        ArrayList<Hoofdgroep> result = new ArrayList<>();
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    Hoofdgroep hoofdgroep;
                    //int hoofdgroepID = resultSet.getInt(1);
                    String publicatie_niveau = resultSet.getString(1);
                    String omschrijving = resultSet.getString(2);

                    hoofdgroep = new Hoofdgroep(publicatie_niveau, omschrijving);
                    // divide and conquer
                    result.add(hoofdgroep);
                }
            } catch (Exception e) {
                e.printStackTrace();
                //returning null zodat het programma blijft werken
                return null;
            }
        }
        return result;
    }

}
