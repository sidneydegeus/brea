package dao;

import model.Correctiefactoren;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by dennis on 16-5-2016.
 */
public class CorrectiesDAO {
    private DAO daoInstance;
    private final String SELECT_JOIN = "SELECT c.sbi, c.productie_0_banen, c.productie_1tot10_banen, c.productie_10tot100_banen, c.productie_boven_100_banen, c.tw_0_banen, c.tw_1tot10_banen, c.tw_10tot100_banen, c.tw_boven_100_banen, \n" +
            "b.sbi_varchar";
    private final String FROM = " FROM public.correctiefactoren c ";
    private final String LEFT_JOIN = " LEFT JOIN bedrijfsleven b ON c.sbi = b.sbi ";
    private final String SELECT = "SELECT sbi, productie_0_banen, productie_1tot10_banen, productie_10tot100_banen, productie_boven_100_banen, tw_0_banen, tw_1tot10_banen, tw_10tot100_banen, tw_boven_100_banen ";
    private final String FROM_WHERE = "FROM public.correctiefactoren WHERE ";
    private final String WHERE = " WHERE ";
    private final String FROM_ORDER_BY = "FROM public.correctiefactoren ORDER BY sbi;";
    private final String ORDER_BY_SBI = "ORDER BY c.sbi;";
    private final String INSERT = "INSERT INTO public.correctiefactoren (sbi, productie_0_banen, productie_1tot10_banen, productie_10tot100_banen, productie_boven_100_banen, tw_0_banen, tw_1tot10_banen, " +
        "tw_10tot100_banen, tw_boven_100_banen) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private final String UPDATE = "UPDATE public.correctiefactoren SET sbi = ?, productie_0_banen = ?,productie_1tot10_banen = ? , productie_10tot100_banen = ? , productie_boven_100_banen = ?, tw_0_banen = ?" +
            ",tw_1tot10_banen = ? , tw_10tot100_banen = ? , tw_boven_100_banen = ?";
    private final String DELETE = "DELETE FROM public.correctiefactoren";

    public CorrectiesDAO() {
        daoInstance = DAO.getInstance();
    }

    /**
     * Get all correctiefactoren from database, orderer by sbi. The sbi_varchar is retrieved from de db for getting the original sbi.
     * @return Correctiefactoren
     */
    public ArrayList<Correctiefactoren> getAll() {
        String query = SELECT_JOIN + FROM + LEFT_JOIN + ORDER_BY_SBI;
        try {
            Connection connection = daoInstance.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            return buildResult(resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get correctiefactor by id
     * @param sbi
     * @return Correctiefactoren
     */
    public ArrayList<Correctiefactoren> getBySBI(int sbi) {
        ArrayList<Correctiefactoren> correctiefactorenlist = null;
        String query = SELECT_JOIN +  FROM + LEFT_JOIN + WHERE + " c.sbi = ?;";
        Connection connection = null;

        PreparedStatement preparedStatement = null;
        try {
            connection = daoInstance.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, sbi);
            ResultSet resultSet = preparedStatement.executeQuery();
            correctiefactorenlist = buildResult(resultSet);
        } catch(Exception e) {
            e.printStackTrace();
        }finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return correctiefactorenlist;
    }

    /**
     * Update a factoren correcties
     * @param correctiefactoren
     */
    public void update(Correctiefactoren correctiefactoren) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;

        String stringSbi = "";

        char[] splitSbi = correctiefactoren.getSbiString().toCharArray();
        if (splitSbi.length == 1) {
            stringSbi = "0" + correctiefactoren.getSbi();
        } else {
            stringSbi = correctiefactoren.getSbiString();;
        }
        int newSbi = Integer.parseInt("1" + stringSbi);

        try {
            connection = daoInstance.getConnection();
            // Statements allow to issue SQL queries to the database
            preparedStatement = connection.prepareStatement(UPDATE + " WHERE sbi = " + newSbi);
            preparedStatement.setInt(1, newSbi);
            preparedStatement.setDouble(2, correctiefactoren.getBanen0Productie());
            preparedStatement.setDouble(3, correctiefactoren.getBanen1Tot10Productie());
            preparedStatement.setDouble(4, correctiefactoren.getBanen10Tot100Productie());
            preparedStatement.setDouble(5, correctiefactoren.getBanen100Productie());
            preparedStatement.setDouble(6, correctiefactoren.getBanen0Tw());
            preparedStatement.setDouble(7, correctiefactoren.getBanen1Tot10Tw());
            preparedStatement.setDouble(8, correctiefactoren.getBanen10Tot100Tw());
            preparedStatement.setDouble(9, correctiefactoren.getBanen100Tw());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Insert a correctiefactoren
     * @param correctiefactoren
     */
    public void insert(Correctiefactoren correctiefactoren) {
        String stringSbi = "";

        char[] splitSbi = correctiefactoren.getSbiString().toCharArray();
        if (splitSbi.length == 1) {
            stringSbi = "0" + correctiefactoren.getSbiString();
        } else {
            stringSbi = correctiefactoren.getSbiString();;
        }
        int newSbi = Integer.parseInt("1" + stringSbi);

        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = daoInstance.getConnection();
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setInt(1, newSbi);
            preparedStatement.setDouble(2, correctiefactoren.getBanen0Productie());
            preparedStatement.setDouble(3, correctiefactoren.getBanen1Tot10Productie());
            preparedStatement.setDouble(4, correctiefactoren.getBanen10Tot100Productie());
            preparedStatement.setDouble(5, correctiefactoren.getBanen100Productie());
            preparedStatement.setDouble(6, correctiefactoren.getBanen0Tw());
            preparedStatement.setDouble(7, correctiefactoren.getBanen1Tot10Tw());
            preparedStatement.setDouble(8, correctiefactoren.getBanen10Tot100Tw());
            preparedStatement.setDouble(9, correctiefactoren.getBanen100Tw());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Delete a correctiefactoren in db
     * @param correctiefactoren
     */
    public void delete(Correctiefactoren correctiefactoren) {
        String stringSbi = "";

        char[] splitSbi = correctiefactoren.getSbiString().toCharArray();
        if (splitSbi.length == 1) {
            stringSbi = "0" + correctiefactoren.getSbiString();
        } else {
            stringSbi = correctiefactoren.getSbiString();;
        }
        int newSbi = Integer.parseInt("1" + stringSbi);
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = daoInstance.getConnection();
            // Statements allow to issue SQL queries to the database
            preparedStatement = connection.prepareStatement(DELETE + " WHERE sbi = " + newSbi);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Build the result
     * @param resultSet
     * @return
     */
    private ArrayList<Correctiefactoren> buildResult(ResultSet resultSet) {
        ArrayList<Correctiefactoren> result = new ArrayList<>();
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    Correctiefactoren correctie;
                    String sbiString = resultSet.getString(10);
                    int sbi = resultSet.getInt(1);
                    double pd_0Banen = resultSet.getDouble(2);
                    double pd_1T10 = resultSet.getDouble(3);
                    double pd_10T100 = resultSet.getDouble(4);
                    double pd_100 = resultSet.getDouble(5);
                    double tw_0Banen = resultSet.getDouble(6);
                    double tw_1T10 = resultSet.getDouble(7);
                    double tw_10T100 = resultSet.getDouble(8);
                    double tw_100 = resultSet.getDouble(9);
                    String sbiVarchar = resultSet.getString(10);
                    correctie = new Correctiefactoren(sbi, sbiString, pd_0Banen, pd_1T10, pd_10T100, pd_100, tw_0Banen, tw_1T10, tw_10T100, tw_100);
                    // divide and conquer
                    result.add(correctie);
                }
            } catch (Exception e) {
                e.printStackTrace();
                //returning null zodat het programma blijft werken
                return null;
            }
        }
        return result;
    }
}
