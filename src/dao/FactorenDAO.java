package dao;

import model.Factoren;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Edited by: Dennis, Sidney
 */
public class FactorenDAO {
    private DAO daoInstance;
    private final String INSERT = " INSERT INTO public.bedrijfsleven_factoren (" +
                "sbi, " +           //1
                "jaar, " +          //2
                "prod_totaal, " +   //3
                "prod0, " +         //4
                "prod1_10 , " +     //5
                "prod0_10, " +      //6
                "prod10_100, " +    //7
                "prod0_19, " +      //8
                "prod20_100, " +    //9
                "prod0_100, " +     //10
                "prod100, " +       //11
                "tw_totaal, " +     //12
                "tw0, " +           //13
                "tw1_10, " +        //14
                "tw0_10, " +        //15
                "tw10_100, " +      //16
                "tw0_19, " +        //17
                "tw20_100, " +      //18
                "tw0_100, " +       //19
                "tw100" +           //20
            ")" +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private final String SELECT = " SELECT " +
            "bf.sbi, " +           //1
            "bf.jaar, " +          //2
            "bf.prod_totaal, " +   //3
            "bf.prod0, " +         //4
            "bf.prod1_10 , " +     //5
            "bf.prod0_10, " +      //6
            "bf.prod10_100, " +    //7
            "bf.prod0_19, " +      //8
            "bf.prod20_100, " +    //9
            "bf.prod0_100, " +     //10
            "bf.prod100, " +       //11
            "bf.tw_totaal, " +     //12
            "bf.tw0, " +           //13
            "bf.tw1_10, " +        //14
            "bf.tw0_10, " +        //15
            "bf.tw10_100, " +      //16
            "bf.tw0_19, " +        //17
            "bf.tw20_100, " +      //18
            "bf.tw0_100, " +       //19
            "bf.tw100, " +         //20
            "b.sbi_varchar, " +
            "b.publicatie_niveau ";
    private final String FROM = "FROM public.bedrijfsleven_factoren bf " +
            "LEFT JOIN bedrijfsleven b ON b.sbi = bf.sbi;";

    public FactorenDAO() {
        daoInstance = DAO.getInstance();
    }

    /**
     * Get all factoren
     * @return Factoren
     */
    public ArrayList<Factoren> getAll() {
        String query = SELECT + FROM;
        try {
            Connection connection = daoInstance.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            return buildResult(resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getAllByJaar() {

    }

    /**
     * Insert multiple factoren to the db
     * @param factoren
     */
    public void insertMultiple(ArrayList<Factoren> factoren) {
        Connection connection = daoInstance.getConnection();
        for (Factoren factor : factoren) {
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT);

                preparedStatement.setInt(1, factor.getSBI());
                preparedStatement.setInt(2, factor.getJaar());
                preparedStatement.setDouble(3, factor.getProdTotaal());
                preparedStatement.setDouble(4, factor.getProd0());
                preparedStatement.setDouble(5, factor.getProd1To10());
                preparedStatement.setNull(6, java.sql.Types.DOUBLE);        // 0 to 10
                preparedStatement.setDouble(7, factor.getProd10To100());
                preparedStatement.setNull(8, java.sql.Types.DOUBLE);        // 0 to 19
                preparedStatement.setNull(9, java.sql.Types.DOUBLE);        // 20 to 100
                preparedStatement.setNull(10, java.sql.Types.DOUBLE);       // 0 to 100
                preparedStatement.setDouble(11, factor.getProd100());
                preparedStatement.setDouble(12, factor.getTwTotaal());
                preparedStatement.setDouble(13, factor.getTw0());
                preparedStatement.setDouble(14, factor.getTw1To10());
                preparedStatement.setNull(15, java.sql.Types.DOUBLE);       // 0 to 10
                preparedStatement.setDouble(16, factor.getTw10To100());
                preparedStatement.setNull(17, java.sql.Types.DOUBLE);       // 0 to 19
                preparedStatement.setNull(18, java.sql.Types.DOUBLE);       // 20 to 100
                preparedStatement.setNull(19, java.sql.Types.DOUBLE);       // 0 to 100
                preparedStatement.setDouble(20, factor.getTw100());

                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Delete all factoren
     */
    public void deleteAll() {
        Connection connection = daoInstance.getConnection();
        PreparedStatement preparedStatement = null;
        String query = "DELETE FROM public.bedrijfsleven_factoren";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Build the result
     * @param resultSet
     * @return
     */
    public ArrayList<Factoren> buildResult(ResultSet resultSet) {
        ArrayList<Factoren> result = new ArrayList<>();
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    int sbi = resultSet.getInt(1);
                    int jaar = resultSet.getInt(2);
                    double prodTotaal = resultSet.getDouble(3);
                    double prod0 = resultSet.getDouble(4);
                    double prod1To10 = resultSet.getDouble(5);
                    double prod0To10 = resultSet.getDouble(6);
                    double prod10To100 = resultSet.getDouble(7);
                    double prod0To19 = resultSet.getDouble(8);
                    double prod20To100 = resultSet.getDouble(9);
                    double prod0To100 = resultSet.getDouble(10);
                    double prod100 = resultSet.getDouble(11);
                    double twTotaal = resultSet.getDouble(12);
                    double tw0 = resultSet.getDouble(13);
                    double tw1To10 = resultSet.getDouble(14);
                    double tw0To10 = resultSet.getDouble(15);
                    double tw10To100 = resultSet.getDouble(16);
                    double tw0To19 = resultSet.getDouble(17);
                    double tw20To100 = resultSet.getDouble(18);
                    double tw0To100 = resultSet.getDouble(19);
                    double tw100 = resultSet.getDouble(20);
                    String sbiString = resultSet.getString(21);
                    String publicatieNiveau = resultSet.getString(22);

                    Factoren factoren = new Factoren(
                            sbi,
                            jaar,
                            prodTotaal,
                            prod0,
                            prod1To10,
                            prod0To10,
                            prod10To100,
                            prod0To19,
                            prod20To100,
                            prod0To100,
                            prod100,
                            twTotaal,
                            tw0,
                            tw1To10,
                            tw0To10,
                            tw10To100,
                            tw0To19,
                            tw20To100,
                            tw0To100,
                            tw100,
                            sbiString,
                            publicatieNiveau);
                    result.add(factoren);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return result;
    }
}
