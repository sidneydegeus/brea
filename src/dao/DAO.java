package dao;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

/**
 * Edited By Sidney
 */
public class DAO {

    private static DAO instance = null;

    private DAO() {
        Connection connection = getLocalhostConnection();
        if (!databaseExists(connection)) {
            createUser(connection);
            createDatabase(connection);
            performSqlScript();
        }
        try {
            connection.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static DAO getInstance() {
        if (instance == null) {
            instance = new DAO();
        }
        return instance;
    }

    public Connection getConnection() {
        String user = "brea_user";
        String password = "admin";
        Connection connection = null;

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost/brea?"
                            + "user=" + user
                            + "&password=" + password);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private boolean databaseExists(Connection connection) {
        boolean exists = false;
        String query = "SELECT 1 AS result FROM pg_database\n" +
                "WHERE datname='brea';";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet =  preparedStatement.executeQuery();

            //iterate each catalog in the ResultSet
            if (resultSet.next()) {
                exists = true;
            }
            resultSet.close();
        } catch (SQLException e) {
            //e.printStackTrace();
            System.out.println("brea bestaat niet");
        }
        return exists;
    }

    private void createDatabase(Connection connection) {
        String query = "CREATE DATABASE brea;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
            System.out.print("is iets fout gegaan met aanmaken van de database");
        }
    }

    private void performSqlScript() {
        String delimiter = ";";
        Scanner scanner;
        Connection connection = getConnection();
        InputStream file = DAO.class.getClassLoader().getResourceAsStream("brea_database_postgres.sql");
        scanner = new Scanner(file).useDelimiter(delimiter);

        try {
            connection.setAutoCommit(false);
            // Loop through the SQL file statements
            Statement currentStatement = null;
            while(scanner.hasNext()) {
                // Get statement
                String rawStatement = scanner.next() + delimiter;
                try {
                    // Execute statement
                    currentStatement = connection.createStatement();
                    currentStatement.execute(rawStatement);
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    // Release resources
                    if (currentStatement != null) {
                        try {
                            currentStatement.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                    currentStatement = null;
                }
            }
            scanner.close();
            connection.commit();
        } catch(Exception e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            System.out.println("er iets iets fout gegaan met het uitvoeren van het sql script");
            e.printStackTrace();
        }
    }

    private void createUser(Connection connection) {
        String query = "DO\n" +
                "$body$\n" +
                "BEGIN\n" +
                "   IF NOT EXISTS (\n" +
                "      SELECT *\n" +
                "      FROM   pg_catalog.pg_user\n" +
                "      WHERE  usename = 'brea_user') THEN\n" +
                "\n" +
                "      CREATE ROLE brea_user LOGIN PASSWORD 'admin';\n" +
                "      ALTER ROLE brea_user WITH superuser;" +
                "   END IF;\n" +
                "END\n" +
                "$body$;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
            System.out.println("Er is iets fout gegaan met een nieuwe user aanmaken");
        }
    }

    private Connection getLocalhostConnection() {
        Connection connection = null;
        try {

            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost/";
            Properties properties = new Properties();
            properties.setProperty("user","postgres");
            connection = DriverManager.getConnection(url, properties);
        } catch (SQLException e) {
            e.printStackTrace();
            editPg_hba();
            connection = getLocalhostConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void editPg_hba() {
        String dir = "Program Files\\PostgreSQL\\9.5\\data\\";
        String fileName = "pg_hba.conf";
        File[] paths;
        paths = File.listRoots();
        File file = null;
        for (int i = 0; i < paths.length; ++i) {
            if (new File(paths[i] + dir + fileName).exists()) {
                file = new File(paths[i] + dir + fileName);
            }
        }
        BufferedReader bufferedReader;
        ArrayList<String> fileLines = new ArrayList<>();
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
        } catch (IOException e1) {
            System.out.println("localhost bestaat niet, is postgres wel geinstaleerd?");
            e1.printStackTrace();
            return;
        }

        boolean reachedIPv4 = false;
        String currentRow = null;
        try {
            while((currentRow = bufferedReader.readLine()) != null) {
                // Get statement
                if (reachedIPv4) {
                    String postgresHost = "host \t all \t\t\t postgres \t\t 127.0.0.1/32 \t\t\t trust";
                    String breauserHost = "host \t all \t\t\t brea_user \t\t 127.0.0.1/32 \t\t\t md5";
                    fileLines.add(postgresHost);
                    fileLines.add(breauserHost);
                    reachedIPv4 = false;
                } else {
                    fileLines.add(currentRow);
                }
                if (currentRow.equals("# IPv4 local connections:")) {
                    reachedIPv4 = true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        BufferedWriter bufferedWriter;
        try {
            bufferedWriter = new BufferedWriter( new FileWriter(file));
        } catch (IOException e1) {
            System.out.println("localhost bestaat niet, is postgres wel geinstaleerd?");
            e1.printStackTrace();
            return;
        }
        try {
            for (String str : fileLines) {
                bufferedWriter.write(str);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
