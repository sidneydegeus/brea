package dao;

import controller.bedrijfsleven.BedrijfslevenImporterenController;
import javafx.concurrent.Task;
import model.*;
import model.factory.BedrijfslevenFactory;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Sidney
 * Edited by Jordan
 */
public class BedrijfslevenDAO {

    private DAO daoInstance;

    private final String SELECT = "SELECT " +
            "b.sbi, b.parent_sbi, b.omschrijving, b.cbs_omschrijving, b.regelcode_id, b.publicatie_niveau, b.sbi_varchar, " +
            "h.publicatie_niveau, h.omschrijving " +
            "FROM public.bedrijfsleven b " +
            "LEFT JOIN hoofdgroep h ON b.publicatie_niveau = h.publicatie_niveau ";

    private final String UPDATE = "UPDATE public.bedrijfsleven SET " +
            "omschrijving = ?, " +
            "cbs_omschrijving = ?, " +
            "regelcode_id = ?, " +
            "publicatie_niveau = ? " +
            "WHERE sbi = ?";

    private final String INSERT = "INSERT INTO public.bedrijfsleven " +
            "(sbi, parent_sbi, omschrijving, cbs_omschrijving, regelcode_id, publicatie_niveau, sbi_varchar) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?)";

    public BedrijfslevenDAO() {
        daoInstance = DAO.getInstance();
    }

    public ArrayList<Bedrijfsleven> getAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Bedrijfsleven> bedrijfslevenList = null;
        String query = "WITH RECURSIVE childSbi AS ( " + SELECT +
                "UNION " + SELECT + "JOIN childSbi ON b.parent_sbi = childSbi.sbi " +
                ") SELECT * FROM childSbi";
        try {
            connection = daoInstance.getConnection();
            preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            bedrijfslevenList = buildResult(resultSet);
        } catch(Exception e) {
            e.printStackTrace();
        }finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bedrijfslevenList;
    }

    public ArrayList<Bedrijfsleven> getBySbi(int sbi) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Bedrijfsleven> bedrijfslevenList = null;
        String query = SELECT + "WHERE b.sbi = ?";
        try {
            connection = daoInstance.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, sbi);
            ResultSet resultSet = preparedStatement.executeQuery();
            bedrijfslevenList = buildResult(resultSet);
        } catch(Exception e) {
            e.printStackTrace();
        }finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bedrijfslevenList;
    }

    public ArrayList<Bedrijfsleven> getByParentSbi(int parentSbi) {
        ArrayList<Bedrijfsleven> bedrijfslevenList = null;
        String query = SELECT + "WHERE b.parent_sbi = ?";
        Connection connection = daoInstance.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, parentSbi);
            ResultSet resultSet = preparedStatement.executeQuery();
            bedrijfslevenList = buildResult(resultSet);
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bedrijfslevenList;
    }

    public ArrayList<Bedrijfsleven> getSbiWithNoChildren() {
        ArrayList<Bedrijfsleven> bedrijfslevenList = null;
        String query = "SELECT\n" +
                "    b.sbi, b.parent_sbi, b.omschrijving, b.cbs_omschrijving, b.regelcode_id, b.publicatie_niveau, b.sbi_varchar,\n" +
                "    h.publicatie_niveau, h.omschrijving\n" +
                "    FROM    public.bedrijfsleven b\n" +
                "    LEFT JOIN hoofdgroep h ON b.publicatie_niveau = h.publicatie_niveau\n" +
                "    WHERE   NOT EXISTS (\n" +
                "              SELECT * \n" +
                "              FROM   public.bedrijfsleven s\n" +
                "              WHERE  s.parent_sbi = b.sbi\n" +
                ")";
        try {
            Connection connection = daoInstance.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            bedrijfslevenList = buildResult(resultSet);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return bedrijfslevenList;
    }

    public ArrayList<Bedrijfsleven> getAllParentSbi(int sbi) {
        ArrayList<Bedrijfsleven> bedrijfslevenList = null;
        String query = "WITH RECURSIVE parentSbi AS ( " +
                SELECT +  "WHERE b.sbi = ? " +
                "UNION " + SELECT + "JOIN parentSbi ps ON ps.parent_sbi = b.sbi " +
                ") SELECT * FROM parentSbi WHERE sbi <> ? ORDER BY sbi desc;";
        Connection connection = daoInstance.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, sbi);
            preparedStatement.setInt(2, sbi);
            ResultSet resultSet = preparedStatement.executeQuery();
            bedrijfslevenList = buildResult(resultSet);
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bedrijfslevenList;
    }

    public ArrayList<Bedrijfsleven> getAllChildSbi(int sbi) {
        ArrayList<Bedrijfsleven> bedrijfslevenList = null;
        String query = "WITH RECURSIVE childSbi AS ( " +
                 SELECT +  "WHERE b.parent_sbi = ? " +
                "UNION ALL " + SELECT + "JOIN childSbi  ON b.parent_sbi = childSbi.sbi " +
                ") SELECT * FROM childSbi ORDER BY sbi desc";
        try {
            Connection connection = daoInstance.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, sbi);
            ResultSet resultSet = preparedStatement.executeQuery();
            bedrijfslevenList = buildResult(resultSet);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return bedrijfslevenList;
    }

    public ArrayList<Bedrijfsleven> getByRegelcodeId(int regelcodeId) {
        ArrayList<Bedrijfsleven> bedrijfslevenList = null;
        String query = SELECT + "WHERE b.regelcode_id = ?";
        try {
            Connection connection = daoInstance.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, regelcodeId);
            ResultSet resultSet = preparedStatement.executeQuery();
            bedrijfslevenList = buildResult(resultSet);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return bedrijfslevenList;
    }

    public void insert(Bedrijfsleven bedrijfsleven) {
        String query = INSERT;

        try {
            Connection connection = DAO.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, bedrijfsleven.getSbi());
            if (bedrijfsleven.getParentSbi() != 0) {
                preparedStatement.setInt(2, bedrijfsleven.getParentSbi());
            } else {
                preparedStatement.setNull(2, java.sql.Types.INTEGER);
            }
            preparedStatement.setString(3, bedrijfsleven.getOmschrijving());
            preparedStatement.setString(4, bedrijfsleven.getCbsOmschrijving());
            preparedStatement.setInt(5, bedrijfsleven.getRegelcode().getRegelcodeId());
            preparedStatement.setString(6, bedrijfsleven.getHoofdgroep().getPublicatie_niveau());
            preparedStatement.setString(7, bedrijfsleven.getSbiString());
            preparedStatement.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void insertMultiple(ArrayList<Bedrijfsleven> bedrijfsleven, BedrijfslevenImporterenController task) {
        String query = INSERT;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = daoInstance.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(query);

            for (int i = 0; i < bedrijfsleven.size(); i++) {
                preparedStatement.setInt(1, bedrijfsleven.get(i).getSbi());

                if (bedrijfsleven.get(i).getParentSbi() != 0) {
                    preparedStatement.setInt(2, bedrijfsleven.get(i).getParentSbi());
                } else {
                    preparedStatement.setNull(2, java.sql.Types.INTEGER);
                }
                preparedStatement.setString(3, bedrijfsleven.get(i).getOmschrijving());
                preparedStatement.setNull(4, Types.VARCHAR);

                RegelcodeDAO regelcodeDAO = new RegelcodeDAO();

                if(regelcodeDAO.getByRegelcodeId(bedrijfsleven.get(i).getRegelcode().getRegelcodeId()).isEmpty()){
                    preparedStatement.setNull(5, java.sql.Types.INTEGER);
                } else {
                    preparedStatement.setInt(5, bedrijfsleven.get(i).getRegelcode().getRegelcodeId());
                }


                preparedStatement.setNull(6, Types.VARCHAR);

                preparedStatement.setString(7, bedrijfsleven.get(i).getSbiString());
                try {
                    preparedStatement.addBatch();
                    preparedStatement.executeBatch();

                    task.setCurrentRow();
                    task.updateProgressBar();
                } catch (BatchUpdateException e){
                 //  e.printStackTrace();
                    task.setCurrentRow();
                    task.updateProgressBar();
                }
                connection.commit();
            }
        } catch (SQLException e) {
            //e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void update(Bedrijfsleven bedrijfsleven) {
        String query = UPDATE;
        try {
            Connection connection = daoInstance.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, bedrijfsleven.getOmschrijving());
            preparedStatement.setString(2, bedrijfsleven.getCbsOmschrijving());
            preparedStatement.setInt(3, bedrijfsleven.getRegelcode().getRegelcodeId());
            preparedStatement.setString(4, bedrijfsleven.getHoofdgroep().getPublicatie_niveau());
            preparedStatement.setInt(5, bedrijfsleven.getSbi());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateMultiple(ArrayList<Bedrijfsleven> bedrijfslevenList, BedrijfslevenImporterenController task) {
        String query = "UPDATE public.bedrijfsleven SET " +
                "cbs_omschrijving = ?, " +
                "publicatie_niveau = ? " +
                "WHERE sbi = ?";
        try {
            Connection connection = daoInstance.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            for (Bedrijfsleven bedrijfsleven : bedrijfslevenList) {
                preparedStatement.setString(1, bedrijfsleven.getCbsOmschrijving());
                preparedStatement.setString(2, bedrijfsleven.getHoofdgroep().getPublicatie_niveau());
                preparedStatement.setInt(3, bedrijfsleven.getSbi());
                preparedStatement.addBatch();
                task.setCurrentRow();
                task.updateProgressBar();
            }
            preparedStatement.executeBatch();
            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(Bedrijfsleven bedrijfsleven) {
        String query = "DELETE FROM public.bedrijfsleven WHERE sbi = ?";
        try {
            Connection connection = daoInstance.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, bedrijfsleven.getSbi());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Bedrijfsleven> getByRegelcode(int regelcode) {
        String query = SELECT + "WHERE regelcode_id = ?";
        try {
            Connection connection = daoInstance.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, regelcode);
            ResultSet resultSet = preparedStatement.executeQuery();
            return buildResult(resultSet);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Bedrijfsleven> buildResult(ResultSet resultSet) {
        ArrayList<Bedrijfsleven> result = new ArrayList<>();
        if (resultSet != null) {
            try {
                BedrijfslevenFactory bedrijfslevenFactory = new BedrijfslevenFactory();
                while (resultSet.next()) {
                    int sbi = resultSet.getInt(1);
                    int parentSbi = resultSet.getInt(2);
                    String omschrijving = resultSet.getString(3);
                    String cbsOmschrijving = resultSet.getString(4);
                    Regelcode regelcode = new Regelcode(resultSet.getInt(5));
                    Hoofdgroep hoofdgroep = new Hoofdgroep(resultSet.getString(6), resultSet.getString(8));
                    String sbiString = resultSet.getString(7);

                    Bedrijfsleven bedrijfsleven = bedrijfslevenFactory.createBedrijfsleven(
                            sbi,
                            omschrijving,
                            cbsOmschrijving,
                            regelcode,
                            hoofdgroep,
                            sbiString
                    );
                    if (bedrijfsleven != null) {
                            result.add(bedrijfsleven);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                //returning null zodat het programma blijft werken indien er errors zijn
                //return null;
            }
        }
        return result;
    }
}
