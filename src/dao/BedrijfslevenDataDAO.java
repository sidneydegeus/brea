package dao;

import controller.bedrijfsleven.BedrijfslevenImporterenController;
import model.*;

import java.sql.*;
import java.util.ArrayList;

/**
 *  Edited by Sidney, Dennis
 */

public class BedrijfslevenDataDAO {

    private DAO daoInstance;

    private final String SELECT = "SELECT " +
            "jaar, banen, opbrengsten, bedrijfskosten, inkoopwaarde_omzet, personeel_kosten, " +
            "overige_kosten, afschrijvingen_vaste_activa, bedrijfsresultaat, resultaat_belastingen " +
            "FROM public.bedrijfsleven_data ";

    private final String INSERT = "INSERT INTO public.bedrijfsleven_data (" +
            "sbi, " +
            "jaar, " +
            "banen, " +
            "opbrengsten, " +
            "bedrijfskosten, " +
            "inkoopwaarde_omzet, " +
            "personeel_kosten, " +
            "overige_kosten, " +
            "afschrijvingen_vaste_activa, " +
            "bedrijfsresultaat, " +
            "resultaat_belastingen" +
            ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private final String DELETE = "DELETE FROM public.bedrijfsleven_data WHERE sbi = ?";


    public BedrijfslevenDataDAO() {
        daoInstance = DAO.getInstance();
    }

    public ArrayList<BedrijfslevenData> getAllBySBI(int sbi) {
        ArrayList<BedrijfslevenData> bedrijfslevenDataList = new ArrayList<>();
        // first do a query to check if it has a parent sbi
        ArrayList<Bedrijfsleven> bedrijfsleven = new BedrijfslevenDAO().getByParentSbi(sbi);
        String query = SELECT + "WHERE sbi = ? ORDER BY jaar desc";
        // if there is no parent sbi, select all where parent sbi
        if (bedrijfsleven.isEmpty()) {
            Connection connection = daoInstance.getConnection();
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, sbi);
                ResultSet resultSet = preparedStatement.executeQuery();
                bedrijfslevenDataList = buildResult(resultSet, sbi);
            } catch(Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            bedrijfslevenDataList = getAllChildData(sbi);
        }
        return bedrijfslevenDataList;
    }


    public ArrayList<BedrijfslevenData> getJaarBySbi(int sbi, int jaar) {
        ArrayList<BedrijfslevenData> bedrijfslevenDataList = new ArrayList<>();
        String query = SELECT + "WHERE sbi = ? AND jaar = ?";
        try {
            Connection connection = daoInstance.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, sbi);
            preparedStatement.setInt(2, jaar);
            ResultSet resultSet = preparedStatement.executeQuery();
            bedrijfslevenDataList = buildResult(resultSet, sbi);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return bedrijfslevenDataList;
    }

    public ArrayList<BedrijfslevenData> getAllChildData(int sbi) {
        ArrayList<BedrijfslevenData> bedrijfslevenDataList = null;
        String query = "WITH RECURSIVE childData AS\n" +
                "(\n" +
                    "SELECT\n" +
                        "b.sbi, b.parent_sbi,\n" +
                        "d.jaar, d.banen, d.opbrengsten, d.bedrijfskosten, d.inkoopwaarde_omzet, d.personeel_kosten,\n" +
                        "d.overige_kosten, d.afschrijvingen_vaste_activa, d.bedrijfsresultaat, d.resultaat_belastingen\n" +
                    "FROM public.bedrijfsleven b\n" +
                    "LEFT JOIN bedrijfsleven_data d ON d.sbi = b.sbi\n" +
                    "WHERE b.parent_sbi = ?\n" +
                    "UNION ALL\n" +
                    "SELECT\n" +
                        "b.sbi, b.parent_sbi,\n" +
                        "d.jaar, d.banen, d.opbrengsten, d.bedrijfskosten, d.inkoopwaarde_omzet, d.personeel_kosten,\n" +
                        "d.overige_kosten, d.afschrijvingen_vaste_activa, d.bedrijfsresultaat, d.resultaat_belastingen\n" +
                    "FROM public.bedrijfsleven b\n" +
                    "LEFT JOIN bedrijfsleven_data d ON d.sbi = b.sbi\n" +
                    "JOIN childData  ON b.parent_sbi = childData.sbi\n" +
                ")\n" +
                "SELECT\n" +
                    "jaar, sum(banen) AS banen, sum(opbrengsten) AS opbgrengsten, sum(bedrijfskosten) AS bedrijfskosten, \n" +
                    "sum(inkoopwaarde_omzet) AS inkoopwaarde_omzet, sum(personeel_kosten) AS personeel_kosten,\n" +
                    "sum(overige_kosten) AS overige_kosten, sum(afschrijvingen_vaste_activa) AS afschrijvingen_vaste_activa, \n" +
                    "sum(bedrijfsresultaat) AS bedrijfsresultaat, sum(resultaat_belastingen) AS resultaat_belastingen\n" +
                "FROM childData WHERE jaar IS NOT NULL\n" +
                "GROUP BY jaar " +
                "ORDER BY jaar desc";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoInstance.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, sbi);
            ResultSet resultSet = preparedStatement.executeQuery();
            bedrijfslevenDataList = buildResult(resultSet, sbi);
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bedrijfslevenDataList;
    }

    public ArrayList<BedrijfslevenData> getAllHoofdgroepData(String publicatie_niveau) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ArrayList<BedrijfslevenData> bedrijfslevenDataList = null;
        String query = "WITH RECURSIVE childData AS\n" +
                "(\n" +
                "   SELECT\n" +
                "\tb.sbi, b.parent_sbi,\n" +
                "\td.jaar, d.banen, d.opbrengsten, d.bedrijfskosten, d.inkoopwaarde_omzet, d.personeel_kosten,\n" +
                "\td.overige_kosten, d.afschrijvingen_vaste_activa, d.bedrijfsresultaat, d.resultaat_belastingen\n" +
                "   FROM public.hoofdgroep h\n" +
                "   JOIN bedrijfsleven b ON b.publicatie_niveau = h.publicatie_niveau\n" +
                "   LEFT JOIN bedrijfsleven_data d ON d.sbi = b.sbi\n" +
                "   WHERE h.publicatie_niveau = ?\n" +
                "   UNION\n" +
                "   SELECT\n" +
                "\tb.sbi, b.parent_sbi,\n" +
                "\td.jaar, d.banen, d.opbrengsten, d.bedrijfskosten, d.inkoopwaarde_omzet, d.personeel_kosten,\n" +
                "\td.overige_kosten, d.afschrijvingen_vaste_activa, d.bedrijfsresultaat, d.resultaat_belastingen\n" +
                "   FROM public.hoofdgroep h\n" +
                "   JOIN bedrijfsleven b ON b.publicatie_niveau = h.publicatie_niveau\n" +
                "   LEFT JOIN bedrijfsleven_data d ON d.sbi = b.sbi\n" +
                "   JOIN childData ON b.parent_sbi = childData.sbi\n" +
                ")\n" +
                "SELECT\n" +
                "\tjaar, sum(banen) AS banen, sum(opbrengsten) AS opbgrengsten, sum(bedrijfskosten) AS bedrijfskosten, \n" +
                "        sum(inkoopwaarde_omzet) AS inkoopwaarde_omzet, sum(personeel_kosten) AS personeel_kosten,\n" +
                "        sum(overige_kosten) AS overige_kosten, sum(afschrijvingen_vaste_activa) AS afschrijvingen_vaste_activa,\n" +
                "        sum(bedrijfsresultaat) AS bedrijfsresultaat, sum(resultaat_belastingen) AS resultaat_belastingen\n" +
                "        FROM childData WHERE jaar IS NOT NULL\n" +
                "        GROUP BY jaar\n" +
                "        ORDER BY jaar desc";
        try {
            connection = daoInstance.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, publicatie_niveau);
            ResultSet resultSet = preparedStatement.executeQuery();
            bedrijfslevenDataList = buildResult(resultSet, 0);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bedrijfslevenDataList;
    }

    public void insert(BedrijfslevenData bedrijfslevenData) {
        String query = INSERT;
        try {
            Connection connection = DAO.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, bedrijfslevenData.getSbi());
            preparedStatement.setInt(2, bedrijfslevenData.getJaar());
            preparedStatement.setDouble(3, bedrijfslevenData.getBanen());
            preparedStatement.setDouble(4, bedrijfslevenData.getOpbrengsten());
            preparedStatement.setDouble(5, bedrijfslevenData.getBedrijfskosten());
            preparedStatement.setDouble(6, bedrijfslevenData.getInkoopwaardeOmzet());
            preparedStatement.setDouble(7, bedrijfslevenData.getPersoneelKosten());
            preparedStatement.setDouble(8, bedrijfslevenData.getOverigeKosten());
            preparedStatement.setDouble(9, bedrijfslevenData.getAfschrijvingenVasteActiva());
            preparedStatement.setDouble(10, bedrijfslevenData.getBedrijfsResultaat());
            preparedStatement.setDouble(11, bedrijfslevenData.getResultaatBelastingen());

            preparedStatement.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void insertMultiple(ArrayList<BedrijfslevenData> bedrijfslevenDataList, BedrijfslevenImporterenController task) {
        String query = INSERT;
        Connection connection = daoInstance.getConnection();

        for (BedrijfslevenData bedrijfslevenData : bedrijfslevenDataList) {
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(query);

                preparedStatement.setInt(1, bedrijfslevenData.getSbi());
                preparedStatement.setInt(2, bedrijfslevenData.getJaar());
                preparedStatement.setDouble(3, bedrijfslevenData.getBanen());
                preparedStatement.setDouble(4, bedrijfslevenData.getOpbrengsten());
                preparedStatement.setDouble(5, bedrijfslevenData.getBedrijfskosten());
                preparedStatement.setDouble(6, bedrijfslevenData.getInkoopwaardeOmzet());
                preparedStatement.setDouble(7, bedrijfslevenData.getPersoneelKosten());
                preparedStatement.setDouble(8, bedrijfslevenData.getOverigeKosten());
                preparedStatement.setDouble(9, bedrijfslevenData.getAfschrijvingenVasteActiva());
                preparedStatement.setDouble(10, bedrijfslevenData.getBedrijfsResultaat());
                preparedStatement.setDouble(11, bedrijfslevenData.getResultaatBelastingen());

                task.setCurrentRow();
                task.updateProgressBar();

                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                System.out.println("duplicate entry");
                //e.printStackTrace();
            }
        }
    }

    public void update(BedrijfslevenData bedrijfslevenData) {
        String query = "UPDATE public.bedrijfsleven_data SET " +
                "banen = ?, " +
                "opbrengsten = ?, " +
                "bedrijfskosten = ?, " +
                "inkoopwaarde_omzet = ?, " +
                "personeel_kosten = ?, " +
                "overige_kosten = ?, " +
                "afschrijvingen_vaste_activa = ?, " +
                "bedrijfsresultaat = ?, " +
                "resultaat_belastingen = ? " +
                "WHERE sbi = ?";
        try {
            Connection connection = DAO.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setDouble(1, bedrijfslevenData.getBanen());
            preparedStatement.setDouble(2, bedrijfslevenData.getOpbrengsten());
            preparedStatement.setDouble(3, bedrijfslevenData.getBedrijfskosten());
            preparedStatement.setDouble(4, bedrijfslevenData.getInkoopwaardeOmzet());
            preparedStatement.setDouble(5, bedrijfslevenData.getPersoneelKosten());
            preparedStatement.setDouble(6, bedrijfslevenData.getOverigeKosten());
            preparedStatement.setDouble(7, bedrijfslevenData.getAfschrijvingenVasteActiva());
            preparedStatement.setDouble(8, bedrijfslevenData.getBedrijfsResultaat());
            preparedStatement.setDouble(9, bedrijfslevenData.getResultaatBelastingen());

            // where
            preparedStatement.setInt(10, bedrijfslevenData.getSbi());
            preparedStatement.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(BedrijfslevenData bedrijfslevenData) {
        String query = DELETE + " AND jaar = ?";
        try {
            Connection connection = daoInstance.getConnection();
            // Statements allow to issue SQL queries to the database
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, bedrijfslevenData.getSbi());
            preparedStatement.setInt(2, bedrijfslevenData.getJaar());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * delete data voor sbi's van bedrijfsleven, bedrijfsleven! niet bedrijfslevenData
     * @param bedrijfslevenList
     */
    public void deleteMultiple(ArrayList<Bedrijfsleven> bedrijfslevenList) {
        String query = DELETE;
        Connection connection = daoInstance.getConnection();

        for (Bedrijfsleven bedrijfsleven : bedrijfslevenList) {
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, bedrijfsleven.getSbi());

                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private ArrayList<BedrijfslevenData> buildResult(ResultSet resultSet, int sbi) {
        ArrayList<BedrijfslevenData> result = new ArrayList<>();
        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    //int sbi = resultSet.getInt(1);
                    int jaar = resultSet.getInt(1);
                    double banen = resultSet.getDouble(2);
                    double opbrengsten = resultSet.getDouble(3);
                    double bedrijfskosten = resultSet.getDouble(4);
                    double inkoopwaardeOmzet = resultSet.getDouble(5);
                    double personeelKosten = resultSet.getDouble(6);
                    double overigeKosten = resultSet.getDouble(7);
                    double afschrijvingenVasteActiva = resultSet.getDouble(8);
                    double bedrijfsResultaat = resultSet.getDouble(9);
                    double resultaatBelastingen = resultSet.getDouble(10);

                    BedrijfslevenData bedrijfslevenData = new BedrijfslevenData(
                            sbi,
                            jaar,
                            banen,
                            opbrengsten,
                            bedrijfskosten,
                            inkoopwaardeOmzet,
                            personeelKosten,
                            overigeKosten,
                            afschrijvingenVasteActiva,
                            bedrijfsResultaat,
                            resultaatBelastingen
                    );

                    if (bedrijfslevenData != null) {
                        result.add(bedrijfslevenData);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        return result;
    }
}
