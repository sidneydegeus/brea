package dao;

import model.Bedrijfsleven;
import model.BedrijfslevenSBIVerzameling;
import model.Hoofdgroep;
import model.Regelcode;
import model.factory.BedrijfslevenFactory;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.ArrayList;

/**
 * @author Jordan Munk
 */
public class BedrijfslevenVerzamelingDAO {
    private DAO daoInstance;

    private final String SELECT = "SELECT sbi_verzameling_id, omschrijving FROM public.bedrijfsleven_sbi_verzameling";

    public BedrijfslevenVerzamelingDAO() {
        daoInstance = DAO.getInstance();
    }

    /**
     * SBI Verzameling wordt toegevoegd
     * @param bedrijfsleven
     */
    public void insert(BedrijfslevenSBIVerzameling bedrijfsleven) {
        String query = "INSERT INTO public.bedrijfsleven_sbi_verzameling " +
                "(omschrijving) " +
                "VALUES (?)";

        try {
            Connection connection = DAO.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
          //  preparedStatement.setInt(1, bedrijfsleven.getSbi());
            preparedStatement.setString(1, bedrijfsleven.getOmschrijving());
            preparedStatement.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Meerdere SBI Verzameling wordt toegevoegd
     * @param bedrijfsleven
     */
    public void insertMultiple(ArrayList<BedrijfslevenSBIVerzameling> bedrijfsleven) {
        String query = "INSERT INTO public.bedrijfsleven_sbi_verzameling " +
                "(omschrijving) " +
                "VALUES (?)";

        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = daoInstance.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(query);

            for (int i = 0; i < bedrijfsleven.size(); i++) {
         //       preparedStatement.setInt(1, bedrijfsleven.get(i).getSbi());
                preparedStatement.setString(1, bedrijfsleven.get(i).getOmschrijving());

                try {
                    preparedStatement.addBatch();
                    preparedStatement.executeBatch();

                } catch (BatchUpdateException e){
                    // e.printStackTrace();
                }
                connection.commit();
            }
        } catch (SQLException e) {
          //  e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Meerdere SBI Verzameling wordt toegevoegd (dit wordt toegevoegd in sbi_verzameling i.p.v. bedrijfsleven_sbi_verzameling).
     * @param bedrijfsleven
     */
    public void insertMultiple_sbi(ArrayList<BedrijfslevenSBIVerzameling> bedrijfsleven) {
        String query = "INSERT INTO public.sbi_verzameling " +
                "(sbi_verzameling_id, sbi) " +
                "VALUES (?, ?)";

        PreparedStatement preparedStatement = null;
        PreparedStatement preparedStatement2 = null;
        Connection connection = null;
        int verzamelingID = 0;
        try {
            connection = daoInstance.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(query);

            for (int i = 0; i < bedrijfsleven.size(); i++) {

                String queryVerzamelingID  = "SELECT sbi_verzameling_id "
                        + " FROM public.bedrijfsleven_sbi_verzameling"
                        + " WHERE omschrijving = ?";

                preparedStatement2 = connection.prepareStatement(queryVerzamelingID);
                preparedStatement2.setString(1, bedrijfsleven.get(i).getOmschrijving());
                ResultSet resultSet = preparedStatement2.executeQuery();

                while (resultSet.next()) {
                    verzamelingID = resultSet.getInt(1);
                }

                preparedStatement.setInt(1, verzamelingID);
                preparedStatement.setInt(2, bedrijfsleven.get(i).getSbi());

                try {
                    preparedStatement.addBatch();
                    preparedStatement.executeBatch();
                } catch (BatchUpdateException e){
               //      e.printStackTrace();
                }
                connection.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

