
SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;


CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;



CREATE TABLE bedrijfsleven (
    sbi integer NOT NULL,
    parent_sbi integer,
    omschrijving character varying(255) NOT NULL,
    cbs_omschrijving character varying(255),
    regelcode_id integer,
    publicatie_niveau character varying,
    sbi_varchar character varying(7)
);


ALTER TABLE bedrijfsleven OWNER TO postgres;


CREATE TABLE bedrijfsleven_data (
    sbi integer NOT NULL,
    jaar integer NOT NULL,
    banen double precision,
    opbrengsten double precision,
    bedrijfskosten double precision,
    inkoopwaarde_omzet double precision,
    personeel_kosten double precision,
    overige_kosten double precision,
    afschrijvingen_vaste_activa double precision,
    bedrijfsresultaat double precision,
    resultaat_belastingen double precision
);


ALTER TABLE bedrijfsleven_data OWNER TO postgres;


CREATE TABLE bedrijfsleven_factoren (
    sbi integer NOT NULL,
    jaar integer NOT NULL,
    prod_totaal double precision,
    prod0 double precision,
    prod1_10 double precision,
    prod0_10 double precision,
    prod10_100 double precision,
    prod0_19 double precision,
    prod20_100 double precision,
    prod0_100 double precision,
    prod100 double precision,
    tw_totaal double precision,
    tw0 double precision,
    tw1_10 double precision,
    tw0_10 double precision,
    tw10_100 double precision,
    tw0_19 double precision,
    tw20_100 double precision,
    tw0_100 double precision,
    tw100 double precision
);


ALTER TABLE bedrijfsleven_factoren OWNER TO postgres;


CREATE TABLE bedrijfsleven_hoofdgroep_verzameling (
    hoofdgroep_verzameling_id integer NOT NULL,
    omschrijving character varying(255) NOT NULL
);


ALTER TABLE bedrijfsleven_hoofdgroep_verzameling OWNER TO postgres;


CREATE SEQUENCE bedrijfsleven_hoofdgroep_verzamel_hoofdgroep_verzameling_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bedrijfsleven_hoofdgroep_verzamel_hoofdgroep_verzameling_id_seq OWNER TO postgres;



ALTER SEQUENCE bedrijfsleven_hoofdgroep_verzamel_hoofdgroep_verzameling_id_seq OWNED BY bedrijfsleven_hoofdgroep_verzameling.hoofdgroep_verzameling_id;



CREATE SEQUENCE bedrijfsleven_sbi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bedrijfsleven_sbi_seq OWNER TO postgres;


ALTER SEQUENCE bedrijfsleven_sbi_seq OWNED BY bedrijfsleven.sbi;



CREATE TABLE bedrijfsleven_sbi_verzameling (
    sbi_verzameling_id integer NOT NULL,
    omschrijving character varying(255) NOT NULL
);


ALTER TABLE bedrijfsleven_sbi_verzameling OWNER TO postgres;


CREATE SEQUENCE bedrijfsleven_sbi_verzameling_sbi_verzameling_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bedrijfsleven_sbi_verzameling_sbi_verzameling_id_seq OWNER TO postgres;


ALTER SEQUENCE bedrijfsleven_sbi_verzameling_sbi_verzameling_id_seq OWNED BY bedrijfsleven_sbi_verzameling.sbi_verzameling_id;



CREATE TABLE correctiefactoren (
    sbi integer NOT NULL,
    productie_0_banen double precision,
    productie_1tot10_banen double precision,
    productie_10tot100_banen double precision,
    productie_boven_100_banen double precision,
    tw_0_banen double precision,
    tw_1tot10_banen double precision,
    tw_10tot100_banen double precision,
    tw_boven_100_banen double precision
);


ALTER TABLE correctiefactoren OWNER TO postgres;



CREATE TABLE hoofdgroep (
    publicatie_niveau character varying(2) NOT NULL,
    omschrijving character varying(255) NOT NULL
);


ALTER TABLE hoofdgroep OWNER TO postgres;


CREATE TABLE hoofdgroep_verzameling (
    hoofdgroep_verzameling_id integer NOT NULL,
    publicatie_niveau character varying NOT NULL
);


ALTER TABLE hoofdgroep_verzameling OWNER TO postgres;


CREATE TABLE regelcode (
    regelcode_id integer NOT NULL,
    activiteit character varying(255) NOT NULL
);


ALTER TABLE regelcode OWNER TO postgres;


CREATE SEQUENCE regelcode_regelcode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE regelcode_regelcode_id_seq OWNER TO postgres;


ALTER SEQUENCE regelcode_regelcode_id_seq OWNED BY regelcode.regelcode_id;



CREATE TABLE sbi_verzameling (
    sbi_verzameling_id integer NOT NULL,
    sbi integer NOT NULL
);


ALTER TABLE sbi_verzameling OWNER TO postgres;



ALTER TABLE ONLY bedrijfsleven ALTER COLUMN sbi SET DEFAULT nextval('bedrijfsleven_sbi_seq'::regclass);



ALTER TABLE ONLY bedrijfsleven_hoofdgroep_verzameling ALTER COLUMN hoofdgroep_verzameling_id SET DEFAULT nextval('bedrijfsleven_hoofdgroep_verzamel_hoofdgroep_verzameling_id_seq'::regclass);



ALTER TABLE ONLY bedrijfsleven_sbi_verzameling ALTER COLUMN sbi_verzameling_id SET DEFAULT nextval('bedrijfsleven_sbi_verzameling_sbi_verzameling_id_seq'::regclass);



ALTER TABLE ONLY regelcode ALTER COLUMN regelcode_id SET DEFAULT nextval('regelcode_regelcode_id_seq'::regclass);




SELECT pg_catalog.setval('bedrijfsleven_hoofdgroep_verzamel_hoofdgroep_verzameling_id_seq', 1, false);





SELECT pg_catalog.setval('bedrijfsleven_sbi_seq', 1, false);






SELECT pg_catalog.setval('bedrijfsleven_sbi_verzameling_sbi_verzameling_id_seq', 1, false);




SELECT pg_catalog.setval('regelcode_regelcode_id_seq', 1, false);




ALTER TABLE ONLY bedrijfsleven_data
    ADD CONSTRAINT bedrijfsleven_data_pkey PRIMARY KEY (sbi, jaar);



ALTER TABLE ONLY bedrijfsleven_factoren
    ADD CONSTRAINT bedrijfsleven_factoren_pkey PRIMARY KEY (sbi, jaar);



ALTER TABLE ONLY bedrijfsleven_hoofdgroep_verzameling
    ADD CONSTRAINT bedrijfsleven_hoofdgroep_verzameling_pkey PRIMARY KEY (hoofdgroep_verzameling_id);


ALTER TABLE ONLY bedrijfsleven
    ADD CONSTRAINT bedrijfsleven_pkey PRIMARY KEY (sbi);



ALTER TABLE ONLY bedrijfsleven_sbi_verzameling
    ADD CONSTRAINT bedrijfsleven_sbi_verzameling_pkey PRIMARY KEY (sbi_verzameling_id);



ALTER TABLE ONLY hoofdgroep_verzameling
    ADD CONSTRAINT hoofdgroep_verzameling_pkey PRIMARY KEY (hoofdgroep_verzameling_id, publicatie_niveau);



ALTER TABLE ONLY hoofdgroep
    ADD CONSTRAINT publicatie_niveau_pkey PRIMARY KEY (publicatie_niveau);



ALTER TABLE ONLY regelcode
    ADD CONSTRAINT regelcode_pkey PRIMARY KEY (regelcode_id);



ALTER TABLE ONLY correctiefactoren
    ADD CONSTRAINT sbi PRIMARY KEY (sbi);



ALTER TABLE ONLY sbi_verzameling
    ADD CONSTRAINT sbi_verzameling_pkey PRIMARY KEY (sbi_verzameling_id, sbi);




ALTER TABLE ONLY bedrijfsleven_data
    ADD CONSTRAINT bedrijfsleven_data_sbi_fk FOREIGN KEY (sbi) REFERENCES bedrijfsleven(sbi) ON DELETE CASCADE;



ALTER TABLE ONLY bedrijfsleven
    ADD CONSTRAINT bedrijfsleven_parent_sbi_fk FOREIGN KEY (parent_sbi) REFERENCES bedrijfsleven(sbi) ON DELETE CASCADE;



ALTER TABLE ONLY bedrijfsleven_factoren
    ADD CONSTRAINT fkbedrijfsle145516 FOREIGN KEY (sbi) REFERENCES bedrijfsleven(sbi);



ALTER TABLE ONLY bedrijfsleven
    ADD CONSTRAINT fkbedrijfsle205554 FOREIGN KEY (regelcode_id) REFERENCES regelcode(regelcode_id) ON DELETE SET NULL;




ALTER TABLE ONLY hoofdgroep_verzameling
    ADD CONSTRAINT fkhoofdgroep625998 FOREIGN KEY (hoofdgroep_verzameling_id) REFERENCES bedrijfsleven_hoofdgroep_verzameling(hoofdgroep_verzameling_id);



ALTER TABLE ONLY sbi_verzameling
    ADD CONSTRAINT fksbi_verzam306196 FOREIGN KEY (sbi_verzameling_id) REFERENCES bedrijfsleven_sbi_verzameling(sbi_verzameling_id);


ALTER TABLE ONLY sbi_verzameling
    ADD CONSTRAINT fksbi_verzam876996 FOREIGN KEY (sbi) REFERENCES bedrijfsleven(sbi);



ALTER TABLE ONLY hoofdgroep_verzameling
    ADD CONSTRAINT publicatie_niveau_fk FOREIGN KEY (publicatie_niveau) REFERENCES hoofdgroep(publicatie_niveau) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE ONLY bedrijfsleven
    ADD CONSTRAINT publicatie_niveau_fk FOREIGN KEY (publicatie_niveau) REFERENCES hoofdgroep(publicatie_niveau) ON UPDATE CASCADE ON DELETE SET NULL;



REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;




REVOKE ALL ON TABLE bedrijfsleven FROM PUBLIC;
REVOKE ALL ON TABLE bedrijfsleven FROM postgres;
GRANT ALL ON TABLE bedrijfsleven TO postgres;
GRANT ALL ON TABLE bedrijfsleven TO PUBLIC;



REVOKE ALL ON TABLE bedrijfsleven_data FROM PUBLIC;
REVOKE ALL ON TABLE bedrijfsleven_data FROM postgres;
GRANT ALL ON TABLE bedrijfsleven_data TO postgres;
GRANT ALL ON TABLE bedrijfsleven_data TO PUBLIC;




REVOKE ALL ON TABLE bedrijfsleven_factoren FROM PUBLIC;
REVOKE ALL ON TABLE bedrijfsleven_factoren FROM postgres;
GRANT ALL ON TABLE bedrijfsleven_factoren TO postgres;
GRANT ALL ON TABLE bedrijfsleven_factoren TO PUBLIC;



REVOKE ALL ON TABLE bedrijfsleven_hoofdgroep_verzameling FROM PUBLIC;
REVOKE ALL ON TABLE bedrijfsleven_hoofdgroep_verzameling FROM postgres;
GRANT ALL ON TABLE bedrijfsleven_hoofdgroep_verzameling TO postgres;
GRANT ALL ON TABLE bedrijfsleven_hoofdgroep_verzameling TO PUBLIC;




REVOKE ALL ON TABLE bedrijfsleven_sbi_verzameling FROM PUBLIC;
REVOKE ALL ON TABLE bedrijfsleven_sbi_verzameling FROM postgres;
GRANT ALL ON TABLE bedrijfsleven_sbi_verzameling TO postgres;
GRANT ALL ON TABLE bedrijfsleven_sbi_verzameling TO PUBLIC;




REVOKE ALL ON TABLE hoofdgroep FROM PUBLIC;
REVOKE ALL ON TABLE hoofdgroep FROM postgres;
GRANT ALL ON TABLE hoofdgroep TO postgres;
GRANT ALL ON TABLE hoofdgroep TO PUBLIC;



REVOKE ALL ON TABLE hoofdgroep_verzameling FROM PUBLIC;
REVOKE ALL ON TABLE hoofdgroep_verzameling FROM postgres;
GRANT ALL ON TABLE hoofdgroep_verzameling TO postgres;
GRANT ALL ON TABLE hoofdgroep_verzameling TO PUBLIC;




REVOKE ALL ON TABLE regelcode FROM PUBLIC;
REVOKE ALL ON TABLE regelcode FROM postgres;
GRANT ALL ON TABLE regelcode TO postgres;
GRANT ALL ON TABLE regelcode TO PUBLIC;



REVOKE ALL ON TABLE sbi_verzameling FROM PUBLIC;
REVOKE ALL ON TABLE sbi_verzameling FROM postgres;
GRANT ALL ON TABLE sbi_verzameling TO postgres;
GRANT ALL ON TABLE sbi_verzameling TO PUBLIC;



