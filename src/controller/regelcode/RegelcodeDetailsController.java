package controller.regelcode;

import controller.DetailsController;
import controller.OverzichtController;
import dao.BedrijfslevenDAO;
import dao.HoofdgroepDAO;
import dao.RegelcodeDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import model.Bedrijfsleven;
import model.Hoofdgroep;
import model.Regelcode;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by dennis on 16-5-2016.
 * Edited by Jordan on 2-6-2016.
 */
public class RegelcodeDetailsController implements Initializable, DetailsController {
    @FXML
    private TextField sbiField;
    @FXML
    private TextField regelcodeIdField;
    @FXML
    private TextField activiteitField;

    public RegelcodeDetailsController() {
    }

    /**
     * Method acts to the button "Bewerken"
     * @param actionEvent
     */
    @FXML
    public void handleBewerkButton(ActionEvent actionEvent) {
        veranderRegelcode();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informatie");
        alert.setHeaderText("Gelukt!");
        alert.setContentText("De regelcode is veranderd!");

        alert.showAndWait();
    }

    /**
     * Method acts to the button "Verwijderen"
     * @param actionEvent
     */
    @FXML
    public void handleVerwijderButton(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Informatie");
        alert.setHeaderText("De regelcode wordt verwijderd!");
        alert.setContentText("Weet u dit zeker?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            verwijderRegelcode();
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informatie");
            alert.setHeaderText("Gelukt!");
            alert.setContentText("De regelcode is verwijderd!");

            alert.showAndWait();
        }
    }

    /**
     * Method changes the Regelcode values in the database to the values of the textfields.
     */
    public void veranderRegelcode() {
        Regelcode regelcode = new Regelcode(Integer.valueOf(regelcodeIdField.getText()), activiteitField.getText());
        RegelcodeDAO regelcodeDAO = new RegelcodeDAO();
        regelcodeDAO.update(regelcode);
    }

    /**
     * Method deletes the current selected Regelcode values in de the database
     */
    public void verwijderRegelcode() {
        Regelcode regelcode = new Regelcode(Integer.valueOf(regelcodeIdField.getText()), activiteitField.getText());
        RegelcodeDAO regelcodeDAO = new RegelcodeDAO();
        regelcodeDAO.delete(regelcode);
    }

    /**
     * Method sets the data in the textfields by the selected row
     * @param overzichtController
     */
    @Override
    public void setDetails(OverzichtController overzichtController) {
            Regelcode regelcode = (Regelcode) overzichtController.getSelectedRow();
            System.out.println(overzichtController.getSelectedRow());
            regelcodeIdField.setText(String.valueOf(regelcode.getRegelcodeId()));
        activiteitField.setText(regelcode.getActiviteit());
        sbiField.setDisable(true);

        BedrijfslevenDAO bedrijfslevenDAO = new BedrijfslevenDAO();
        try {
            ArrayList<Bedrijfsleven> list = bedrijfslevenDAO.getByRegelcode(regelcode.getRegelcodeId());
            sbiField.setText(String.valueOf(list.get(0).getSbiString()));

        } catch (Exception e) {
            sbiField.setText("Nog niet toegewezen");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

}
