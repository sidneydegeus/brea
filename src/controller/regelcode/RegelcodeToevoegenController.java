package controller.regelcode;

import dao.BedrijfslevenDAO;
import dao.RegelcodeDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import model.Bedrijfsleven;
import model.Regelcode;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by dennis on 16-5-2016.
 */
public class RegelcodeToevoegenController implements Initializable{
    @FXML
    TextField regelcodeIdField;

    @FXML
    TextField activiteitField;

    public RegelcodeToevoegenController() {
    }

    /**Method registrates when the voeg toe button is clicked.
     * It adds the regelcode when there isn't a same regelcode in de db.
     * @param actionEvent
     */
    @FXML
    public void handleVoegToeButton(ActionEvent actionEvent) {
        if (checkEqual() == false) {
            addRegelCode();
        } else {
            giveFailedCheckDialog(true);
        }
    }

    /**Method checks if the regelcode or activiteit is equal to the regelcode in de db. When it is equal it wil return true. When it is not equal it will return true
     * When it is equal it will popup a dialog.
      * @return boolean
     */
    public boolean checkEqual() {
        boolean equal = false;

        try {
            int regelcode = Integer.valueOf(regelcodeIdField.getText());
            String activiteit = activiteitField.getText();
            RegelcodeDAO regelcodeDAO = new RegelcodeDAO();
            ArrayList<Regelcode> regelcodes = regelcodeDAO.getAll();

            for (int i = 0; i < regelcodes.size(); i++) {
                if (Integer.valueOf(regelcodeIdField.getText()) == regelcodes.get(i).getRegelcodeId()) {
                    equal = true;
                    giveFailedCheckDialog(true);
                    break;
                } else {
                    equal = false;

                }
            }
            return equal;
        } catch (Exception e) {
            giveFailedCheckDialog(false);
            throw new UnsupportedOperationException("Regelcode is null");
        }
    }

    /**Method adds a regelcode to the db through the DAO.
     * When it is done it gives a accomplished dialog.
     */
    public void addRegelCode() {
        Regelcode regelcode = new Regelcode(Integer.valueOf(regelcodeIdField.getText()), activiteitField.getText());
        RegelcodeDAO regelcodeDAO = new RegelcodeDAO();
        regelcodeDAO.insert(regelcode);
        giveAccomplishedDialog();
    }

    /**Method makes an alertbox to inform the user that the regelcode successfully has added to the database
     *
     */
    public void giveAccomplishedDialog() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informatie");
        alert.setHeaderText("Gelukt!");
        alert.setContentText("De regelcode is succesvol toegevoegd!");
        alert.show();
    }

    /**
     * Method gives an failed add dialog. When it is equal or not the dialog has a specific dialog.
     * @param equal
     */
    public void giveFailedCheckDialog(boolean equal) {
        if (equal) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Foutmelding");
            alert.setHeaderText("Er is een foutmelding gevonden!");
            alert.setContentText("Deze regelcode is niet uniek!");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Foutmelding");
            alert.setHeaderText("Er is een foutmelding gevonden!");
            alert.setContentText("Er is een veld niet goed ingevuld!");
            alert.show();
        }

    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
