package controller.regelcode;

import controller.OverzichtController;
import dao.RegelcodeDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.Regelcode;
import org.apache.poi.ss.usermodel.Cell;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by dennis on 16-5-2016.
 */
public class RegelcodeOverzichtController extends OverzichtController implements Initializable {
    //TODO, add more comments
    @FXML
    private TextField zoekFunctie;
    private Cell cell;
    private String sbi;
    private String regelcodeCode;
    private String activiteit;

    public RegelcodeOverzichtController() {
        this.detailsView = "view/regelcode/RegelcodeDetailsView.fxml";
        this.toevoegenView = "view/regelcode/RegelcodeToevoegenView.fxml";
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.tableView.setItems(searchFilter(FXCollections.observableArrayList(getAllRegelcode()), this.tableView));
        tableEventListener();
    }

    /**
     * Method Adds the regelcode values to the Tableview and adds a searchfilter to the values
     *
     * @param lijst
     * @param overzicht
     * @return
     */
    public SortedList<Object> searchFilter(ObservableList<Regelcode> lijst, TableView<Object> overzicht) {
        FilteredList<Regelcode> filterData = new FilteredList<Regelcode>(lijst);

        zoekFunctie.textProperty().addListener((observable, oldValue, newValue) -> {
            filterData.setPredicate(regelcode -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (regelcode.getActiviteit().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (Integer.toString(regelcode.getRegelcodeId()).toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }

                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<Object> sortedData = new SortedList<Object>(filterData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(overzicht.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        return sortedData;

    }

    /**
     * get all regelcodes
     *
     * @return Regelcodes
     */
    private ArrayList<Regelcode> getAllRegelcode() {
        RegelcodeDAO regelcodeDAO = new RegelcodeDAO();
        return regelcodeDAO.getAll();
    }
}
