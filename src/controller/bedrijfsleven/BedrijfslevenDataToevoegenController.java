package controller.bedrijfsleven;

import controller.ToevoegenController;
import dao.BedrijfslevenDataDAO;
import utility.TextFieldHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import model.Bedrijfsleven;
import model.BedrijfslevenData;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Edited by Sidney
 */
public class BedrijfslevenDataToevoegenController implements Initializable, ToevoegenController {

    @FXML private TextField jaarTextField;
    @FXML private TextField banenTextField;
    @FXML private TextField bedrijfsopbrengstenTextField;
    @FXML private TextField bedrijfskostenTotaalTextField;
    @FXML private TextField bedrijfskostenInkoopwaardeOmzetTextField;
    @FXML private TextField bedrijfskostenPersoneleKostenTextField;
    @FXML private TextField overigeBedrijfskostenTextField;
    @FXML private TextField afschrijvingOpVasteActivaTextField;
    @FXML private TextField bedrijfsresultaatTextField;
    @FXML private TextField resultaatVoorBelastingenTextField;

    private Bedrijfsleven bedrijfsleven;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TextFieldHandler tfHandler = new TextFieldHandler();
        tfHandler.textFieldToIntegerOnly(jaarTextField);
        tfHandler.textFieldLimit(jaarTextField, 4);
        tfHandler.textFieldToDecimalOnly(banenTextField);
        tfHandler.textFieldToDecimalOnly(bedrijfsopbrengstenTextField);
        tfHandler.textFieldToDecimalOnly(bedrijfskostenTotaalTextField);
        tfHandler.textFieldToDecimalOnly(bedrijfskostenInkoopwaardeOmzetTextField);
        tfHandler.textFieldToDecimalOnly(bedrijfskostenPersoneleKostenTextField);
        tfHandler.textFieldToDecimalOnly(overigeBedrijfskostenTextField);
        tfHandler.textFieldToDecimalOnly(afschrijvingOpVasteActivaTextField);
        tfHandler.textFieldToDecimalOnly(bedrijfsresultaatTextField);
        tfHandler.textFieldToDecimalOnly(resultaatVoorBelastingenTextField);
    }

    @Override
    public void handleToevoegenButton() {
       if (bedrijfslevenDataInputChecker()) {
           BedrijfslevenData bedrijfslevenData = new BedrijfslevenData(
                   bedrijfsleven.getSbi(),
                   Integer.parseInt(jaarTextField.getText()),
                   Double.parseDouble(banenTextField.getText()),
                   Double.parseDouble(bedrijfsopbrengstenTextField.getText()),
                   Double.parseDouble(bedrijfskostenTotaalTextField.getText()),
                   Double.parseDouble(bedrijfskostenInkoopwaardeOmzetTextField.getText()),
                   Double.parseDouble(bedrijfskostenPersoneleKostenTextField.getText()),
                   Double.parseDouble(overigeBedrijfskostenTextField.getText()),
                   Double.parseDouble(afschrijvingOpVasteActivaTextField.getText()),
                   Double.parseDouble(bedrijfsresultaatTextField.getText()),
                   Double.parseDouble(resultaatVoorBelastingenTextField.getText())
           );
           BedrijfslevenDataDAO bedrijfslevenDataDAO = new BedrijfslevenDataDAO();
           bedrijfslevenDataDAO.insert(bedrijfslevenData);
           bedrijfsleven.registerBedrijfslevenData(bedrijfslevenData);
       }
    }

    public void setBedrijfsleven(Bedrijfsleven bedrijfsleven) {
        this.bedrijfsleven = bedrijfsleven;
    }

    /**
     * Methode die er voor zorgt dat alle velden gecheckt worden en vervolgens aanpassingen toelaat of niet.
     * @return
     */
    private boolean bedrijfslevenDataInputChecker() {

        boolean correctInputCheck;
        String errorMessage = "De onderstaande punten dienen verbeterd te worden om de bedrijfsleven data toe te voegen: \n\n";

        boolean emptyJaar = false;
        boolean jaarAlreadyInUse = false;

        BedrijfslevenDataDAO bedrijfslevenDataDAO = new BedrijfslevenDataDAO();

        if (jaarTextField.getText() == null || jaarTextField.getText().trim().isEmpty()) {
            emptyJaar = true;
            errorMessage += "U dient een jaar in te voeren! \n";
        } else {
            if (!bedrijfslevenDataDAO.getJaarBySbi(bedrijfsleven.getSbi(), Integer.parseInt(jaarTextField.getText())).isEmpty()) { // is not empty
                jaarAlreadyInUse = true;
                errorMessage += "Het ingevoerde jaar voor dit SBI bestaat al!  \n";
            }
        }

        // handle error after checking everything
        if (emptyJaar || jaarAlreadyInUse) {
            // failed
            correctInputCheck = false;
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Informatie");
            alert.setHeaderText("Error!");
            alert.setContentText(errorMessage);
            alert.show();
        } else {
            // success
            correctInputCheck = true;
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informatie");
            alert.setHeaderText("Gelukt!");
            alert.setContentText("De bedrijfsleven data is succesvol toegevoegd!");
            alert.show();
        }
        return correctInputCheck;
    }
}
