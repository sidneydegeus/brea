package controller.bedrijfsleven;

import controller.OverzichtController;
import dao.BedrijfslevenDAO;
import dao.BedrijfslevenVerzamelingDAO;
import dao.RegelcodeDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import model.Bedrijfsleven;
import model.BedrijfslevenSBIVerzameling;
import model.Hoofdgroep;
import model.Regelcode;
import model.factory.BedrijfslevenFactory;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.util.SystemOutLogger;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;

/**
 * Edited by Sidney, Jordan
 */
public class BedrijfslevenOverzichtController extends OverzichtController implements Initializable {

    @FXML TableColumn tableColumnSbi;
    @FXML TableColumn tableColumnHoofdgroep;

    @FXML private TextField zoekFunctie;

    private Cell cell;
    private String sbi;
    private String SBI;
    private String regelcode;
    private String omschrijving;

    public BedrijfslevenOverzichtController() {
        this.detailsView = "view/bedrijfsleven/BedrijfslevenDetailsView.fxml";
        this.toevoegenView = "view/bedrijfsleven/BedrijfslevenToevoegenView.fxml";
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tableColumnSbi.setSortType(TableColumn.SortType.ASCENDING);
        ArrayList<Bedrijfsleven> arrList = getAllSBI();
        this.tableView.setItems(searchFilter(FXCollections.observableArrayList(arrList), this.tableView));
        tableEventListener();
        tableColumnHoofdgroep.getCellValueFactory();
        tableColumnHoofdgroep.setCellValueFactory(
                new PropertyValueFactory<Bedrijfsleven, String>("hoofdgroepOmschrijving")
        );

        //tableView.getColumns().addAll(tableColumnSbi);
        tableView.getSortOrder().add(tableColumnSbi);
    }

    /**
     * search filter zorgt voor het opzoeken van gegevens uit de tabelview.
     * @param lijst
     * @param overzicht
     * @return
     */
    public SortedList<Object> searchFilter(ObservableList<Bedrijfsleven> lijst, TableView<Object> overzicht) {
        FilteredList<Bedrijfsleven> filterData = new FilteredList<Bedrijfsleven>(lijst);

        zoekFunctie.textProperty().addListener((observable, oldValue, newValue) -> {
            filterData.setPredicate(bedrijfsleven -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (bedrijfsleven.getSbiString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (bedrijfsleven.getOmschrijving().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                } //else if (bedrijfsleven.getHoofdgroepOmschrijving().toLowerCase().contains(lowerCaseFilter) && bedrijfsleven.getHoofdgroepOmschrijving()) {
                  //  return true; // Filter matches last name.
                //}

                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<Object> sortedData = new SortedList<Object>(filterData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(overzicht.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        return sortedData;

    }

    /**
     *
     */

    @FXML
    public void handleImporterenTest() {
        new BedrijfslevenImporterenController();
    }


    private ArrayList<Bedrijfsleven> getAllSBI() {
        BedrijfslevenDAO bedrijfslevenDAO = new BedrijfslevenDAO();
        //return bedrijfslevenDAO.getAllChildSbi(sbi);
        return bedrijfslevenDAO.getAll();
    }
}
