package controller.bedrijfsleven;

import controller.DetailsController;
import controller.OverzichtController;
import dao.BedrijfslevenDAO;
import dao.BedrijfslevenDataDAO;
import dao.HoofdgroepDAO;
import dao.RegelcodeDAO;
import utility.TextFieldHandler;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import model.Bedrijfsleven;
import model.BedrijfslevenData;
import model.Hoofdgroep;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Edited by Sidney
 */
public class BedrijfslevenDetailsController implements Initializable, DetailsController {

    @FXML TextField sbiTextField;
    @FXML TextField regelcodeIdTextField;
    @FXML TextField omschrijvingTextField;
    @FXML TextField cbsOmschrijvingTextField;
    @FXML ComboBox hoofdgroepComboBox;
    @FXML Button bedrijfslevenDataToevoegenButton;
    @FXML Button bedrijfslevenDataAanpassenButton;
    @FXML Button bedrijfslevenDataVerwijderenButton;
    @FXML TableView dataTableView;
    // for the sake of reusability
    private Bedrijfsleven bedrijfsleven;
    private BedrijfslevenDAO bedrijfslevenDAO;
    private BedrijfslevenDataDAO bedrijfslevenDataDAO;
    private BedrijfslevenData selectedDataRow;

    public void getDataBySBI() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bedrijfslevenDAO = new BedrijfslevenDAO();
        bedrijfslevenDataDAO = new BedrijfslevenDataDAO();

        TextFieldHandler tfHandler = new TextFieldHandler();
        tfHandler.textFieldToIntegerOnly(sbiTextField);
        tfHandler.textFieldToIntegerOnly(regelcodeIdTextField);
        tfHandler.textFieldLimit(sbiTextField, 5);
        sbiTextField.setDisable(true);

        ObservableList<Hoofdgroep> hoofdgroepen = FXCollections.observableArrayList(new HoofdgroepDAO().getAll());
        hoofdgroepComboBox.setItems(hoofdgroepen);
        hoofdgroepComboBox.setConverter(new StringConverter<Hoofdgroep>() {
            @Override
            public String toString(Hoofdgroep object) {
                return object.getPublicatie_niveau() + ", " + object.getHoofdgroepOmschrijving();
            }

            @Override
            public Hoofdgroep fromString(String string) {
                // TODO Auto-generated method stub
                return null;
            }
        });

        dataTableView.setRowFactory( tv -> {
            TableRow<BedrijfslevenData> row = new TableRow<>();

            row.setOnKeyPressed(event -> {
                if(event.getCode() == KeyCode.ENTER && (! row.isEmpty()) ) {
                    selectedDataRow = row.getItem();
                }
            });

            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 1 && (! row.isEmpty()) ) {
                    selectedDataRow = row.getItem();
                }
            });
            // returns instance (or pointer) of the clicked object.
            return row;
        });
    }

    /**
     * Zet de gegevens in de textvelden van het aangeklikte object uit de tableview in het overzicht.
     */
    public void setDetails(OverzichtController overzichtController) {
        bedrijfsleven = (Bedrijfsleven) overzichtController.getSelectedRow();
        sbiTextField.setText(String.valueOf(bedrijfsleven.getSbiString()));
        regelcodeIdTextField.setText(String.valueOf(bedrijfsleven.getRegelcode().getRegelcodeId()));
        omschrijvingTextField.setText(bedrijfsleven.getOmschrijving());
        cbsOmschrijvingTextField.setText(bedrijfsleven.getCbsOmschrijving());

        hoofdgroepComboBox.setValue(bedrijfsleven.getHoofdgroep());

        if (!bedrijfslevenDAO.getByParentSbi(bedrijfsleven.getSbi()).isEmpty()) { // is not empty
            bedrijfslevenDataToevoegenButton.setDisable(true);
            bedrijfslevenDataAanpassenButton.setDisable(true);
            bedrijfslevenDataVerwijderenButton.setDisable(true);
        }
        this.dataTableView.setItems(FXCollections.observableArrayList(getAllBedrijfslevenDataBySbi()));
    }

    @FXML
    public void bedrijfslevenDataToevoegen() throws IOException {
        String view = "view/bedrijfsleven/BedrijfslevenDataToevoegenView.fxml";
        bedrijfslevenDataPopup(view);
    }

    @FXML
    public void bedrijfslevenDataAanpassen() throws IOException {
        if (selectedDataRow != null) {
            String view = "view/bedrijfsleven/BedrijfslevenDataAanpassenView.fxml";
            bedrijfslevenDataPopup(view);
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informatie");
            alert.setHeaderText("Geen selectie");
            alert.setContentText("U heeft geen jaar aangeklikt");
            alert.showAndWait();
        }
    }

    @FXML
    public void bedrijfslevenDataVerwijderen() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                "Delete het jaar: " + selectedDataRow.getJaar() + " ?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            bedrijfslevenDataDAO.delete(selectedDataRow);
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informatie");
            alert.setHeaderText("Gelukt!");
            alert.setContentText("Het jaar is verwijderd!");
            alert.showAndWait();
        }
    }

    /**
     * weertoont de popup voor het toevoegen of aanpassen van bedrijfsleven data
     * @param view
     * @throws IOException
     */
    private void bedrijfslevenDataPopup(String view) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(view));
        Parent root = (Parent) loader.load(getClass().getClassLoader().getResource(view).openStream());
        //Parent root = (Parent) loader.load();
        if (view.equals("view/bedrijfsleven/BedrijfslevenDataAanpassenView.fxml")) {
            loader.<BedrijfslevenDataAanpassenController>getController().setBedrijfsleven(bedrijfsleven);
            loader.<BedrijfslevenDataAanpassenController>getController().setFields(selectedDataRow);
        } else {
            loader.<BedrijfslevenDataToevoegenController>getController().setBedrijfsleven(bedrijfsleven);
        }
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("popup?");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    public void handleBewerkButton() {
        if (sbiInputChecker()) {
            bedrijfsleven.setRegelcodeId(Integer.parseInt(regelcodeIdTextField.getText()));
            bedrijfsleven.setOmschrijving(omschrijvingTextField.getText());
            bedrijfsleven.setCbsOmschrijving(cbsOmschrijvingTextField.getText());
            bedrijfsleven.setHoofdgroep((Hoofdgroep) hoofdgroepComboBox.getSelectionModel().getSelectedItem());
            bedrijfslevenDAO.update(bedrijfsleven);
        }
    }

    @FXML
    public void handleVerwijderButton() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                "Delete het dit SBI en alle onderliggende SBI's?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            bedrijfslevenDAO.delete(bedrijfsleven);
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informatie");
            alert.setHeaderText("Gelukt!");
            alert.setContentText("Het jaar is verwijderd!");
            alert.showAndWait();
        }
    }

    private ArrayList<BedrijfslevenData> getAllBedrijfslevenDataBySbi() {
        BedrijfslevenDataDAO bedrijfslevenDataDAO = new BedrijfslevenDataDAO();
        ArrayList<BedrijfslevenData> bedrinfslevenData = bedrijfslevenDataDAO.getAllBySBI(bedrijfsleven.getSbi());
        return bedrinfslevenData;
    }

    /**
     * Checkt alle textvelden en geeft een error indien er fouten zijn
     * @return
     */
    private boolean sbiInputChecker() {

        boolean correctInputCheck;
        String errorMessage = "De onderstaande punten dienen verbeterd te worden om het SBI aan te kunnen passen: \n\n";

        boolean emptyRegelcodeField = false;
        boolean regelcodeDoesNotExist = false;

        boolean emptyOmschrijvingField = false;
        boolean emptyCbsOmshchrijvingField = false;
        boolean emptyHoofdgroep = false;

        BedrijfslevenDAO bedrijfslevenDAO = new BedrijfslevenDAO();
        RegelcodeDAO regelcodeDAO = new RegelcodeDAO();

        if (regelcodeIdTextField.getText() == null || regelcodeIdTextField.getText().trim().isEmpty()) {
            emptyRegelcodeField = true;
            errorMessage += "U dient een regelcode id in te voeren! \n";
        } else {
            if (regelcodeDAO.getByRegelcodeId(Integer.parseInt(regelcodeIdTextField.getText())).isEmpty()) {
                regelcodeDoesNotExist = true;
                errorMessage += "Het ingevoerde regelcode id bestaat nog niet!  \n";
            }
        }

        if (omschrijvingTextField.getText() == null || omschrijvingTextField.getText().trim().isEmpty()) {
            emptyOmschrijvingField = true;
            errorMessage += "U dient een omschrijving in te voeren! \n";
        }

        if (cbsOmschrijvingTextField.getText() == null || cbsOmschrijvingTextField.getText().trim().isEmpty()) {
            emptyCbsOmshchrijvingField = true;
            errorMessage += "U dient een CBS omschrijving in te voeren! \n";
        }

        if (hoofdgroepComboBox.getValue() == null) {
            emptyHoofdgroep = true;
            errorMessage += "U dient een hoofdgroep te kiezen! \n";
        }


        // handle error after checking everything
        if (emptyRegelcodeField
                || regelcodeDoesNotExist
                || emptyOmschrijvingField
                || emptyCbsOmshchrijvingField
                || emptyHoofdgroep
                ) {
            // failed
            correctInputCheck = false;
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Informatie");
            alert.setHeaderText("Error!");
            alert.setContentText(errorMessage);
            alert.show();
        } else {
            // success
            correctInputCheck = true;
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informatie");
            alert.setHeaderText("Gelukt!");
            alert.setContentText("De SBI is succesvol toegevoegd!");
            alert.show();
        }
        return correctInputCheck;
    }
}
