package controller.bedrijfsleven;

import utility.BedrijfslevenDataImporteren;
import utility.BedrijfslevenImporteren;
import utility.RegelcodeImporteren;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;

/**
 * Edited by: Sidney
 */
public class BedrijfslevenImporterenController extends Task{

    private int maxRows = 0;
    private int currentRow = 0;
    private ProgressBar bar;
    private BedrijfslevenImporterenController task;
    private File selectedFile;
    private Stage stage;

    public BedrijfslevenImporterenController() {
        task = this;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open bestand");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Microsoft Excel-werkblad", "*.xlsx"), new FileChooser.ExtensionFilter("Microsoft Excel-bestand", "*.xls"));

        FXMLLoader loader = new FXMLLoader();
        selectedFile = fileChooser.showOpenDialog(loader.getRoot());
        if (selectedFile != null && selectedFile.getName().contains(".xlsx")) {

            bar = new ProgressBar(0);
            bar.setMinWidth(200);

            HBox hbox = new HBox();
            hbox.setAlignment(Pos.CENTER);
            hbox.getChildren().addAll(bar);
            stage = new Stage();
            stage.setScene(new Scene(hbox));
            stage.setTitle("Progress bar");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            bar.progressProperty().bind(this.progressProperty());
            stage.show();
            new Thread(this).start();
        }
    }

    /**
     * Door Task te extenden wordt er gebruik gemaakt van threads. call wordt automatisch opgeroepen in de constructor
     * @return
     * @throws Exception
     */
    @Override
    protected Object call() throws Exception {
        XSSFWorkbook workbook = new XSSFWorkbook(selectedFile);
        String[] tabbladen = {
                "RegelCodes",
                "Indeling Stat Bedrijfsleven",
                "Ruwe data Bedrijfsleven"
        };
        for (int currTab = 0; currTab < tabbladen.length; currTab++) {
            XSSFSheet sheet = workbook.getSheet(tabbladen[currTab]);
            maxRows += sheet.getPhysicalNumberOfRows();
        }

        new RegelcodeImporteren().importRegelcode(workbook, task);
        new BedrijfslevenImporteren().importBedrijfsleven(workbook, task);
        new BedrijfslevenDataImporteren().importBedrijfslevenData(workbook, task);

        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        updateMessage("Done!");
        stage.hide();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informatie");
        alert.setHeaderText("Importeren gelukt!");
        alert.setContentText("Het importeren is succesvol afgerond.");
        alert.show();
    }

    public void updateProgressBar() {
        this.updateProgress(currentRow, maxRows);
    }

    public void setCurrentRow() {
        currentRow++;
    }
}


