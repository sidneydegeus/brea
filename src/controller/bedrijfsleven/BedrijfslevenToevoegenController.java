package controller.bedrijfsleven;

import controller.ToevoegenController;
import dao.BedrijfslevenDAO;
import dao.BedrijfslevenDataDAO;
import dao.HoofdgroepDAO;
import dao.RegelcodeDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import model.*;
import model.factory.BedrijfslevenFactory;
import utility.TextFieldHandler;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Edited by Sidney
 */
public class BedrijfslevenToevoegenController  implements Initializable, ToevoegenController {

    @FXML TextField sbiTextField;
    @FXML TextField regelcodeIdTextField;
    @FXML TextField omschrijvingTextField;
    @FXML TextField cbsOmschrijvingTextField;
    @FXML ComboBox hoofdgroepComboBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TextFieldHandler tfHandler = new TextFieldHandler();
        tfHandler.textFieldToIntegerOnly(sbiTextField);
        tfHandler.textFieldToIntegerOnly(regelcodeIdTextField);
        tfHandler.textFieldLimit(sbiTextField, 5);

        ObservableList<Hoofdgroep> hoofdgroepen = FXCollections.observableArrayList(new HoofdgroepDAO().getAll());
        hoofdgroepComboBox.setItems(hoofdgroepen);
        hoofdgroepComboBox.setConverter(new StringConverter<Hoofdgroep>() {
            @Override
            public String toString(Hoofdgroep object) {
                return object.getPublicatie_niveau() + ", " + object.getHoofdgroepOmschrijving();
            }

            @Override
            public Hoofdgroep fromString(String string) {
                // TODO Auto-generated method stub
                return null;
            }
        });
    }

    @Override
    public void handleToevoegenButton() {
        if (sbiInputChecker()) {
            Bedrijfsleven bedrijfsleven = new BedrijfslevenFactory().createBedrijfsleven(
                    sbiTextField.getText(),
                    omschrijvingTextField.getText(),
                    cbsOmschrijvingTextField.getText(),
                    new Regelcode(Integer.parseInt(regelcodeIdTextField.getText())),
                    (Hoofdgroep) hoofdgroepComboBox.getSelectionModel().getSelectedItem()
            );

            boolean transferData = false;
            if (new BedrijfslevenDAO().getByParentSbi(bedrijfsleven.getParentSbi()).isEmpty()) {
                transferData = true;
            }
            BedrijfslevenDataDAO bedrijfslevenDataDAO = new BedrijfslevenDataDAO();
            ArrayList<BedrijfslevenData> data = bedrijfslevenDataDAO.getAllBySBI(bedrijfsleven.getParentSbi());

            new BedrijfslevenDAO().insert(bedrijfsleven);

            if (transferData) {
                for (int i = 0; i < data.size(); i++) {
                    //ugly way of fixing, but the update on a foreign key contraints, would have to drop it and recreate
                    bedrijfslevenDataDAO.delete(data.get(i));
                    data.get(i).setSbi(bedrijfsleven.getSbi());
                    bedrijfslevenDataDAO.insert(data.get(i));
                }
            }
        }

    }

    /**
     * Dient er voor om alle velden te checken en overzicht van de fouten te weergeven indien
     * er foute inputs gedaan zijn.
     * @return
     */
    private boolean sbiInputChecker() {

        boolean correctInputCheck;
        String errorMessage = "De onderstaande punten dienen verbeterd te worden om het SBI toe te kunnen voegen: \n\n";

        boolean emptySbiField = false;
        boolean sbiAlreadyExist = false;
        boolean parentSbiExist = false;
        boolean sbiIsBedrijfsklasse = false;

        boolean emptyRegelcodeField = false;
        boolean regelcodeDoesNotExist = false;

        boolean emptyOmschrijvingField = false;
        boolean emptyCbsOmshchrijvingField = false;
        boolean emptyHoofdgroep = false;

        BedrijfslevenDAO bedrijfslevenDAO = new BedrijfslevenDAO();
        RegelcodeDAO regelcodeDAO = new RegelcodeDAO();

        int parsedSbiTextField = Integer.parseInt(1 + "" + sbiTextField.getText());

        if (sbiTextField.getText() == null || sbiTextField.getText().trim().isEmpty()) {
            emptySbiField = true;
            errorMessage += "U dient een SBI in te voeren! \n";
        } else {
            if (!bedrijfslevenDAO.getBySbi(parsedSbiTextField).isEmpty()) { // is not empty
                sbiAlreadyExist = true;
                errorMessage += "Het ingevoerde SBI bestaat al! \n";
            }
            BedrijfslevenFactory bFactory = new BedrijfslevenFactory();
            if (!bedrijfslevenDAO.getBySbi(bFactory.parentSbiCalculator(parsedSbiTextField)).isEmpty()) { // is not empty
                parentSbiExist = true;
            } else {

                //nogmaals checken, indien sbi 2 digits (bedrijfsklasse) heeft kan het geen parentsbi hebben
                if (bedrijfslevenDAO.getBySbi(bFactory.parentSbiCalculator(parsedSbiTextField)).isEmpty()
                        && parsedSbiTextField < 200) {
                    sbiIsBedrijfsklasse = true;
                } else {
                    errorMessage += "Het ingevoerde SBI heeft geen hoger niveau \n";
                }

            }
        }

        if (regelcodeIdTextField.getText() == null || regelcodeIdTextField.getText().trim().isEmpty()) {
            emptyRegelcodeField = true;
            errorMessage += "U dient een regelcode id in te voeren! \n";
        } else {
            if (regelcodeDAO.getByRegelcodeId(Integer.parseInt(regelcodeIdTextField.getText())).isEmpty()) {
                regelcodeDoesNotExist = true;
                errorMessage += "Het ingevoerde regelcode id bestaat nog niet!  \n";
            }
        }

        if (omschrijvingTextField.getText() == null || omschrijvingTextField.getText().trim().isEmpty()) {
            emptyOmschrijvingField = true;
            errorMessage += "U dient een omschrijving in te voeren! \n";
        }

        if (cbsOmschrijvingTextField.getText() == null || cbsOmschrijvingTextField.getText().trim().isEmpty()) {
            emptyCbsOmshchrijvingField = true;
            errorMessage += "U dient een CBS omschrijving in te voeren! \n";
        }

        if (hoofdgroepComboBox.getValue() == null) {
            emptyHoofdgroep = true;
            errorMessage += "U dient een hoofdgroep te kiezen! \n";
        }


        // handle error after checking everything
        if (emptySbiField
                || sbiAlreadyExist
                || (!parentSbiExist && !sbiIsBedrijfsklasse)
                || emptyRegelcodeField
                || regelcodeDoesNotExist
                || emptyOmschrijvingField
                || emptyCbsOmshchrijvingField
                || emptyHoofdgroep
        ) {
            // failed
            correctInputCheck = false;
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Informatie");
            alert.setHeaderText("Error!");
            alert.setContentText(errorMessage);
            alert.show();
        } else {
            // success
            correctInputCheck = true;
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informatie");
            alert.setHeaderText("Gelukt!");
            alert.setContentText("De SBI is succesvol toegevoegd!");
            alert.show();
        }
        return correctInputCheck;
    }
}
