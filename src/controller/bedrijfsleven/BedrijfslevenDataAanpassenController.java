package controller.bedrijfsleven;

import dao.BedrijfslevenDataDAO;
import utility.TextFieldHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import model.Bedrijfsleven;
import model.BedrijfslevenData;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Edited by: Sidney
 */
public class BedrijfslevenDataAanpassenController implements Initializable {

    @FXML TextField jaarTextField;
    @FXML TextField banenTextField;
    @FXML TextField bedrijfsopbrengstenTextField;
    @FXML TextField bedrijfskostenTotaalTextField;
    @FXML TextField bedrijfskostenInkoopwaardeOmzetTextField;
    @FXML TextField bedrijfskostenPersoneleKostenTextField;
    @FXML TextField overigeBedrijfskostenTextField;
    @FXML TextField afschrijvingOpVasteActivaTextField;
    @FXML TextField bedrijfsresultaatTextField;
    @FXML TextField resultaatVoorBelastingenTextField;

    private Bedrijfsleven bedrijfsleven;
    private BedrijfslevenData bedrijfslevenData;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TextFieldHandler tfHandler = new TextFieldHandler();
        tfHandler.textFieldToIntegerOnly(jaarTextField);
        tfHandler.textFieldLimit(jaarTextField, 4);
        tfHandler.textFieldToDecimalOnly(banenTextField);
        tfHandler.textFieldToDecimalOnly(bedrijfsopbrengstenTextField);
        tfHandler.textFieldToDecimalOnly(bedrijfskostenTotaalTextField);
        tfHandler.textFieldToDecimalOnly(bedrijfskostenInkoopwaardeOmzetTextField);
        tfHandler.textFieldToDecimalOnly(bedrijfskostenPersoneleKostenTextField);
        tfHandler.textFieldToDecimalOnly(overigeBedrijfskostenTextField);
        tfHandler.textFieldToDecimalOnly(afschrijvingOpVasteActivaTextField);
        tfHandler.textFieldToDecimalOnly(bedrijfsresultaatTextField);
        tfHandler.textFieldToDecimalOnly(resultaatVoorBelastingenTextField);
        jaarTextField.setDisable(true);
    }

    @FXML
    public void handleAanpassenButton() {
        if (bedrijfslevenDataInputChecker()) {
            BedrijfslevenData bedrijfslevenData = new BedrijfslevenData(
                    bedrijfsleven.getSbi(),
                    Integer.parseInt(jaarTextField.getText()),
                    Double.parseDouble(banenTextField.getText()),
                    Double.parseDouble(bedrijfsopbrengstenTextField.getText()),
                    Double.parseDouble(bedrijfskostenTotaalTextField.getText()),
                    Double.parseDouble(bedrijfskostenInkoopwaardeOmzetTextField.getText()),
                    Double.parseDouble(bedrijfskostenPersoneleKostenTextField.getText()),
                    Double.parseDouble(overigeBedrijfskostenTextField.getText()),
                    Double.parseDouble(afschrijvingOpVasteActivaTextField.getText()),
                    Double.parseDouble(bedrijfsresultaatTextField.getText()),
                    Double.parseDouble(resultaatVoorBelastingenTextField.getText())
            );
            BedrijfslevenDataDAO bedrijfslevenDataDAO = new BedrijfslevenDataDAO();
            bedrijfslevenDataDAO.update(bedrijfslevenData);
        }
    }

    public void setBedrijfsleven(Bedrijfsleven bedrijfsleven) {
        this.bedrijfsleven = bedrijfsleven;
    }

    /**
     * Vult de velden in bij het aanklikken van aanpassen
     * @param selectedDataRow
     */
    public void setFields(Object selectedDataRow) {
        bedrijfslevenData = (BedrijfslevenData) selectedDataRow;
        jaarTextField.setText(Integer.toString(bedrijfslevenData.getJaar()));
        System.out.println(Double.toString(bedrijfslevenData.getBanen()));
        banenTextField.setText(Double.toString(bedrijfslevenData.getBanen()));
        bedrijfsopbrengstenTextField.setText(Double.toString(bedrijfslevenData.getOpbrengsten()));
        bedrijfskostenTotaalTextField.setText(Double.toString(bedrijfslevenData.getBedrijfskosten()));
        bedrijfskostenInkoopwaardeOmzetTextField.setText(Double.toString(bedrijfslevenData.getInkoopwaardeOmzet()));
        bedrijfskostenPersoneleKostenTextField.setText(Double.toString(bedrijfslevenData.getPersoneelKosten()));
        overigeBedrijfskostenTextField.setText(Double.toString(bedrijfslevenData.getBedrijfskosten()));
        afschrijvingOpVasteActivaTextField.setText(Double.toString(bedrijfslevenData.getAfschrijvingenVasteActiva()));
        bedrijfsresultaatTextField.setText(Double.toString(bedrijfslevenData.getBedrijfsResultaat()));
        resultaatVoorBelastingenTextField.setText(Double.toString(bedrijfslevenData.getResultaatBelastingen()));
    }

    /**
     * Methode die er voor zorgt dat alle velden gecheckt worden en vervolgens aanpassingen toelaat of niet.
     * @return
     */
    private boolean bedrijfslevenDataInputChecker() {

        boolean correctInputCheck;
        String errorMessage = "De onderstaande punten dienen verbeterd te worden om de bedrijfsleven data aan te passen: \n\n";

        boolean emptyJaar = false;
        boolean jaarAlreadyInUse = false;

        BedrijfslevenDataDAO bedrijfslevenDataDAO = new BedrijfslevenDataDAO();

        if (jaarTextField.getText() == null || jaarTextField.getText().trim().isEmpty()) {
            emptyJaar = true;
            errorMessage += "U dient een jaar in te voeren! \n";
        } else {
            if (!bedrijfslevenDataDAO.getJaarBySbi(bedrijfsleven.getSbi(), Integer.parseInt(jaarTextField.getText())).isEmpty() // is not empty
                    && bedrijfslevenData.getJaar() != Integer.parseInt(jaarTextField.getText())) {
                jaarAlreadyInUse = true;
                errorMessage += "Het ingevoerde jaar voor dit SBI bestaat al!  \n";
            }
        }

        // handle error after checking everything
        if (emptyJaar || jaarAlreadyInUse) {
            // failed
            correctInputCheck = false;
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Informatie");
            alert.setHeaderText("Error!");
            alert.setContentText(errorMessage);
            alert.show();
        } else {
            // success
            correctInputCheck = true;
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informatie");
            alert.setHeaderText("Gelukt!");
            alert.setContentText("De bedrijfsleven data is succesvol aangepast!");
            alert.show();
        }
        return correctInputCheck;
    }
}
