package controller;

import javafx.fxml.FXML;

/**
 * Created by Sidney on 28-5-2016.
 */
public interface ToevoegenController {

    @FXML void handleToevoegenButton();
}
