package controller.correcties;

import dao.CorrectiesDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import model.Correctiefactoren;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by dennis on 16-5-2016.
 */
public class CorrectiesToevoegenController implements Initializable{
    @FXML
    private TextField sbiField;
    @FXML
    private TextField banen0ProductieField;
    @FXML
    private TextField banen1Tot10ProductieField;
    @FXML
    private TextField banen10Tot100ProductieField;
    @FXML
    private TextField banen100ProductieField;
    @FXML
    private TextField banen0TwField;
    @FXML
    private TextField banen1Tot10TwField;
    @FXML
    private TextField banen10Tot100TwField;
    @FXML
    private TextField banen100TwField;

    public CorrectiesToevoegenController() {
    }


    /**Method registrates when the voeg toe button is clicked.
     * It adds the correctiefactor when there isn't a same sbi in de db of correctiefactoren.
     * @param actionEvent
     */
    @FXML
    public void handleVoegToeButton(ActionEvent actionEvent) {
        if (checkEqual() == false) {
            addCorrectiefactor();
        } else {
            giveFailedCheckDialog();
        }
    }

    /**Method checks if the Correctiefactor is equal to the Correctiefactor in de db. When it is equal it wil return true. When it is not equal it will return true
     * When it is equal it will popup a dialog.
     * @return boolean
     */
    public boolean checkEqual() {
        try {
            int sbi = Integer.valueOf(sbiField.getText());
            CorrectiesDAO correctiesDAO = new CorrectiesDAO();
            ArrayList<Correctiefactoren> correctiefactoren = correctiesDAO.getBySBI(sbi);
            if (correctiefactoren.size() == 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /**Method adds a Correctiefactor to the db through the DAO.
     * When it is done it gives a accomplished dialog.
     */
    public void addCorrectiefactor() {
        Correctiefactoren correctiefactoren = new Correctiefactoren(Integer.valueOf(sbiField.getText()), sbiField.getText(), Double.valueOf(banen0ProductieField.getText()), Double.valueOf(banen1Tot10ProductieField.getText()),
                Double.valueOf(banen10Tot100ProductieField.getText()), Double.valueOf(banen100ProductieField.getText()), Double.valueOf(banen0TwField.getText()), Double.valueOf(banen1Tot10TwField.getText()), Double.valueOf(banen10Tot100TwField.getText()), Double.valueOf(banen100TwField.getText()));
        CorrectiesDAO correctiesDAO = new CorrectiesDAO();
        correctiesDAO.insert(correctiefactoren);
        giveAccomplishedDialog();
    }


    /**Method makes an alertbox to inform the user that the correctiefactor successfully has added to the database
     */
    public void giveAccomplishedDialog() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informatie");
        alert.setHeaderText("Gelukt!");
        alert.setContentText("De correctiefactor is succesvol toegevoegd!");
        alert.show();
    }

    /**
     * Method gives an failed add dialog
     */
    public void giveFailedCheckDialog() {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Foutmelding");
            alert.setHeaderText("Er is een foutmelding gevonden!");
            alert.setContentText("Deze sbi is al in gebruik!");
            alert.show();
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
