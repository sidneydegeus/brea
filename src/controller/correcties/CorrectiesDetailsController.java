package controller.correcties;

import controller.DetailsController;
import controller.OverzichtController;
import dao.CorrectiesDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import model.Correctiefactoren;
import utility.TextFieldHandler;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by dennis on 16-5-2016.
 */
public class CorrectiesDetailsController implements Initializable, DetailsController {
    @FXML
    private TextField sbiField;
    @FXML
    private TextField banen0ProductieField;
    @FXML
    private TextField banen1Tot10ProductieField;
    @FXML
    private TextField banen10Tot100ProductieField;
    @FXML
    private TextField banen100ProductieField;
    @FXML
    private TextField banen0TwField;
    @FXML
    private TextField banen1Tot10TwField;
    @FXML
    private TextField banen10Tot100TwField;
    @FXML
    private TextField banen100TwField;

    public CorrectiesDetailsController() {
    }

    /**
     * Method acts to the button "Bewerken"
     *
     * @param actionEvent
     */
    @FXML
    public void handleBewerkButton(ActionEvent actionEvent) {
        veranderCorrectiefactoren();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informatie");
        alert.setHeaderText("Gelukt!");
        alert.setContentText("De correcties van de factoren is veranderd!");

        alert.showAndWait();
    }

    /**
     * Method acts to the button "Verwijderen"
     *
     * @param actionEvent
     */
    @FXML
    public void handleVerwijderButton(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Informatie");
        alert.setHeaderText("De correcties van de factoren wordt verwijderd!");
        alert.setContentText("Weet u dit zeker?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            verwijderCorrectieFactoren();
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informatie");
            alert.setHeaderText("Gelukt!");
            alert.setContentText("De correcties van de factoren is verwijderd!");

            alert.showAndWait();
        }
    }

    /**
     * Method changes the Regelcode values in the database to the values of the textfields.
     */
    public void veranderCorrectiefactoren() {
        Correctiefactoren correctiefactoren = new Correctiefactoren(Integer.valueOf(sbiField.getText()), sbiField.getText(), Double.valueOf(banen0ProductieField.getText()), Double.valueOf(banen1Tot10ProductieField.getText()),
                Double.valueOf(banen10Tot100ProductieField.getText()), Double.valueOf(banen100ProductieField.getText()), Double.valueOf(banen0TwField.getText()), Double.valueOf(banen1Tot10TwField.getText()), Double.valueOf(banen10Tot100TwField.getText()), Double.valueOf(banen100TwField.getText()));
        CorrectiesDAO correctiesDAO = new CorrectiesDAO();
        correctiesDAO.update(correctiefactoren);
    }

    /**
     * Method deletes the current selected Regelcode values in de the database
     */
    public void verwijderCorrectieFactoren() {
        Correctiefactoren correctiefactoren = new Correctiefactoren(Integer.valueOf(sbiField.getText()), sbiField.getText(), Double.valueOf(banen0ProductieField.getText()), Double.valueOf(banen1Tot10ProductieField.getText()),
                Double.valueOf(banen10Tot100ProductieField.getText()), Double.valueOf(banen100ProductieField.getText()), Double.valueOf(banen0TwField.getText()), Double.valueOf(banen1Tot10TwField.getText()), Double.valueOf(banen10Tot100TwField.getText()), Double.valueOf(banen100TwField.getText()));
        CorrectiesDAO correctiesDAO = new CorrectiesDAO();
        correctiesDAO.delete(correctiefactoren);
    }

    /**
     * Method sets the data in the textfields by the selected row
     *
     * @param overzichtController
     */
    @Override
    public void setDetails(OverzichtController overzichtController) {
        Correctiefactoren correctiefactoren = (Correctiefactoren) overzichtController.getSelectedRow();
        System.out.println(overzichtController.getSelectedRow());
        sbiField.setEditable(false);
        CorrectiesDAO correctiesDAO = new CorrectiesDAO();
        try {
            ArrayList<Correctiefactoren> list = correctiesDAO.getBySBI(correctiefactoren.getSbi());
            sbiField.setText(String.valueOf(list.get(0).getSbiString()));
            banen0ProductieField.setText(String.valueOf(list.get(0).getBanen0Productie()));
            banen1Tot10ProductieField.setText(String.valueOf(list.get(0).getBanen1Tot10Productie()));
            banen10Tot100ProductieField.setText(String.valueOf(list.get(0).getBanen10Tot100Productie()));
            banen100ProductieField.setText(String.valueOf(list.get(0).getBanen100Productie()));
            banen0TwField.setText(String.valueOf(list.get(0).getBanen0Tw()));
            banen1Tot10TwField.setText(String.valueOf(list.get(0).getBanen1Tot10Tw()));
            banen10Tot100TwField.setText(String.valueOf(list.get(0).getBanen10Tot100Tw()));
            banen100TwField.setText(String.valueOf(list.get(0).getBanen100Tw()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
