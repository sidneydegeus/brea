package controller.correcties;

import controller.OverzichtController;
import dao.CorrectiesDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import model.Correctiefactoren;
import org.apache.poi.ss.usermodel.Cell;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by dennis on 16-5-2016.
 */
public class CorrectiesOverzichtController extends OverzichtController implements Initializable {
    //TODO, add more comments
    @FXML
    private TextField zoekFunctie;
    private Cell cell;
    private int sbi;
    private String banen0Productie;
    private String banen1Tot10Productie;
    private String banen10Tot100Productie;
    private String banen100Productie;
    private String banen0Tw;
    private String banen1Tot10Tw;
    private String banen10Tot100Tw;
    private String banen100Tw;

    public CorrectiesOverzichtController() {
        this.detailsView = "view/correcties/CorrectiesDetailsView.fxml";
        this.toevoegenView = "view/correcties/CorrectiesToevoegenView.fxml";
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.tableView.setItems(searchFilter(FXCollections.observableArrayList(getAllCorrecties()), this.tableView));
        tableEventListener();
    }

    /**
     * Method Adds the correctiefactoren values to the Tableview and adds a searchfilter to the values
     * @param lijst
     * @param overzicht
     * @return
     */
    public SortedList<Object> searchFilter(ObservableList<Correctiefactoren> lijst, TableView<Object> overzicht) {
        FilteredList<Correctiefactoren> filterData = new FilteredList<Correctiefactoren>(lijst);

        zoekFunctie.textProperty().addListener((observable, oldValue, newValue) -> {
            filterData.setPredicate(correctiefactoren -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (correctiefactoren.getSbiString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches
                }
                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<Object> sortedData = new SortedList<Object>(filterData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(overzicht.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        return sortedData;

    }

    /**
     * get all correctiefactoren
     * @return Correctiefactoren
     */
    private ArrayList<Correctiefactoren> getAllCorrecties() {
        CorrectiesDAO correctiesDAO = new CorrectiesDAO();
        return correctiesDAO.getAll();
    }
}
