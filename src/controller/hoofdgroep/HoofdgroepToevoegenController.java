package controller.hoofdgroep;

import controller.ToevoegenController;
import dao.HoofdgroepDAO;
import utility.TextFieldHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import model.Hoofdgroep;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Jordan Munk
 */
public class HoofdgroepToevoegenController implements Initializable, ToevoegenController {

    @FXML
    TextField publicatie_niveauField;

    @FXML
    TextField omschrijvingField;


    /**
     * Als er op de toevoegen knop wordt gedrukt.
     * @param actionEvent
     */
    @FXML
    public void handleVoegToeButton(ActionEvent actionEvent) {
        if (checkEqual() == false) {

            addHoofdgroep();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informatie");
            alert.setHeaderText("Gelukt!");
            alert.setContentText("De hoofdgroep is succesvol toegevoegd!");

            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Foutmelding");
            alert.setHeaderText("Er is een foutmelding gevonden!");
            alert.setContentText("Niet alle velden zijn ingevuld!");

            alert.showAndWait();
        }
    }

    /**
     * Kijken of de velden leeg zijn of niet.
     * @return
     */
    public boolean checkEqual() {
        if(publicatie_niveauField.getText().isEmpty() || omschrijvingField.getText().isEmpty()){
            return true;
        } else{
            return false;
        }
    }

    /**
     * Hoofdgroep toevoegen.
     */
    public void addHoofdgroep() {
        Hoofdgroep hoofdgroep = new Hoofdgroep(publicatie_niveauField.getText().toUpperCase(), omschrijvingField.getText());
        HoofdgroepDAO hoofdgroepDAO = new HoofdgroepDAO();
        hoofdgroepDAO.insert(hoofdgroep);
    }

    /**
     * Tekstvelden aanpassen dat alleen Strings mogelijk zijn en het een limiet heeft.
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TextFieldHandler tfHandler = new TextFieldHandler();
        tfHandler.textFieldLimit(publicatie_niveauField, 1);
        tfHandler.textFieldToStringOnly(publicatie_niveauField);
    }

    @Override
    public void handleToevoegenButton() {

    }
}
