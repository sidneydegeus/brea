package controller.hoofdgroep;

import controller.DetailsController;
import controller.OverzichtController;
import dao.BedrijfslevenDataDAO;
import dao.HoofdgroepDAO;
import javafx.collections.FXCollections;
import javafx.scene.control.TableView;
import model.BedrijfslevenData;
import utility.TextFieldHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import model.Hoofdgroep;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * @author Jordan Munk
 */
public class HoofdgroepDetailsController implements Initializable, DetailsController {
    @FXML
    private TextField omschrijvingField;
    @FXML
    private TextField pubnivField;
    @FXML TableView dataTableView;

    //
    private Hoofdgroep hoofdgroep;


    /**
     * Methode wordt opgeroepen als er op de bewerk knop wordt gedrukt.
     * @param actionEvent
     */
    @FXML
    public void handleBewerkButton(ActionEvent actionEvent) {
        veranderHoofdgroep();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informatie");
        alert.setHeaderText("Gelukt!");
        alert.setContentText("De hoofdgroep is veranderd!");

        alert.showAndWait();
    }

    /**
     * Methode wordt opgeroepen als er op de verwijder knop wordt gedrukt.
     * @param actionEvent
     */
    @FXML
    public void handleVerwijderButton(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Informatie");
        alert.setHeaderText("De hoofdgroep wordt verwijderd!");
        alert.setContentText("Weet u dit zeker?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            verwijderHoofdgroep();
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informatie");
            alert.setHeaderText("Gelukt!");
            alert.setContentText("De hoofdgroep is verwijderd!");

            alert.showAndWait();
        }
    }

    /**
     * Hoofdgroep wordt veranderd.
     */
    public void veranderHoofdgroep() {
        Hoofdgroep hoofdgroep = new Hoofdgroep(pubnivField.getText().toUpperCase(), omschrijvingField.getText());
        HoofdgroepDAO hoofdgroepDAO = new HoofdgroepDAO();
        hoofdgroepDAO.update(hoofdgroep);
    }

    /**
     * Hoofdgroep wordt verwijderd.
     */
    public void verwijderHoofdgroep() {
        Hoofdgroep hoofdgroep = new Hoofdgroep(pubnivField.getText().toUpperCase(), omschrijvingField.getText());
        HoofdgroepDAO hoofdgroepDAO = new HoofdgroepDAO();
        hoofdgroepDAO.delete(hoofdgroep);
    }

    /**
     * De details van de geselecteerde kolumn wordt in de tekstvelden geplaatst.
     * @param overzichtController
     */
    @Override
    public void setDetails(OverzichtController overzichtController) {
        hoofdgroep = (Hoofdgroep) overzichtController.getSelectedRow();
        System.out.println(overzichtController.getSelectedRow());

        omschrijvingField.setText(hoofdgroep.getHoofdgroepOmschrijving());
        pubnivField.setText(hoofdgroep.getPublicatie_niveau());

        this.dataTableView.setItems(FXCollections.observableArrayList(getAllDataByHoofdgroep()));
    }

    private ArrayList<BedrijfslevenData> getAllDataByHoofdgroep() {
        BedrijfslevenDataDAO bedrijfslevenDataDAO = new BedrijfslevenDataDAO();
        ArrayList<BedrijfslevenData> bedrinfslevenData = bedrijfslevenDataDAO.getAllHoofdgroepData(hoofdgroep.getPublicatie_niveau());
        return bedrinfslevenData;
    }

    /**
     * Textfielden wijzigen.
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TextFieldHandler tfHandler = new TextFieldHandler();
        tfHandler.textFieldLimit(pubnivField, 1);
        tfHandler.textFieldToStringOnly(pubnivField);
        pubnivField.setDisable(true);
    }
}
