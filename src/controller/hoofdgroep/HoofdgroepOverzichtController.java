package controller.hoofdgroep;

import controller.OverzichtController;
import dao.HoofdgroepDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.Hoofdgroep;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * @author Jordan Munk
 */
public class HoofdgroepOverzichtController extends OverzichtController implements Initializable {

    @FXML
    private TextField zoekFunctie;

    /**
     * Constructor van overzicht. Voegt de views toe.
     */
    public HoofdgroepOverzichtController() {
        this.detailsView = "view/hoofdgroep/HoofdgroepDetailsView.fxml";
        this.toevoegenView = "view/hoofdgroep/HoofdgroepToevoegenView.fxml";
    }

    /**
     * Hoofdgroepen toevoegen aan de tableview.
     * @param resources
     * @param location
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.tableView.setItems(searchFilter(FXCollections.observableArrayList(getAllHoofdgroepen()), this.tableView));
        tableEventListener();
    }

    /**
     * Een zoekbalk. Hier worden de hoofdgroepen gesorteerd.
     * @param lijst
     * @param overzicht
     */
    public SortedList<Object> searchFilter(ObservableList<Hoofdgroep> lijst, TableView<Object> overzicht) {
        FilteredList<Hoofdgroep> filterData = new FilteredList<Hoofdgroep>(lijst);

        zoekFunctie.textProperty().addListener((observable, oldValue, newValue) -> {
            filterData.setPredicate(hoofdgroep -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (hoofdgroep.getHoofdgroepOmschrijving().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (hoofdgroep.getPublicatie_niveau().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }

                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<Object> sortedData = new SortedList<Object>(filterData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(overzicht.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        return sortedData;

    }

    /**
     * Alle hoofdgroepen worden opgehaald.
     * @return
     */
    public ArrayList<Hoofdgroep> getAllHoofdgroepen() {
        HoofdgroepDAO hoofdgroepDAO = new HoofdgroepDAO();
        return hoofdgroepDAO.getAll();
    }
}
