package controller.factoren;

import controller.OverzichtController;
import dao.FactorenDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.Factoren;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by dennis on 16-5-2016.
 */
public class FactorenOverzichtController extends OverzichtController implements Initializable{

    @FXML
    private TextField zoekFunctie;

    public FactorenOverzichtController() {
        this.detailsView = "view/factoren/FactorenDetailsView.fxml";
        this.toevoegenView = "view/FactorenToevoegenView.fxml";
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.tableView.setItems(searchFilter(FXCollections.observableArrayList(getAllFactoren()), this.tableView));
        tableEventListener();
    }

    public SortedList<Object> searchFilter(ObservableList<Factoren> lijst, TableView<Object> overzicht) {
        FilteredList<Factoren> filterData = new FilteredList<Factoren>(lijst);

        zoekFunctie.textProperty().addListener((observable, oldValue, newValue) -> {
            filterData.setPredicate(factoren -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (Integer.toString(factoren.getJaar()).toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (Integer.toString(factoren.getSBI()).toLowerCase().contains(lowerCaseFilter)) {
                        return true; // Filter matches first name.
                } else if (Double.toString(factoren.getProdTotaal()).toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }  else if (Double.toString(factoren.getTwTotaal()).toLowerCase().contains(lowerCaseFilter)) {

                }
                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<Object> sortedData = new SortedList<Object>(filterData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(overzicht.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        return sortedData;

    }

    public void handleBerekenenButton(){
        new FactorenBerekenenController();
    }

    /**
     * get all factoren
     * @return Factoren
     */
    private ArrayList<Factoren> getAllFactoren() {
        FactorenDAO factorenDAO = new FactorenDAO();
        return factorenDAO.getAll();
    }

}
