package controller.factoren;

import controller.DetailsController;
import controller.OverzichtController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import model.Factoren;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.StringJoiner;
import java.util.UnknownFormatConversionException;

/**
 * Created by dennis on 16-5-2016.
 */
public class FactorenDetailsController implements Initializable, DetailsController {

    @FXML private TextField sbiField;
    @FXML private TextField jaarField;
    @FXML private TextField totaalField;
    @FXML private TextField value0Field;
    @FXML private TextField value1tm10Field;
    @FXML private TextField value0tm10Field;
    @FXML private TextField value10tm100Field;
    @FXML private TextField value0tm19Field;
    @FXML private TextField value20tm100Field;
    @FXML private TextField value0tm100Field;
    @FXML private TextField value100Field;
    @FXML private TextField valueTw0tm100Field;

    @FXML private Label detailsName;
    @FXML private Label valuetotaal;
    @FXML private Label value0;
    @FXML private Label value1tm10;
    @FXML private Label value0tm10;
    @FXML private Label value10tm100;
    @FXML private Label value0tm19;
    @FXML private Label value20tm100;
    @FXML private Label value0tm100;
    @FXML private Label value100;
    @FXML private Label valueTw0tm100;

    @FXML
    private ComboBox comboBox;

    private Factoren factoren;

    private final String PRODUCTEN_VIEW = "Producten";
    private final String TW_VIEW = "Toegevoegde waarde";

    private String currentView;

    public FactorenDetailsController() {
    }

    /**
     * Method sets the data in the textfields by the selected row
     * @param overzichtController
     */
    @Override
    public void setDetails(OverzichtController overzichtController) {
    this.factoren = (Factoren) overzichtController.getSelectedRow();
        comboBox.getItems().addAll(PRODUCTEN_VIEW, TW_VIEW);
        setCurrentView(PRODUCTEN_VIEW);
        comboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue observable, String t, String view) {
                setCurrentView(view);
            }
        });
        setFieldsUneditable();
    }
    /**
     * Method sets the Texfields uneditable
     */
    private void setFieldsUneditable() {
        sbiField.setEditable(false);
        jaarField.setEditable(false);
        totaalField.setEditable(false);
        value0Field.setEditable(false);
        value10tm100Field.setEditable(false);
        value0tm100Field.setEditable(false);
        value0tm19Field.setEditable(false);
        value20tm100Field.setEditable(false);
        value100Field.setEditable(false);
//        valueTw0tm100Field.setEditable(false);
    }

    /**
     * Method sets the correct view that the user wants to see. Views are products or toegevoegde waarde
     * @param view
     */
    private void setCurrentView(String view) {
        if (view == PRODUCTEN_VIEW) {
            currentView = view;
            detailsName.setText("Gegevens details - Factoren : prod");
            valuetotaal.setText("prod Totaal");
            value0.setText("prod 0");
            value1tm10.setText("prod 1 tm 10");
            value0tm10.setText("prod 0 tm 10");
            value10tm100.setText("prod 10 tm 100");
            value0tm19.setText("prod 0 tm 19");
            value20tm100.setText("prod 20 tm 100");
            value0tm100.setText("prod 0 tm 100");
            value100.setText("prod 100");
//            valueTw0tm100.setVisible(false);
            sbiField.setText(String.valueOf(factoren.getSbiString()));
            jaarField.setText(String.valueOf(factoren.getJaar()));
            totaalField.setText(String.valueOf(factoren.getProdTotaal()));
            value0Field.setText(String.valueOf(factoren.getProd0()));
            value1tm10Field.setText(String.valueOf(factoren.getProd1To10()));
            value0tm10Field.setText(String.valueOf(factoren.getProd0To10()));
            value10tm100Field.setText(String.valueOf((factoren.getProd10To100())));
            value0tm19Field.setText(String.valueOf(factoren.getProd10To100()));
            value20tm100Field.setText(String.valueOf(factoren.getProd0To19()));
            value0tm100Field.setText((String.valueOf(factoren.getProd0To100())));
            value100Field.setText(String.valueOf(factoren.getTw100()));
 //           valueTw0tm100Field.setVisible(false);

        } else if (view == TW_VIEW) {
            currentView = view;
            detailsName.setText("Gegevens details - Factoren : tw");
            valuetotaal.setText("tw Totaal");
            value0.setText("tw 0");
            value1tm10.setText("tw 1 To 10");
            value0tm10.setText("tw 0 To 10");
            value10tm100.setText("tw 10 To 100");
            value0tm19.setText("tw 0 To 19");
            value20tm100.setText("tw 20 To 100");
            value0tm100.setText("tw 0To 100");
            value100.setText("tw 100");
  //          valueTw0tm100.setVisible(true);
            sbiField.setText(String.valueOf(factoren.getSbiString()));
            jaarField.setText(String.valueOf(factoren.getJaar()));
            totaalField.setText(String.valueOf(factoren.getTwTotaal()));
            value0Field.setText(String.valueOf(factoren.getTw0()));
            value1tm10Field.setText(String.valueOf(factoren.getTw1To10()));
            value0tm10Field.setText(String.valueOf(factoren.getTw0To10()));
            value10tm100Field.setText(String.valueOf((factoren.getTw10To100())));
            value0tm19Field.setText(String.valueOf(factoren.getTw10To100()));
            value20tm100Field.setText(String.valueOf(factoren.getTw0To19()));
            value0tm100Field.setText((String.valueOf(factoren.getTw0To100())));
            value100Field.setText(String.valueOf(factoren.getTw100()));
   //         valueTw0tm100Field.setVisible(true);
        } else {
            throw new UnknownFormatConversionException("View is not defined!");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
