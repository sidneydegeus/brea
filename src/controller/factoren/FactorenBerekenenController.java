package controller.factoren;

import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import utility.FactorenBerekenen;

import java.io.File;


import dao.BedrijfslevenDAO;
import dao.BedrijfslevenDataDAO;
import dao.HoofdgroepDAO;
import model.Bedrijfsleven;
import model.BedrijfslevenData;
import model.Correctiefactoren;
import model.Hoofdgroep;
import dao.*;
import model.*;

import java.util.ArrayList;

/**
 * Edited by Sidney, Dennis
 */
public class FactorenBerekenenController extends Task{
    ArrayList<Factoren> factoren;

    private int maxRows = 0;
    private int currentRow = 0;
    private ProgressBar bar;
    private FactorenBerekenenController task;
    //private static Task task;
    private File selectedFile;

    //view
    private Stage stage;

    public FactorenBerekenenController() {
        task = this;

        bar = new ProgressBar(0);
        bar.setMinWidth(200);

        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER);
        hbox.getChildren().addAll(bar);
        stage = new Stage();
        stage.setScene(new Scene(hbox));
        stage.setTitle("Factoren berekenen...");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        bar.progressProperty().bind(this.progressProperty());
        stage.show();
        new Thread(this).start();

    }

    @Override
    protected Object call() throws Exception {
        BedrijfslevenDAO bedrijfslevenDAO = new BedrijfslevenDAO();
        BedrijfslevenDataDAO bedrijfslevenDataDAO = new BedrijfslevenDataDAO();
        HoofdgroepDAO hoofdgroepDAO = new HoofdgroepDAO();
        CorrectiesDAO correctiesDAO = new CorrectiesDAO();
        FactorenDAO factorenDAO = new FactorenDAO();

        ArrayList<Bedrijfsleven> bedrijfslevenList = bedrijfslevenDAO.getAll();
        maxRows += bedrijfslevenList.size();
        for (Bedrijfsleven bedrijfsleven : bedrijfslevenList) {
            ArrayList<BedrijfslevenData> sbiData = bedrijfslevenDataDAO.getAllBySBI(bedrijfsleven.getSbi());
            maxRows += sbiData.size();

            bedrijfsleven.setBedrijfslevenDataList(sbiData);
            setCurrentRow();
            updateProgressBar();
        }

        ArrayList<Hoofdgroep> hoofdgroepList = hoofdgroepDAO.getAll();
        maxRows += hoofdgroepList.size();
        for (Hoofdgroep hoofdgroep : hoofdgroepList) {
            ArrayList<BedrijfslevenData> publicatieNiveauData = bedrijfslevenDataDAO.getAllHoofdgroepData(hoofdgroep.getPublicatie_niveau());
            hoofdgroep.setBedrijfslevenDataList(publicatieNiveauData);
            setCurrentRow();
            updateProgressBar();
        }

        ArrayList<Correctiefactoren> correctiefactorenList = correctiesDAO.getAll();

        if (correctiefactorenList.isEmpty()) {
            for (Bedrijfsleven bedrijfsleven : bedrijfslevenList) {
                bedrijfsleven.setCorrectiefactoren(new Correctiefactoren(
                        bedrijfsleven.getSbi(),
                        bedrijfsleven.getSbiString(),
                        0.00,
                        0.00,
                        0.00,
                        0.00,
                        0.00,
                        0.00,
                        0.00,
                        0.00
                ));
            }
        } else {
            for (Correctiefactoren correctiefactoren : correctiefactorenList) {
                for (Bedrijfsleven bedrijfsleven : bedrijfslevenList) {
                    String trueParent = bedrijfsleven.getSbiString().substring(0, 2);
                    if (correctiefactoren.getSbiString().equals(trueParent)) {
                        //System.out.println("True parent: " + trueParent + " current sbi: " + bedrijfsleven.getSbiString());
                        bedrijfsleven.setCorrectiefactoren(correctiefactoren);
                    } else {
                        bedrijfsleven.setCorrectiefactoren(new Correctiefactoren(
                                bedrijfsleven.getSbi(),
                                bedrijfsleven.getSbiString(),
                                0.00,
                                0.00,
                                0.00,
                                0.00,
                                0.00,
                                0.00,
                                0.00,
                                0.00
                        ));
                    }
                }
            }
        }

        FactorenBerekenen berekenFactoren = new FactorenBerekenen(bedrijfslevenList, hoofdgroepList, correctiefactorenList, task);
        ArrayList<Factoren> factorenList = berekenFactoren.berekenen();
        //delete all before inserting again...
        factorenDAO.deleteAll();
        factorenDAO.insertMultiple(factorenList);

        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        updateMessage("Done!");
        stage.hide();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informatie");
        alert.setHeaderText("Factoren berekenen gelukt!");
        alert.setContentText("Het berekenen van de factoren is succesvol afgerond.");
        alert.show();
    }

    public void updateProgressBar() {
        this.updateProgress(currentRow, maxRows);
    }

    public void setCurrentRow() {
        currentRow++;
    }

    /*

    public void calculateFactorenTw() {
    public void calculateFactorenTwAndProd() {
        ArrayList<Bedrijfsleven> allBedrijfsleven = getAllSbiBedrijfsleven();
        ArrayList<Hoofdgroep> allHoofdgroepen = getAllHoofdgroepen();
        factoren = new ArrayList<>();

        for (Bedrijfsleven bedrijfsleven : allBedrijfsleven) {
            ArrayList<BedrijfslevenData> sbiYearsTw = getToegevoegdeWaardeBySbi(bedrijfsleven.getSbi());
            for (BedrijfslevenData tWYear : sbiYearsTw) {
                int year = tWYear.getJaar();
                double tw = tWYear.getToegevoegdeWaarde();
                Hoofdgroep hoofdgroep = bedrijfsleven.getHoofdgroep();

                if (hoofdgroep != null) {
                    ArrayList<BedrijfslevenData> hoofdgroepData = getHoofdgroepData(hoofdgroep.getPublicatie_niveau());
                    double twHoofdgroep = calculateHoofdgroepTwByYear(hoofdgroepData, year);
                    double gemPerBaanHoofdgroep = calculateHoofdgroepProductieGemiddeldPerBaan(hoofdgroepData, year);
                    double gemPerBaanBedrijfsleven = calculateProductieGemiddeldPerBaan(tWYear);

                    if (twHoofdgroep != 0) {
                        double twTotaal = twHoofdgroep / tw;
                        addFactor(bedrijfsleven.getSbi(), year, twTotaal);

//                        calculateProductieGemiddeldPerBaan();
//                        double prodTotaal = gem.prod.per.baan sbi/ hoofdgroep
                    } else {
//                    do iets
                    }
                    double twTotaal = twHoofdgroep / tw;
                    double prodTotaal = gemPerBaanBedrijfsleven / gemPerBaanHoofdgroep;
                    addFactor(bedrijfsleven.getSbi(), year, twTotaal, prodTotaal);
                }
            }
        }
        if (factoren.size() != 0) {
            addFactorenToDb();
        }
    }

    public void calculateProductieGemiddeldPerBaan(int year, int sbi) {

    public double calculateProductieGemiddeldPerBaan(BedrijfslevenData prodYear) {
//        TODO bedrijfsopbrengst/banen per werkz..
        return;
        if (Double.isNaN(prodYear.getOpbrengsten() / prodYear.getBanen())) {
            return 0;
        } else {
            return prodYear.getOpbrengsten() / prodYear.getBanen();
        }
    }

    public double calculateHoofdgroepProductieGemiddeldPerBaan(ArrayList<BedrijfslevenData> hoofdgroepData, int year) {
        for (BedrijfslevenData hData : hoofdgroepData) {
            if (hData.getJaar() == year) {
                return hData.getOpbrengsten() / hData.getBanen();
            }
        }
        return 0;
    }

    public double calculateHoofdgroepTwByYear(ArrayList<BedrijfslevenData> hoofdgroepData, int year) {
        for (BedrijfslevenData hData : hoofdgroepData) {
            if (hData.getJaar() == year) {
            if (hData.getJaar() == year && hData.getAfschrijvingenVasteActiva() != 0) {
                return hData.getAfschrijvingenVasteActiva() + hData.getBedrijfsResultaat() + hData.getPersoneelKosten();
            }
        }
        return 0;
    }

    public void addFactor(int sbi, int year, double twTotaal) {
    public void addFactor(int sbi, int year, double twTotaal, double prodTotaal) {
        factoren.add(calculateCorrectiesbySbi(sbi, year, twTotaal, prodTotaal));
    }

    public void addFactorenToDb() {
        FactorenDAO factorenDAO = new FactorenDAO();
        factorenDAO.insertMultiple(factoren);
    }

    public ArrayList<Correctiefactoren> getCorrectiesBySbiAndYear(int sbi, int year) {
        return
    public Factoren calculateCorrectiesbySbi(int sbi, int year, double twTotaal, double prodTotaal) {
        ArrayList<Correctiefactoren> correctiefactoren = getCorrectiesBySbi(sbi);

        if (correctiefactoren.size() == 1) {
            double prod0 = 0.001;
            double prod1Tot10 = 0.8 * prodTotaal * correctiefactoren.get(0).getBanen1Tot10Productie();
            double prod0Tot10 = 0;
            double prod10Tot100 = prodTotaal * correctiefactoren.get(0).getBanen10Tot100Productie();
            double prod0T19 = 0;
            double prod20Tot100 = 0;
            double prod100 = prodTotaal * correctiefactoren.get(0).getBanen100Productie();
            double tW0 = 0.001;
            double tW1Tot10 = 0.8 * twTotaal * correctiefactoren.get(0).getBanen1Tot10Tw();
            double tW0Tot10 = 0;
            double tW10Tot100 = twTotaal * correctiefactoren.get(0).getBanen10Tot100Tw();
            double tW0Tot19 = 0;
            double tW0Tot100 = 0;
            double tW100 = twTotaal * correctiefactoren.get(0).getBanen100Tw();
            return new Factoren(sbi, year, prodTotaal, prod0, prod1Tot10, prod0Tot10, prod10Tot100, prod0T19, prod20Tot100, prod100, twTotaal,
                    tW0, tW1Tot10, tW0Tot10, tW10Tot100, tW0Tot19, tW0Tot100, tW100);
        } else {
            return new Factoren(sbi, year, prodTotaal, 0, 0, 0, 0, 0, 0, 0, twTotaal,
                    0, 0, 0, 0, 0, 0, 0);
        }
    }

    public ArrayList<Correctiefactoren> getCorrectiesBySbi(int sbi) {
        CorrectiesDAO correctiesDAO = new CorrectiesDAO();
        return correctiesDAO.getBySBI(sbi);
    }

    public ArrayList<Bedrijfsleven> getAllSbiBedrijfsleven() {
        BedrijfslevenDAO bedrijfslevenDAO = new BedrijfslevenDAO();
        return bedrijfslevenDAO.getAll();
    }


    public ArrayList<Hoofdgroep> getAllHoofdgroepen() {
        HoofdgroepDAO hoofdgroepDAO = new HoofdgroepDAO();
        return hoofdgroepDAO.getAll();
    }

    public ArrayList<BedrijfslevenData> getHoofdgroepData(String publicatie) {
        BedrijfslevenDataDAO bedrijfslevenDataDAO = new BedrijfslevenDataDAO();
        return bedrijfslevenDataDAO.getAllHoofdgroepData(publicatie);
    }

    public ArrayList<BedrijfslevenData> getToegevoegdeWaardeBySbi(int sbi) {
        BedrijfslevenDataDAO bedrijfslevenDataDAO = new BedrijfslevenDataDAO();
        return bedrijfslevenDataDAO.getAllTWBySBI(sbi);
    }

*/
}
