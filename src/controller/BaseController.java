package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import model.Bedrijfsleven;
import utility.BaseService;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.ResourceBundle;


/**
 * Edited by Sidney
 * Edited by Dennis
 */
public class BaseController implements Initializable {

    private String currentState;

    @FXML private final String BEDRIJFSLEVEN = "Bedrijfsleven";
    @FXML private final String REGELCODE = "Regelcode";
    @FXML private final String HOOFDGROEP = "Hoofdgroep";
    @FXML private final String FACTOREN = "Factoren";
    @FXML private final String CORRECTIES = "Correcties";

    @FXML private Pane overzicht;

    public BaseController() {
        currentState = BEDRIJFSLEVEN;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            displayOverzicht();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * hanteerd de navigatie knoppen en veranderd de huidige state
     * @param event
     */
    @FXML
    private void navigateButtonHandler(ActionEvent event) {
        currentState = ((Button) event.getSource()).getText();
        try {
            displayOverzicht();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * weergeeft het huidige scherm gebaseerd op de huidige state.
     * @throws IOException
     */
    private void displayOverzicht() throws IOException {
        overzicht.getChildren().clear();
        String file = "view/" + currentState.toLowerCase() + "/" + currentState + "OverzichtView.fxml";
        FXMLLoader loader = new FXMLLoader(getClass().getResource(file));
        Parent root = (Parent) loader.load(getClass().getClassLoader().getResource(file).openStream());
        overzicht.setPickOnBounds(false);
        overzicht.getChildren().add(root);
    }
}
