package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;

import java.io.IOException;

/**
 * Created by Sidney on 20-5-2016.
 */
public abstract class OverzichtController {

    protected String detailsView;
    protected String toevoegenView;
    protected Object selectedRow;

    @FXML private Pane display;
    @FXML protected TableView<Object> tableView;

    /**
     * Dient ervoor om in alle DetailsController de huidig geselecteerde regel te herkennen.
     * Object dient omgezet te worden naar het des betreffende model dat nodig is.
     * @return
     */
    public Object getSelectedRow() {
        return selectedRow;
    }

    /**
     * Weertoont het sub-scherm voor het overzicht. (Details of handleToevoegenButton)
     */
    private void display(String view) {
        display.getChildren().clear();

        FXMLLoader loader = new FXMLLoader(getClass().getResource(view));
        Parent root = null;
        try {
            root = (Parent) loader.load(getClass().getClassLoader().getResource(view).openStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        display.setPickOnBounds(false);
        display.getChildren().add(root);

        if (loader.<DetailsController>getController() != null && loader.<DetailsController>getController() instanceof DetailsController) {
            loader.<DetailsController>getController().setDetails(this);
        }
    }

    protected void displayDetails() {
        display(detailsView);
    }

    protected void displayToevoegen() {
        tableView.getSelectionModel().clearSelection();
        selectedRow = null;
        display(toevoegenView);
    }

    protected void tableEventListener() {
        tableView.setRowFactory( tv -> {
            TableRow<Object> row = new TableRow<>();

            row.setOnKeyPressed(event -> {
                if(event.getCode() == KeyCode.ENTER && (! row.isEmpty()) ) {
                    selectedRow = row.getItem();
                    displayDetails();
                }
            });

            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 1 && (! row.isEmpty()) ) {
                    selectedRow = row.getItem();
                    displayDetails();
                }
            });
            // returns instance (or pointer) of the clicked object.
            return row;
        });
    }

    @FXML
    public void handleToevoegenButton(ActionEvent event) {
        displayToevoegen();
    }


}
